#include <iostream>
#include <sstream>
#include <cmath>
#include <ctime>
#include "util.h"
#include "network/v_network.h"
#include "network/v_convolutional_layer.h"
#include "trainer/v_gpu_util.h"
#include "trainer/v_gpu_auc.h"
#include "trainer/v_gpu_bp.h"
#include "v_temporal_network.h"

#ifdef OPENCV_3_0
#include <opencv2/cudaarithm.hpp>
using namespace cv::cuda;
#define cv_gpu cv::cuda
#else
#include <opencv2/gpu/gpu.hpp>
using namespace cv::gpu;
#define cv_gpu cv::gpu
#endif

using namespace cv_ann;

std::stringstream ss;
std::string prefix = "../data/";

struct Tensor_iterator
{
	std::vector<V_Tensor>* images_;
	int i;
	int j;

	Tensor_iterator(std::vector<V_Tensor>* images)
	{
		images_ = images;
		i = 0;
		j = 0;
	}

	cv::Mat operator()()
	{
		if(j >= (*images_)[i].depth())
		{
			j = 0;
			i += 1;
		}
		if(i >= images_->size()) return cv::Mat();
		return (*images_)[i][j++];
	}

};

cv::Mat tile_images(std::vector<V_Tensor>& images, int R, int C, int width, int height)
{
	cv::Mat display = cv::Mat::zeros(height*R, width*C, CV_8UC1);
	Tensor_iterator iter(&images);

    for(int y = 0;y<R;++y)
	{
        for(int x = 0;x<C;++x)
		{
			cv::Mat d = display(cv::Range(y*height,(y+1)*height),cv::Range(x*width,(x+1)*width));
			cv::Mat image = iter();
			if(image.empty()) return display;
			cv::resize(image, image, cv::Size(width,height));
			cv::normalize(image, image, 1.0, 0.0, cv::NORM_MINMAX);
			image.convertTo(d, CV_8UC1, 255.0);
		}
	}

	return display;
}

cv::Mat tile_images(V_Tensor& images, int R, int C, int width, int height)
{
	cv::Mat display = cv::Mat::zeros(height*R, width*C, CV_8UC1);
	int i = 0;

    for(int y = 0;y<R;++y)
	{
        for(int x = 0;x<C;++x)
		{
			cv::Mat d = display(cv::Range(y*height,(y+1)*height),cv::Range(x*width,(x+1)*width));
			if(i >= images.depth()) return display;
			cv::Mat image = images[i++];
			cv::resize(image, image, cv::Size(width,height));
			cv::normalize(image, image, 1.0, 0.0, cv::NORM_MINMAX);
			image.convertTo(d, CV_8UC1, 255.0);
		}
	}

	return display;
}

cv::Mat tile_images(GpuMat images, int R, int C, int width, int height, int true_rows)
{
	cv::Mat display = cv::Mat::zeros(height*R, width*C, CV_8UC1);
	int i = 0;

    for(int y = 0;y<R;++y)
	{
        for(int x = 0;x<C;++x)
		{
			cv::Mat d = display(cv::Range(y*height,(y+1)*height),cv::Range(x*width,(x+1)*width));
			if(i >= images.cols) return display;
			cv::Mat image;
			images.col(i++).download(image);
			image = image.reshape(1, true_rows);
			cv::resize(image, image, cv::Size(width,height));
			cv::normalize(image, image, 1.0, 0.0, cv::NORM_MINMAX);
			image.convertTo(d, CV_8UC1, 255.0);
		}
	}

	return display;
}

void test_auc_training(int width, int height) 
{
	cv::Mat d;
	std::vector<V_GPU_Tensor> data;
	ss.str("");
	ss << prefix << "0.png";
	d = load_image(ss.str(), width, height);
	data.push_back(V_GPU_Tensor(GpuMat(d)));
	ss.str("");
	ss << prefix << "4.png";
	d = load_image(ss.str(), width, height);
	data.push_back(V_GPU_Tensor(GpuMat(d)));
	ss.str("");
	ss << prefix << "8.png";
	d = load_image(ss.str(), width, height);
	data.push_back(V_GPU_Tensor(GpuMat(d)));
	
	std::vector<V_Tensor> weights;
	if(!load("./test.kernel", weights))
	{
		cv_ann::setup_gpu();
		cv::RNG rand_seed = cv::RNG(std::time(0));
		std::vector<V_GPU_Tensor> Ws;
		for(int i = 0;i<100;++i)
		{
			cv::Mat kernel = cv::Mat(11,11,CV_32FC1);
			rand_seed.fill(kernel,cv::RNG::NORMAL,0,0.01);
			GpuMat g_kernel;
			g_kernel.upload(kernel);
			Ws.push_back(V_GPU_Tensor(g_kernel));
		}

		V_Parameter_AUC params;
		params.batchPortion = 1.0;
		params.error_threshold = 1e-5;
		params.lambda = 0.0;
		params.learning_rate = 0.01;
		params.max_iteration = 10000;
		params.momentum = 0.5;

		auc_train(data,Ws,params);

		for(int i = 0;i<Ws.size();++i)
		{
			weights.push_back(V_Tensor(Ws[i].depth()));
			Ws[i].download(weights.back());
		}
		save("./test.kernel", weights);
	}

	//load("./test.kernel", weights);
	//save("./test2.kernel", weights);
	////weights.push_back(V_Tensor(cv::Mat::ones(11,11,CV_32FC1)));
	////weights.push_back(V_Tensor(cv::Mat::ones(11,11,CV_32FC1)));
	
	ss.str("");
	ss << prefix << "5.png";
	d = load_image(ss.str(), width, height);

	auto layer = std::make_shared<V_Convolutional_Layer>();
	layer->set_weight(weights);

	cv::imshow("raw", d);
	V_Tensor h = layer->activate(V_Tensor(d));
	V_Tensor v = layer->project(h);
	cv::imshow("display", v[0]);

	cv::Mat show_w = tile_images(weights, 10, 10, 50, 50);
	
	ss.str("");
	ss << "Kernels";
	cv::imshow(ss.str(),show_w);
    cv::moveWindow(ss.str(),100,500);

	cv::waitKey(0);
}

void test_conditional_auc_training(int width, int height)
{
	cv::Mat d;
	std::vector<V_GPU_Tensor> data;
	std::vector<V_GPU_Tensor> conditions;
	ss.str("");
	ss << prefix << "s2.png";
	d = load_image(ss.str(), width, height);
	data.push_back(V_GPU_Tensor(GpuMat(d)));
	conditions.push_back(V_GPU_Tensor(GpuMat(d)));	


	std::vector<V_Tensor> ws;
	std::vector<V_Tensor> us;
	load("./test_conditional_u.kernel", us);
	if(!load("./test_conditional_w.kernel", ws))
	{
		cv_ann::setup_gpu();
		cv::RNG rand_seed = cv::RNG(std::time(0));
		std::vector<V_GPU_Tensor> Ws;
		for(int i = 0;i<10;++i)
		{
			cv::Mat kernel = cv::Mat(11,11,CV_32FC1);
			rand_seed.fill(kernel,cv::RNG::NORMAL,0,0.01);
			GpuMat g_kernel;
			g_kernel.upload(kernel);
			Ws.push_back(V_GPU_Tensor(g_kernel));
		}

		std::vector<V_GPU_Tensor> Us;
		{
			cv::Mat kernel = cv::Mat(21,21,CV_32FC1);
			rand_seed.fill(kernel,cv::RNG::NORMAL,0,0.01);
			GpuMat g_kernel;
			g_kernel.upload(kernel);
			Us.push_back(V_GPU_Tensor(g_kernel));
		}

		V_Parameter_AUC params;
		params.batchPortion = 1.0;
		params.error_threshold = 1e-6;
		params.lambda = 0.0;
		params.learning_rate = 0.01;
		params.max_iteration = 10000;
		params.momentum = 0.5;

		auc_train(data,Ws,conditions,Us,params);

		for(int i = 0;i<Ws.size();++i)
		{
			ws.push_back(V_Tensor(Ws[i].depth()));
			Ws[i].download(ws.back());
		}
		for(int i = 0;i<Us.size();++i)
		{
			us.push_back(V_Tensor(Us[i].depth()));
			Us[i].download(us.back());
		}

		save("./test_conditional_w.kernel", ws);
		save("./test_conditional_u.kernel", us);
	}

	ss.str("");
	ss << prefix << "s1.png";
	d = load_image(ss.str(), width, height);
	cv::imshow("image", d);
	cv::imshow("condition", d);

	auto layer = std::make_shared<V_Conditional_Convolutional_Layer>();
	layer->set_weight(ws, us);

	V_Tensor h = layer->activate(V_Tensor(d));
	V_Tensor c = layer->activate_condition(V_Tensor(d));
	cv::imshow("gen condition", c[0]);
	V_Tensor v = layer->project(h,c);
	cv::imshow("gen display", v[0]);
	
	cv::Mat show_w = tile_images(ws, 10, 10, 50, 50);
	ss.str("");
	ss << "W";
	cv::imshow(ss.str(),show_w);
    cv::moveWindow(ss.str(),100,500);

	show_w = tile_images(us, 1, 1, 200, 200);
	ss.str("");
	ss << "U";
	cv::imshow(ss.str(),show_w);
    cv::moveWindow(ss.str(),100,500);


	cv::waitKey(0);
}

void test_pack_unpack(int width, int height)
{
	cv::Mat d;
	V_GPU_Tensor data;
	ss.str("");
	ss << prefix << "0.png";
	d = load_image(ss.str(), width, height);
	data.push_back(GpuMat(d));
	ss.str("");
	ss << prefix << "4.png";
	d = load_image(ss.str(), width, height);
	data.push_back(GpuMat(d));
	ss.str("");
	ss << prefix << "8.png";
	d = load_image(ss.str(), width, height);
	data.push_back(GpuMat(d));

	const int s = 4;

	V_GPU_Tensor pack_map;
	V_GPU_Tensor unpack_map;
	cv_ann::create_pack_map(data.depth(),width,height,s,pack_map);
	cv_ann::create_unpack_map(data.depth(),width,height,s,unpack_map);

	V_GPU_Tensor packed(s*s*data.depth());
	V_GPU_Tensor unpacked(data.depth());
	V_GPU_Tensor buff(data.depth());

	cv_ann::pack_grid(data, packed, s, pack_map);
	cv_ann::unpack_grid(packed, unpacked, s, unpack_map, buff);

	V_Tensor local_data(data.depth());
	V_Tensor local_packed(s*s*data.depth());
	V_Tensor local_unpacked(data.depth());

	data.download(local_data);
	packed.download(local_packed);
	unpacked.download(local_unpacked);

	cv::Mat show_w;
	
	show_w = tile_images(local_packed, 3, 4*4, 50, 100);
	ss.str("");
	ss << "packed";
	cv::imshow(ss.str(),show_w);
    cv::moveWindow(ss.str(),100,500);
	
	show_w = tile_images(local_unpacked, 1, 3, 100, 200);
	ss.str("");
	ss << "unpacked";
	cv::imshow(ss.str(),show_w);
    cv::moveWindow(ss.str(),100,500);
	
	show_w = tile_images(local_data, 1, 3, 100, 200);
	ss.str("");
	ss << "data";
	cv::imshow(ss.str(),show_w);
    cv::moveWindow(ss.str(),100,500);

	cv::waitKey(0);
}

void test_back_propagation(int width, int height)
{
	cv::Mat d, l_;
	cv::Mat data_;
	cv::Mat label_;
	ss.str("");
	ss << prefix << "0.png";
	d = load_image(ss.str(), width, height);
	data_.push_back(d.reshape(1, 1));
	l_ = cv::Mat::zeros(1, 10, CV_32FC1);
	l_.at<float>(0) = 1.0;
	label_.push_back(l_);
	ss.str("");
	ss << prefix << "4.png";
	d = load_image(ss.str(), width, height);
	data_.push_back(d.reshape(1, 1));
	l_ = cv::Mat::zeros(1, 10, CV_32FC1);
	l_.at<float>(4) = 1.0;
	label_.push_back(l_);
	ss.str("");
	ss << prefix << "8.png";
	d = load_image(ss.str(), width, height);
	data_.push_back(d.reshape(1, 1));
	l_ = cv::Mat::zeros(1, 10, CV_32FC1);
	l_.at<float>(8) = 1.0;
	label_.push_back(l_);

	GpuMat label, data;
	label.upload(label_.t());
	data.upload(data_.t());

	std::vector<GpuMat> Ws;
	std::vector<GpuMat> Bs;
	Ws.resize(2);
	Bs.resize(2);

	cv::RNG rand_seed = cv::RNG(std::time(0));
	cv::Mat rand = cv::Mat(50, data.rows, CV_32FC1);
	rand_seed.fill(rand, cv::RNG::NORMAL, 0, 0.01);
	Ws[0].upload(rand);
	Bs[0].create(50, 1, CV_32FC1);
	Bs[0].setTo(1);

	rand = cv::Mat(label.rows, rand.rows, CV_32FC1);
	rand_seed.fill(rand, cv::RNG::NORMAL, 0, 0.01);
	Ws[1].upload(rand);
	Bs[1].create(label.rows, 1, CV_32FC1);
	Bs[1].setTo(1);

	std::cout << label_ << std::endl;
	backpropagation(data, label, Ws, Bs);
}

void test_auc_generic(int width, int height)
{
	cv::Mat d, l_;
	cv::Mat data_;

	for(int i = 1;i<=116;i+=1)
	{
		ss.str("");
		ss << prefix << "/mix_digits/d (" << i << ").png";
		d = load_image_grey(ss.str(), width, height);
		if(d.empty()) continue;
		data_.push_back(d.reshape(1, 1));
	}

	GpuMat data;
	data.upload(data_.t());

	GpuMat Wr0, Wr1, Wr2, Wr3;
	GpuMat Wg0, Wg1, Wg2, Wg3;
	GpuMat B0, B1, B2, B3, Bp;

	cv::RNG rand_seed = cv::RNG(std::time(0));
	cv::Mat rand = cv::Mat(50, data.rows, CV_32FC1);
	rand_seed.fill(rand, cv::RNG::NORMAL, 0, 0.001);
	Wr0.upload(rand);
	rand_seed.fill(rand, cv::RNG::NORMAL, 0, 0.001);
	Wg0.upload(rand);
	B0.create(50, 1, CV_32FC1);
	B0.setTo(0);

	rand.create(10, 50, CV_32FC1);
	rand_seed.fill(rand, cv::RNG::NORMAL, 0, 0.001);
	Wr1.upload(rand);
	rand_seed.fill(rand, cv::RNG::NORMAL, 0, 0.001);
	Wg1.upload(rand);
	B1.create(10, 1, CV_32FC1);
	B1.setTo(0);

	rand.create(10, 10, CV_32FC1);
	rand_seed.fill(rand, cv::RNG::NORMAL, 0, 0.001);
	Wr2.upload(rand);
	rand_seed.fill(rand, cv::RNG::NORMAL, 0, 0.001);
	Wg2.upload(rand);
	B2.create(10, 1, CV_32FC1);
	B2.setTo(0);

	rand.create(2, 10, CV_32FC1);
	rand_seed.fill(rand, cv::RNG::NORMAL, 0, 0.0001);
	Wr3.upload(rand);
	rand_seed.fill(rand, cv::RNG::NORMAL, 0, 0.0001);
	Wg3.upload(rand);
	B3.create(2, 1, CV_32FC1);
	B3.setTo(0);

	Bp.create(data.rows, data.cols, CV_32FC1);
	Bp.setTo(0);

	cv_ann::V_Parameter_AUC params;
	params.learning_rate = 0.001;
	params.lambda = 0;
	params.error_threshold = 1e-6;
	params.max_iteration = 20000;
	params.momentum = 0.9;
	params.with_bias = true;
	cv_ann::auc_non_sharing_relu_train(data, Wr0, Wg0, B0, Bp, params);
	
	GpuMat emp = GpuMat(), buf, temp, data1, data2, data3, temp2;
	GpuMat one_row_data = GpuMat(1,data.cols,CV_32FC1); one_row_data.setTo(1);

	cv_gpu::gemm(B0,one_row_data,1,emp,0,temp);
	cv_gpu::gemm(Wr0,data,1,temp,1,temp2);
	cv_ann::relum(temp2, data1);
	
	cv_ann::auc_non_sharing_relu_train(data1, Wr1, Wg1, B1, temp, params);

	cv_gpu::gemm(B1,one_row_data,1,emp,0,temp);
	cv_gpu::gemm(Wr1,data1,1,temp,1,temp2);
	cv_ann::relum(temp2, data2);
	
	cv_ann::auc_non_sharing_relu_train(data2, Wr2, Wg2, B2, temp, params);

	cv_gpu::gemm(B2,one_row_data,1,emp,0,temp);
	cv_gpu::gemm(Wr2,data2,1,temp,1,temp2);
	cv_ann::relum(temp2, data3);
	
	params.learning_rate = 0.0001;
	cv_ann::auc_non_sharing_relu_train(data3, Wr3, Wg3, B3, temp, params);
	
	Bp.create(data.rows, 1, CV_32FC1);
	Bp.setTo(0);

	GpuMat input_g, h3, h2, h1, v, Wg3_t, Wg2_t, Wg1_t, Wg0_t;
	cv::Mat input;
	
	cv_gpu::transpose(Wg3, Wg3_t);
	cv_gpu::transpose(Wg2, Wg2_t);
	cv_gpu::transpose(Wg1, Wg1_t);
	cv_gpu::transpose(Wg0, Wg0_t);

	cv_ann::V_Parameter_BP param_bp;
	param_bp.learning_rate = 0.001;
	param_bp.error_threshold = 1e-6;
	param_bp.lambda = 0;
	param_bp.max_iteration = 20000;
	param_bp.momentum = 0.9;

	std::vector<GpuMat> Ws, Bs;
	Ws.push_back(Wr0);
	Ws.push_back(Wr1);
	Ws.push_back(Wr2);
	Ws.push_back(Wr3);
	Ws.push_back(Wg3_t);
	Ws.push_back(Wg2_t);
	Ws.push_back(Wg1_t);
	Ws.push_back(Wg0_t);
	Bs.push_back(B0);
	Bs.push_back(B1);
	Bs.push_back(B2);
	Bs.push_back(B3);
	Bs.push_back(B2);
	Bs.push_back(B1);
	Bs.push_back(B0);
	Bs.push_back(Bp);
	backpropagation(data, data, Ws, Bs, param_bp);

	auto activate = [&](GpuMat input_g)
	{
		cv_gpu::gemm(B2,one_row_data,1,emp,0,temp);
		cv_gpu::gemm(Wg3_t,input_g,1,temp,1,temp2);
		cv_ann::relum(temp2, h3);

		cv_gpu::gemm(B1,one_row_data,1,emp,0,temp);
		cv_gpu::gemm(Wg2_t, h3,1,temp,1,temp2);
		cv_ann::relum(temp2, h2);

		cv_gpu::gemm(B0,one_row_data,1,emp,0,temp);
		cv_gpu::gemm(Wg1_t,h2,1,temp,1,temp2);
		cv_ann::relum(temp2, h1);

		cv_gpu::gemm(Wg0_t,h1,1,Bp,1,temp2);
		cv_ann::relum(temp2, v);

		return v;
	};
	
	float num1 = 0, num2 = 0;
	char c;
	while(true)
	{	
		cv::Mat input = (cv::Mat_<float>(2, 1) << num1, num2);
		input_g.upload(input);
		one_row_data.create(1, 1, CV_32FC1); one_row_data.setTo(1);
		Bp.create(data.rows, input.cols, CV_32FC1); Bp.setTo(0);

		GpuMat v = activate(input_g);
		cv::Mat display = tile_images(v, 1, 1, 200, 400, 40);
		cv::imshow("Reconstruction", display);

		c = cv::waitKey(-1);
		if(c=='q') break;
		else if(c=='w') num1 += 0.1;
		else if(c=='s') num1 -= 0.1;
		else if(c=='e') num2 += 0.1;
		else if(c=='d') num2 -= 0.1;

		std::cout << num1 << " " << num2 << std::endl;
	}

}

int main(int argc, char *argv[])
{
	std::cout << "Version 1.0" << std::endl;

	//test_auc_training(100, 200);
	//test_conditional_auc_training(0,0);
	//test_pack_unpack(100,200);
	//V_Temporal_Network::test();
	//test_back_propagation(10, 20);
	test_auc_generic(20,40);

	return 0;
}
