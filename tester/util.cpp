#include "util.h"

cv::Mat load_image(std::string file_name, int width, int height, bool reshape)
{
	cv::Mat d;
	d = cv::imread(file_name, cv::IMREAD_GRAYSCALE);
	if(d.empty()) 
	{
		std::cout << "Fail to load image: " << file_name << std::endl;
		return cv::Mat();
	}
	if(width > 0 && height > 0) cv::resize(d,d,cv::Size(width,height));
	cv::threshold(d,d,100,255.0,cv::THRESH_OTSU|cv::THRESH_BINARY);
	d.convertTo(d,CV_32FC1,1/255.0);
	if(reshape) return d.reshape(0,1);
	return 1-d;
}

cv::Mat load_image_grey(std::string file_name, int width, int height, bool reshape)
{
	cv::Mat d;
	d = cv::imread(file_name, cv::IMREAD_GRAYSCALE);
	if(d.empty()) 
	{
		std::cout << "Fail to load image: " << file_name << std::endl;
		return cv::Mat();
	}
	if(width > 0 && height > 0) cv::resize(d,d,cv::Size(width,height));
	d.convertTo(d,CV_32FC1,1/255.0);
	if(reshape) return d.reshape(0,1);
	return (1-d);
}


cv::Mat load_image_64(std::string file_name, int width, int height, bool reshape)
{
	cv::Mat d;
	d = cv::imread(file_name, cv::IMREAD_GRAYSCALE);
	if(d.empty()) 
	{
		std::cout << "Fail to load image: " << file_name << std::endl;
		return cv::Mat();
	}
	if(width > 0 && height > 0) cv::resize(d,d,cv::Size(width,height));
	cv::threshold(d,d,100,255.0,cv::THRESH_OTSU|cv::THRESH_BINARY);
	d.convertTo(d,CV_64FC1,1/255.0);
	if(reshape) return d.reshape(0,1);
	return 1-d;
}