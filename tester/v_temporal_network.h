#ifndef V_TEMPORAL_NETWORK_H
#define V_TEMPORAL_NETWORK_H

#include "trainer/v_gpu_util.h"
#include "network/v_util.h"
#include <vector>
#include <functional>

namespace cv_ann
{
    enum V_Layer_Type{
        SIGM = 0,RECT
    };

    class V_Temporal_Network
    {
    protected:
        // input -> W0 + B1 + U1 -> activate1 -> W1 + B2 + U2 -> activate2 -> W2 + B3 + U3 -> activate3 -> output
        // output -> W2' + B2 + U2 -> activate2 -> W1' + B1 + U1 -> activate1 -> W0' + B0 + U0 -> activate0 -> input
        std::vector<cv::Mat> Ws_;
        std::vector<cv::Mat> Bs_;
		std::vector<cv::Mat> Us_;
        std::vector< std::function<cv::Mat(cv::Mat)> > As_; //activations
        std::vector< std::function<cv::Mat(cv::Mat)> > Ds_; //derivatives
        std::vector< std::function<cv::Mat(cv::Mat)> > Ss_; //sampling_functions
        std::vector<V_Layer_Type> types_; //derivatives
        std::vector<int> recursive_steps_;

        static cv::Mat arrange_data(cv::Mat data, int recursive_step, std::vector<int> episode);
		
		static cv::Mat extract_hist(cv::Mat data, int recursive_step);
		
        static std::pair<cv::Mat,cv::Mat> sample(
                cv::Mat V_, cv::Mat W_, cv::Mat B_, cv::Mat Bp_,
                std::function<cv::Mat(cv::Mat)> A,std::function<cv::Mat(cv::Mat)> Sp,
                int iteration = 10);

    public:

        V_Temporal_Network();
        ~V_Temporal_Network();

        V_Temporal_Network& operator=(V_Temporal_Network& other);

        // t and t-1: recursive_steps = 1; t t-1 t-2: recursive steps = 2
        void add_layer(int num_outputs, int recursive_steps, V_Layer_Type type = V_Layer_Type::RECT);

        cv::Mat sample(cv::Mat initial_frames);

        // data are a list column-wised datum concatenated together. episodes tell how long each sequence is.
        void train(cv::Mat data, std::vector<int> episodes, bool incremental = false);

        bool save(std::string file_name = "./tnn.ann");
        bool load(std::string file_name = "./tnn.ann");
		
		static void test(int width = 20, int height = 40);
    };

}

#endif
