#ifndef UTIL_H
#define UTIL_H

#include <iostream>
#include <cmath>
#include <ctime>
#include <opencv2/opencv.hpp>

cv::Mat load_image(std::string file_name, int width, int height, bool reshape = false);
cv::Mat load_image_grey(std::string file_name, int width, int height, bool reshape = false);
cv::Mat load_image_64(std::string file_name, int width, int height, bool reshape = false);

#endif