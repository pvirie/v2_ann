#include "v_temporal_network.h"
#include "trainer/v_gpu_auc.h"
#include "trainer/v_gpu_rbm.h"
#include "util.h"
#include <iostream>
#include <sstream>
#include <limits>
#include <ctime>

#ifdef OPENCV_3_0
#include <opencv2/cudaarithm.hpp>
using namespace cv::cuda;
#define cv_gpu cv::cuda
#else
#include <opencv2/gpu/gpu.hpp>
using namespace cv::gpu;
#define cv_gpu cv::gpu
#endif

using namespace cv;

namespace cv_ann
{

    V_Temporal_Network::V_Temporal_Network()
    {
	}

    V_Temporal_Network::~V_Temporal_Network()
    {	
	}

	
    V_Temporal_Network& V_Temporal_Network::operator=(V_Temporal_Network& other)
	{
		this->Ws_.resize(other.Ws_.size());
		this->Bs_.resize(other.Bs_.size());
		this->Us_.resize(other.Us_.size());

		for (unsigned int i = 0; i<this->Ws_.size(); ++i) other.Ws_[i].copyTo(this->Ws_[i]);
		for (unsigned int i = 0; i<this->Bs_.size(); ++i) other.Bs_[i].copyTo(this->Bs_[i]);
		for (unsigned int i = 0; i<this->Us_.size(); ++i) other.Us_[i].copyTo(this->Us_[i]);

		this->As_ = other.As_;
		this->Ds_ = other.Ds_;
		this->Ss_ = other.Ss_;
		this->types_ = other.types_;

		return (*this);
	}

    void V_Temporal_Network::add_layer(int num_outputs, int recursive_steps, V_Layer_Type type)
    {
        types_.push_back(type);
		if (this->Bs_.size() > 0)
		{
			this->Ws_.push_back(cv::Mat::zeros(num_outputs, this->Bs_.back().rows, CV_64FC1));
            this->Us_.push_back(cv::Mat::zeros(num_outputs, this->Bs_.back().rows*max(this->recursive_steps_[this->recursive_steps_.size()-1],1), CV_64FC1));
		}
        this->Bs_.push_back(cv::Mat::zeros(num_outputs, num_outputs*max(recursive_steps,1), CV_64FC1));
        this->recursive_steps_.push_back(recursive_steps);

        switch(type)
        {
        case V_Layer_Type::SIGM:
            this->As_.push_back(cv_ann::sigmoid_activate);
            this->Ds_.push_back(cv_ann::sigmoid_grad);
            this->Ss_.push_back(cv_ann::sigmoid_sample);
            break;
        default:
			this->As_.push_back(cv_ann::relu_activate);
            this->Ds_.push_back(cv_ann::relu_grad);
            this->Ss_.push_back(cv_ann::relu_sample);
            break;
        }
    }
	
    std::pair<cv::Mat,cv::Mat> V_Temporal_Network::sample(
        cv::Mat V_, cv::Mat W_, cv::Mat B_, cv::Mat Bp_,
        std::function<cv::Mat(cv::Mat)> A,std::function<cv::Mat(cv::Mat)> Sp,
        int iteration)
    {
        cv::Mat H_;
        cv::Mat W_t = W_.t();

        for(int j = 0;j<iteration;++j)
        {
            H_ = A(W_*V_ + B_);
            V_ = Sp(W_t*H_ + Bp_);
        }

        return std::make_pair(V_,H_);
    }

	cv::Mat V_Temporal_Network::extract_hist(cv::Mat data, int recursive_step)
	{
		cv::Mat res;
        if(recursive_step == 0) res = cv::Mat::zeros(data.rows,1,CV_64FC1);
		for(int i = recursive_step; i>0; --i)
		{
			res.push_back(data.col(data.cols - i));		
		}
		return res;
	}

    cv::Mat V_Temporal_Network::sample(cv::Mat initial_frames)
    {
		std::vector<int> episodes;
		episodes.push_back(initial_frames.cols);
		
		int i;
		cv::Mat hist;
        cv::Mat data = initial_frames;
		//propagate upward
        std::vector<cv::Mat> hist_layer;
		for(i = 0;i<Bs_.size()-1;++i)
		{
            hist_layer.push_back(V_Temporal_Network::extract_hist(data, this->recursive_steps_[i]));
            hist = arrange_data(data, this->recursive_steps_[i], episodes);
            data = As_[i+1](Ws_[i]*data + Us_[i]*hist);
        }
		hist_layer.push_back(V_Temporal_Network::extract_hist(data, this->recursive_steps_[i]));

        cv::Mat V_ = Ss_[Ss_.size()-2](Bs_[Bs_.size()-2]*hist_layer[Bs_.size()-2] + Us_[Us_.size()-2]*hist_layer[Us_.size()-2]);
        V_ = V_Temporal_Network::sample(
                    V_, Ws_[Ws_.size()-1], Us_[Us_.size()-1]*hist_layer[Us_.size()-1], Bs_[Bs_.size()-2]*hist_layer[Bs_.size()-2],
                    As_[As_.size()-1], Ss_[Ss_.size()-2]).first;

        for(i = Bs_.size()-3;i>0;--i)
        {
            V_ = As_[i](Ws_[i].t()*V_ + Bs_[i]*hist_layer[i]);
            //V_ = As_[i](Ws_[i].t()*V_ + Us_[i-1]*hist_layer[i-1]);
        }
        V_ = As_[i](Ws_[i].t()*V_ + Bs_[i]*hist_layer[i]);

        return V_;
    }


    cv::Mat V_Temporal_Network::arrange_data(cv::Mat data, int recursive_step, std::vector<int> episode)
    {
		cv::Mat dummy = cv::Mat::zeros(data.rows, 1, CV_64FC1);
        cv::Mat hist;

		int c = 0;
		for(int i = 0;i<episode.size();++i)
		{
			for(int j = 0;j<episode[i];++j,++c)
			{
				cv::Mat datum;
                if(recursive_step == 0) datum = cv::Mat::zeros(data.rows,1,CV_64FC1);
				for(int s = recursive_step;s>0;--s)
				{
					if(j-s >= 0) datum.push_back(data.col(c-s));
					else datum.push_back(dummy);
				}
                datum = datum.t();
                hist.push_back(datum);
			}
		}

        hist = hist.t();
        return hist;
    }


    void V_Temporal_Network::train(cv::Mat data, std::vector<int> episodes, bool incremental)
    {
        int learning_iteration = incremental? 2000:20000;
        if(!incremental)
        {
            cv::RNG rand_seed = cv::RNG(std::time(0));
            for(unsigned int j = 1;j<As_.size();++j)
            {
                rand_seed.fill(Ws_[j-1],cv::RNG::NORMAL,0,0.01);
                rand_seed.fill(Us_[j-1],cv::RNG::NORMAL,0,0.01);
			}
        }

		cv::Mat hist;
        {
            cv::Mat bias = cv::Mat::zeros(Bs_[0].rows,1,CV_64FC1);
            switch(types_[0])
            {
                case V_Layer_Type::RECT:
					{
						hist = V_Temporal_Network::arrange_data(data, this->recursive_steps_[0], episodes);

						cv_ann::V_Parameter_AUC params;
						params.error_threshold = 1e-6;
						params.lambda = 0.0;
						params.learning_rate = 0.002;
						params.max_iteration = 50000;
						params.momentum = 0.9;
						params.with_bias = false;

						GpuMat gV,gH,gX,gB;
						cv::Mat float_mat; 
						hist.convertTo(float_mat,CV_32FC1);
						gH.upload(float_mat);
						data.convertTo(float_mat,CV_32FC1);
						gV.upload(float_mat);
						Bs_[0].convertTo(float_mat,CV_32FC1);
						gX.upload(float_mat);
						bias.convertTo(float_mat,CV_32FC1);
						gB.upload(float_mat);

						cv_ann::supervise_relu_train(gH, gV, gX, gB, params);
				
						cv::Mat oX;
						gX.download(oX);
						oX.convertTo(Bs_[0], Bs_[0].type());
					}
                    break;
                case V_Layer_Type::SIGM:
                    hist = V_Temporal_Network::arrange_data(data, this->recursive_steps_[0], episodes);
                    tie(hist,data,Bs_[0],bias,As_[0],learning_iteration,1e-6,0.1,0.9,0.0,false);
                    break;
            }
        }

        unsigned int i = 1;
        for(;i<As_.size()-1;++i)
        {

            cv::Mat bias = cv::Mat::zeros(Bs_[i].rows,1,CV_64FC1);
			switch (types_[i])
			{
			case V_Layer_Type::RECT:
				{
					cv_ann::V_Parameter_AUC params;
					params.error_threshold = 1e-6;
					params.lambda = 0.001;
					params.learning_rate = 0.001;
					params.max_iteration = 50000;
					params.momentum = 0.9;
					params.with_bias = false;

					GpuMat gV,gH,gW,gB,gU,gBs;
					cv::Mat float_mat; 
					data.convertTo(float_mat,CV_32FC1);
					gV.upload(float_mat);
					hist.convertTo(float_mat,CV_32FC1);
					gH.upload(float_mat);
					Ws_[i - 1].convertTo(float_mat,CV_32FC1);
					gW.upload(float_mat);
					bias.convertTo(float_mat,CV_32FC1);
					gB.upload(float_mat);
					Us_[i - 1].convertTo(float_mat,CV_32FC1);
					gU.upload(float_mat);
					cv::Mat temp = Bs_[i-1]*hist;
					temp.convertTo(float_mat,CV_32FC1);
					gBs.upload(float_mat);

					cv_ann::auc_relu_train(gV, gH, gW, gB, gU, gBs, params);
				
					cv::Mat oW,oB,oU;
					gW.download(oW);
					gB.download(oB);
					gU.download(oU);
					oW.convertTo(Ws_[i - 1], Ws_[i - 1].type());
					oB.convertTo(bias, bias.type());
					oU.convertTo(Us_[i - 1], Us_[i - 1].type());

					data = relu_activate(Ws_[i - 1]*data + Us_[i - 1]*hist + bias*cv::Mat::ones(1,data.cols, data.type()));
					hist = V_Temporal_Network::arrange_data(data, this->recursive_steps_[i], episodes);
					params.with_bias = false;
					params.lambda = 0.0;
					params.learning_rate = 0.002;
					GpuMat gX;
					
					data.convertTo(float_mat,CV_32FC1);
					gV.upload(float_mat);
					hist.convertTo(float_mat,CV_32FC1);
					gH.upload(float_mat);
					Bs_[i].convertTo(float_mat,CV_32FC1);
					gX.upload(float_mat);

					cv_ann::supervise_relu_train(gH, gV, gX, gB, params);
				
					cv::Mat oX;
					gX.download(oX);
					oX.convertTo(Bs_[i], Bs_[i].type());
				}
				break;
            case V_Layer_Type::SIGM:
				{
					data = cv_ann::conditional_auenc_train(
						data, hist,
						Ws_[i - 1], bias, Us_[i - 1],
						Bs_[i-1]*hist,
						As_[i], As_[i - 1], Ds_[i],
						learning_iteration, 1e-6, 0.1,0.9,0.0,false);
				
					hist = V_Temporal_Network::arrange_data(data, this->recursive_steps_[i], episodes);
					tie(hist, data, Bs_[i], bias, As_[i], learning_iteration, 1e-6, 0.01, 0.9, 0.0, false);
				}
				break;
			}

        }

        {

            cv::Mat bias = cv::Mat::zeros(Bs_[i].rows,1,CV_64FC1);
			switch (types_[i])
			{
			case V_Layer_Type::RECT:
				{
					cv_ann::V_Parameter_RBM params;
					params.cd_round = 10;
					params.lambda = 0.0;
					params.learning_rate = 0.001;
					params.max_iteration = 10000;
					params.momentum = 0.9;
					params.with_bias = false;

					GpuMat gV,gH,gW,gB,gU,gBs;
					cv::Mat float_mat; 
					data.convertTo(float_mat,CV_32FC1);
					gV.upload(float_mat);
					hist.convertTo(float_mat,CV_32FC1);
					gH.upload(float_mat);
					Ws_[i - 1].convertTo(float_mat,CV_32FC1);
					gW.upload(float_mat);
					bias.convertTo(float_mat,CV_32FC1);
					gB.upload(float_mat);
					Us_[i - 1].convertTo(float_mat,CV_32FC1);
					gU.upload(float_mat);
					cv::Mat temp = Bs_[i-1]*hist;
					temp.convertTo(float_mat,CV_32FC1);
					gBs.upload(float_mat);

					cv_ann::rbm_relu_train(gV, gH, gW, gB, gU, gBs, params);

					cv::Mat oW,oB,oU;
					gW.download(oW);
					gB.download(oB);
					gU.download(oU);
					oW.convertTo(Ws_[i - 1], Ws_[i - 1].type());
					oB.convertTo(bias, bias.type());
					oU.convertTo(Us_[i - 1], Us_[i - 1].type());

					data = relu_activate(Ws_[i - 1] * data + Us_[i - 1] * hist + bias*cv::Mat::ones(1, data.cols, data.type()));
				}
				break;
			case V_Layer_Type::SIGM:
				{
					data = cv_ann::conditional_rbm_train(
						data, hist,
						Ws_[i - 1], bias, Us_[i - 1],
						Bs_[i-1]*hist,
						As_[i], As_[i - 1], Ss_[i],
						learning_iteration, 10, 0.1,0.9,0.0,false);
				}
				break;
			}

        }

    }


    bool V_Temporal_Network::save(std::string file_name)
    {
        if(types_.size() == 0) return false;
        cv::FileStorage fs(file_name, cv::FileStorage::WRITE);
        if(!fs.isOpened()) return false;
        fs << "num_layers" << ((int) types_.size());
        fs << "layers" << "[";
            fs << "{";
            fs << "Type" << (int)types_[0];
            fs << "B" << Bs_[0];
            fs << "recur" << recursive_steps_[0];
            fs << "}";
        for(unsigned int i = 1;i<types_.size();++i)
        {
            fs << "{";
            fs << "Type" << (int)types_[i];
			fs << "W" << Ws_[i - 1];
			fs << "U" << Us_[i - 1];
            fs << "B" << Bs_[i];
            fs << "recur" << recursive_steps_[i];
            fs << "}";
        }
        fs << "]";
        fs.release();
        return true;
    }
	
    bool V_Temporal_Network::load(std::string file_name)
    {
        cv::FileStorage fs(file_name, cv::FileStorage::READ);
        if(!fs.isOpened()) return false;
        int num_layers = int(fs["num_layers"]);
        types_.clear();
		Ws_.clear();
		Us_.clear();
        Bs_.clear();
        As_.clear();
        Ds_.clear();
        Ss_.clear();
        FileNode tl = fs["layers"];
        {
            FileNode fn = tl[0];
            cv::Mat B;
            int recur;
            fn["B"] >> B;
            fn["recur"] >> recur;
            add_layer(B.rows,recur,(V_Layer_Type) int(fn["Type"]));
            Bs_[0] = B;
        }
        for(unsigned int i = 1;i<num_layers;++i)
        {
            FileNode fn = tl[i];
            cv::Mat B,W,U;
            int recur;
			fn["W"] >> W;
			fn["U"] >> U;
            fn["B"] >> B;
            fn["recur"] >> recur;
            add_layer(B.rows,recur,(V_Layer_Type) int(fn["Type"]));
			Ws_[i - 1] = W;
			Us_[i - 1] = U;
            Bs_[i] = B;
        }
        fs.release();
        return true;
    }


	std::stringstream ss;
	std::string prefix = "../data/";
	std::string get_image_file_name(int j, int i)
	{
		ss.str("");
		ss << prefix << j << "/" << j << " (" << i << ").png";
		return ss.str();
	}

	void V_Temporal_Network::test(int width, int height)
	{
		cv_ann::setup_gpu();

		cv::Mat data;
		std::vector<int> episodes;
		episodes.push_back(0);

		for(int i = 1;i<=20;++i)
		{
			for(int j = 0;j<10;++j)
			{
				cv::Mat img = load_image_64(get_image_file_name(j,i), width, height, true);
				if(img.empty())
				{
					episodes.push_back(0);
					continue;
				}
				data.push_back(img);
				episodes[episodes.size()-1] += 1;
			}
		}

		data = data.t();

		int num_initial = 3;
		cv_ann::V_Temporal_Network ann;
		if(!ann.load("./tann.ann"))
		{
			std::cout << "Cannot loaded the trained parameter file(s). Initiating traning sequence..." << std::endl;
			ann.add_layer(width*height,0,V_Layer_Type::RECT);
			ann.add_layer(width*height/5,num_initial,V_Layer_Type::RECT);
			ann.add_layer(width*height/10,num_initial-1,V_Layer_Type::RECT);
			ann.add_layer(width*height/20,num_initial-2,V_Layer_Type::RECT);
			ann.train(data,episodes);
			ann.save("./tann.ann");
		}

		{		
			int R = 5;
			int C = 20;
			std::vector<cv::Mat> images;
			cv::Mat tmp;

			for(int j = 1;j<=8;++j)
			{
				int i = 0;
				for(;i<num_initial;++i)
				{
					tmp = load_image_64(get_image_file_name(i,j), width, height, true);
					images.push_back(tmp);
				}
				for(;i<10;++i)
				{
					cv::Mat initial_frames;
					for(int k = num_initial;k>0;--k)
					{
						tmp = images[images.size()-k];
						initial_frames.push_back(tmp);
					}
					cv::Mat artificial = ann.sample(initial_frames.t()).t();
					images.push_back(artificial);
				}
			}

			cv::Mat display = cv::Mat::zeros(height*R,width*C,CV_8UC1);

			int i = 0;
			for(int y = 0;y<R;++y)
			{
				if(i >= images.size()) break;
				for(int x = 0;x<C;++x,++i)
				{
					if(i >= images.size()) break;
					cv::Mat d = display(cv::Range(y*height,(y+1)*height),cv::Range(x*width,(x+1)*width));
					images[i].reshape(0,height).convertTo(d,CV_8UC1,255.0);
				}
			}

			ss.str("");
			ss << "Normal";
			cv::imshow(ss.str(),display);
			cv::moveWindow(ss.str(),100,500);
		}

		std::cout << "Press Esc to exit." << std::endl;
		char c = cv::waitKey(0);	
	}

}
