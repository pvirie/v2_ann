Patrick Virie's opencv-based neural network library. This project entails the use of experimental GPU implementation of convolutional neural networks, LSTMs, and deep belief networks.

Version 1.0.0

Prerequisite:

- CMake
- OpenCV 2 (also support OpenCV 3)
- Cuda SDK (tested with 6.5+)

Run test:

- Clone this repository and browse to the project's directory.
- mkdir build
- cd build
- cmake ..
- make (this will generate a test file at ../bin

Experiment:

- Hand-drawing style : output styled paths <- input unstyled paths
- Train a few layers of reconstruction training on styled paths
- Then conditional reconstruction training on unstyled paths
- Convolutional RBM at the top
- Deep LSTM

Contact:
p.virie@gmail.com