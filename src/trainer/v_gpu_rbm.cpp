#include "trainer/v_gpu_rbm.h"
#include "trainer/v_gpu_util.h"
#include <iostream>
#include <cmath>
#include <limits>
#include <ctime>

namespace cv_ann
{

	cv::Mat rbm_train(
		cv::Mat data,cv::Mat W,cv::Mat B,cv::Mat Bp,
		std::function<cv::Mat(cv::Mat)> A,std::function<cv::Mat(cv::Mat)> Ap,std::function<cv::Mat(cv::Mat)> S,
		int _max_iteration, int _cd_round, double _learning_rate, double _momentum_weight, double _lambda, bool _with_bias)
    {
        std::cout << "Training an RBM with GPU ...";
		
		cv::Mat float_mat; 
		GpuMat gW,gB,gBp;
		W.convertTo(float_mat,CV_32FC1);
		gW.upload(float_mat);
		B.convertTo(float_mat,CV_32FC1);
		gB.upload(float_mat);
		Bp.convertTo(float_mat,CV_32FC1);
		gBp.upload(float_mat);

		data.convertTo(float_mat,CV_32FC1);
		GpuMat V; V.upload(float_mat);

        GpuMat dW,dB,H_,V_,G_;
		GpuMat Vw = GpuMat(W.rows,W.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vb = GpuMat(B.rows,1,CV_32FC1); Vb.setTo(0);
		
		GpuMat one_row_data = GpuMat(1,V.cols,CV_32FC1); one_row_data.setTo(1);
		GpuMat one_col_data = GpuMat(V.cols,1,CV_32FC1); one_col_data.setTo(1);

		GpuMat B_expand,B_grad,H_grad,H_data,H_data_;
		GpuMat V_data,V_data_;
        GpuMat positive;

		GpuMat emp = GpuMat();

		GPU_Random rand_seed(time(0),W.rows,V.cols);

		for(unsigned int i = 1;i<=_max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

            double time_factor = i*1.0/_max_iteration;
            double momentum = (time_factor*_momentum_weight + (1-time_factor)*0.5);
            unsigned int cd_count = static_cast<unsigned int>(_cd_round*time_factor + 1);

			cv_gpu::gemm(gB,one_row_data,1,emp,0,B_expand);
			cv_gpu::gemm(gW,V,1,B_expand,1,H_data);
			sigmoidm(H_data,H_,H_data_);

			cv_gpu::gemm(H_,V,1.0/V.cols,emp,0,positive,cv::GEMM_2_T);
			if(_with_bias) H_.copyTo(G_);

            for(unsigned int j = 0;j<cd_count;++j)
            {
				sample_bernoulli(H_,H_data,rand_seed);
				cv_gpu::gemm(gW,H_data,1,gBp,1,V_data,cv::GEMM_1_T);
				sigmoidm(V_data,V_,V_data_);
				cv_gpu::gemm(gW,V_,1,B_expand,1,H_data);
				sigmoidm(H_data,H_,H_data_);
            }

            //compute the gradients (positive - negative) - penalization
			
			cv_gpu::gemm(H_,V,1.0/V.cols,emp,0,H_grad,cv::GEMM_2_T);
			subtract(positive,H_grad,dW);
			addWeighted(dW,1,gW,-1.0*_lambda,0,dW);
			addWeighted(Vw,momentum,dW,1,0,Vw);
            addWeighted(gW,1,Vw,_learning_rate,0,gW);

            if(_with_bias)
            {
                //compute the gradients (positive - negative) - penalization
				subtract(G_,H_,H_data);
				cv_gpu::gemm(H_data,one_col_data,1.0/H_.cols,emp,0,B_grad);
                addWeighted(B_grad,1,gB,-1.0*_lambda,0,dB);
				addWeighted(Vb,momentum,dB,1,0,Vb);
                addWeighted(gB,1,Vb,_learning_rate,0,gB);
            }

            if(!(i%100)) std::cout<< " " << i << std::flush;
        }
        std::cout << std::endl;

		cv::Mat oW,oB;
		gW.download(oW);
		gB.download(oB);
		oW.convertTo(W,W.type());
		oB.convertTo(B,B.type());
        return S(W*data + B*cv::Mat::ones(1,V.cols,W.type()));
    }


    cv::Mat bimodal_rbm_train(
        cv::Mat data,cv::Mat condition,cv::Mat W,cv::Mat B, cv::Mat U,cv::Mat Bp,
        std::function<cv::Mat(cv::Mat)> A,std::function<cv::Mat(cv::Mat)> Ap,std::function<cv::Mat(cv::Mat)> S,
        int _max_iteration, int _cd_round, double _learning_rate, double _momentum_weight, double _lambda, bool _with_bias)
    {
        std::cout << "Training an RBM with GPU ...";

        cv::Mat float_mat;
        GpuMat gW,gB,gU,gBp;
        W.convertTo(float_mat,CV_32FC1);
        gW.upload(float_mat);
        B.convertTo(float_mat,CV_32FC1);
        gB.upload(float_mat);
        U.convertTo(float_mat,CV_32FC1);
        gU.upload(float_mat);
        Bp.convertTo(float_mat,CV_32FC1);
        gBp.upload(float_mat);

        data.convertTo(float_mat,CV_32FC1);
        GpuMat V; V.upload(float_mat);
        condition.convertTo(float_mat,CV_32FC1);
        GpuMat C; C.upload(float_mat);

        GpuMat dW,dB,dU,H_,U_,V_,G_;
        GpuMat Vw = GpuMat(W.rows,W.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vu = GpuMat(U.rows,U.cols,CV_32FC1); Vu.setTo(0);
        GpuMat Vb = GpuMat(B.rows,1,CV_32FC1); Vb.setTo(0);

        GpuMat one_row_data = GpuMat(1,V.cols,CV_32FC1); one_row_data.setTo(1);
        GpuMat one_col_data = GpuMat(V.cols,1,CV_32FC1); one_col_data.setTo(1);

        GpuMat B_expand,H_grad,H_data,H_data_,UC_;
        GpuMat V_data,V_data_,U_data;
        GpuMat positive, positive_C;

        GpuMat emp = GpuMat();

        GPU_Random rand_seed(time(0),W.rows,V.cols);

        for(int i = 1;i<=_max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

            double time_factor = i*1.0/_max_iteration;
            double momentum = (time_factor*_momentum_weight + (1-time_factor)*0.5);
            unsigned int cd_count = static_cast<unsigned int>(_cd_round*time_factor + 1);

            cv_gpu::gemm(gB,one_row_data,1,emp,0,B_expand);
            cv_gpu::gemm(gW,V,1,B_expand,1,H_);
            cv_gpu::gemm(gU,C,1,emp,0,UC_);
            add(H_,UC_,H_data);
            sigmoidm(H_data,H_,H_data_);

            cv_gpu::gemm(H_,V,1.0/V.cols,emp,0,positive,cv::GEMM_2_T);
            cv_gpu::gemm(H_,C,1.0/C.cols,emp,0,positive_C,cv::GEMM_2_T);
            if(_with_bias) H_.copyTo(G_);

            for(unsigned int j = 0;j<cd_count;++j)
            {
                sample_bernoulli(H_,H_data,rand_seed);
                cv_gpu::gemm(gW,H_data,1,gBp,1,V_data,cv::GEMM_1_T);
                cv_gpu::gemm(gU,H_data,1,emp,0,U_data,cv::GEMM_1_T);
                sigmoidm(V_data,V_,V_data_);
                sigmoidm(U_data,U_,V_data_);

                cv_gpu::gemm(gW,V_,1,B_expand,1,H_);
                cv_gpu::gemm(gU,U_,1,emp,0,UC_);
                add(H_,UC_,H_data);
                sigmoidm(H_data,H_,H_data_);
            }

            //compute the gradients (positive - negative) - penalization

            cv_gpu::gemm(H_,V,1.0/V.cols,emp,0,H_grad,cv::GEMM_2_T);
            subtract(positive,H_grad,dW);
            addWeighted(dW,1,gW,-1.0*_lambda,0,dW);
            addWeighted(Vw,momentum,dW,1,0,Vw);
            addWeighted(gW,1,Vw,_learning_rate,0,gW);

            cv_gpu::gemm(H_,C,1.0/C.cols,emp,0,H_grad,cv::GEMM_2_T);
            subtract(positive_C,H_grad,dU);
            addWeighted(dU,1,gU,-1.0*_lambda,0,dU);
            addWeighted(Vu,momentum,dU,1,0,Vu);
            addWeighted(gU,1,Vu,_learning_rate,0,gU);

            if(_with_bias)
            {
                subtract(G_,H_,H_data);
                cv_gpu::gemm(H_data,one_col_data,1.0/H_.cols,emp,0,dB);
                addWeighted(dB,1,gB,-1.0*_lambda,0,dB);
                addWeighted(Vb,momentum,dB,1,0,Vb);
                addWeighted(gB,1,Vb,_learning_rate,0,gB);
            }

            if(!(i%100)) std::cout<< " " << i << std::flush;
        }
        std::cout << std::endl;

        cv::Mat oW,oB,oU;
        gW.download(oW);
        gB.download(oB);
        gU.download(oU);
        oW.convertTo(W,W.type());
        oB.convertTo(B,B.type());
        oU.convertTo(U,U.type());
        return S(W*data + B*cv::Mat::ones(1,V.cols,W.type()) + U*condition);
    }


    cv::Mat conditional_rbm_train(
        cv::Mat data,cv::Mat condition,cv::Mat W,cv::Mat B, cv::Mat U,cv::Mat Bp,
        std::function<cv::Mat(cv::Mat)> A,std::function<cv::Mat(cv::Mat)> Ap,std::function<cv::Mat(cv::Mat)> S,
        int _max_iteration, int _cd_round, double _learning_rate, double _momentum_weight, double _lambda, bool _with_bias)
    {
        std::cout << "Training an RBM with GPU ...";

        cv::Mat float_mat;
        GpuMat gW,gB,gU,gBp;
        W.convertTo(float_mat,CV_32FC1);
        gW.upload(float_mat);
        B.convertTo(float_mat,CV_32FC1);
        gB.upload(float_mat);
        U.convertTo(float_mat,CV_32FC1);
        gU.upload(float_mat);
        Bp.convertTo(float_mat,CV_32FC1);
        gBp.upload(float_mat);

        data.convertTo(float_mat,CV_32FC1);
        GpuMat V; V.upload(float_mat);
        condition.convertTo(float_mat,CV_32FC1);
        GpuMat C; C.upload(float_mat);

        GpuMat dW,dB,dU,H_,X,V_,G_;
        GpuMat Vw = GpuMat(W.rows,W.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vu = GpuMat(U.rows,U.cols,CV_32FC1); Vu.setTo(0);
        GpuMat Vb = GpuMat(B.rows,1,CV_32FC1); Vb.setTo(0);

        GpuMat one_row_data = GpuMat(1,V.cols,CV_32FC1); one_row_data.setTo(1);
        GpuMat one_col_data = GpuMat(V.cols,1,CV_32FC1); one_col_data.setTo(1);

        GpuMat B_expand,H_grad,H_data,H_data_,UC_;
        GpuMat V_data,V_data_;
        GpuMat positive;

        GpuMat emp = GpuMat();

        GPU_Random rand_seed(time(0),W.rows,V.cols);

        for(int i = 1;i<=_max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

            double time_factor = i*1.0/_max_iteration;
            double momentum = (time_factor*_momentum_weight + (1-time_factor)*0.5);
            unsigned int cd_count = static_cast<unsigned int>(_cd_round*time_factor + 1);

            cv_gpu::gemm(gB,one_row_data,1,emp,0,B_expand);
            cv_gpu::gemm(gW,V,1,B_expand,1,H_);
            cv_gpu::gemm(gU,C,1,emp,0,UC_);
            add(H_,UC_,H_data);
            sigmoidm(H_data,H_,H_data_);

            cv_gpu::gemm(H_,V,1.0/V.cols,emp,0,positive,cv::GEMM_2_T);
            H_.copyTo(G_);

            for(unsigned int j = 0;j<cd_count;++j)
            {
                sample_bernoulli(H_,H_data,rand_seed);
                cv_gpu::gemm(gW,H_data,1,gBp,1,V_data,cv::GEMM_1_T);
                sigmoidm(V_data,V_,V_data_);
                cv_gpu::gemm(gW,V_,1,B_expand,1,H_);
                add(H_,UC_,H_data);
                sigmoidm(H_data,H_,H_data_);
            }

            //compute the gradients (positive - negative) - penalization

            cv_gpu::gemm(H_,V,1.0/V.cols,emp,0,H_grad,cv::GEMM_2_T);
            subtract(positive,H_grad,dW);
            addWeighted(dW,1,gW,-1.0*_lambda,0,dW);
            addWeighted(Vw,momentum,dW,1,0,Vw);
            addWeighted(gW,1,Vw,_learning_rate,0,gW);

            subtract(G_,H_,H_data);
            cv_gpu::gemm(H_data,C,1.0/H_.cols,emp,0,dU,cv::GEMM_2_T);
            addWeighted(dU,1,gU,-1.0*_lambda,0,dU);
            addWeighted(Vu,momentum,dU,1,0,Vu);
            addWeighted(gU,1,Vu,_learning_rate,0,gU);

            if(_with_bias)
            {
                cv_gpu::gemm(H_data,one_col_data,1.0/H_.cols,emp,0,dB);
                addWeighted(dB,1,gB,-1.0*_lambda,0,dB);
                addWeighted(Vb,momentum,dB,1,0,Vb);
                addWeighted(gB,1,Vb,_learning_rate,0,gB);
            }

            if(!(i%100)) std::cout<< " " << i << std::flush;
        }
        std::cout << std::endl;

        cv::Mat oW,oB,oU;
        gW.download(oW);
        gB.download(oB);
        gU.download(oU);
        oW.convertTo(W,W.type());
        oB.convertTo(B,B.type());
        oU.convertTo(U,U.type());
        return S(W*data + B*cv::Mat::ones(1,V.cols,W.type()) + U*condition);
    }


	void rbm_relu_train(GpuMat V, GpuMat C, GpuMat gW, GpuMat gB, GpuMat gU, GpuMat gBp, V_Parameter_RBM params)
    {
        std::cout << "Training a rectified linear RBM with GPU ...";

        GpuMat dW,dB,dU,H_,X,V_,G_;
        GpuMat Vw = GpuMat(gW.rows,gW.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vu = GpuMat(gU.rows,gU.cols,CV_32FC1); Vu.setTo(0);
        GpuMat Vb = GpuMat(gB.rows,1,CV_32FC1); Vb.setTo(0);

        GpuMat one_row_data = GpuMat(1,V.cols,CV_32FC1); one_row_data.setTo(1);
        GpuMat one_col_data = GpuMat(V.cols,1,CV_32FC1); one_col_data.setTo(1);

        GpuMat B_expand,H_grad,H_data,H_data_,UC_;
        GpuMat V_data,V_data_;
        GpuMat positive;

        GpuMat emp = GpuMat();

        GPU_Random rand_seed(time(0),gW.rows,V.cols);

        for(int i = 1;i<=params.max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

            double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);
            unsigned int cd_count = static_cast<unsigned int>(params.cd_round*time_factor + 1);

            cv_gpu::gemm(gB,one_row_data,1,emp,0,B_expand);
            cv_gpu::gemm(gW,V,1,B_expand,1,H_);
            cv_gpu::gemm(gU,C,1,emp,0,UC_);
            add(H_,UC_,H_data);
            relum(H_data,H_);

            cv_gpu::gemm(H_,V,1.0/V.cols,emp,0,positive,cv::GEMM_2_T);
            H_.copyTo(G_);

            for(unsigned int j = 0;j<cd_count;++j)
            {
				sample_gaussian(H_, H_data, rand_seed); //only copy for now
                cv_gpu::gemm(gW,H_data,1,gBp,1,V_data,cv::GEMM_1_T);
                relum(V_data,V_);
                cv_gpu::gemm(gW,V_,1,B_expand,1,H_);
                add(H_,UC_,H_data);
                relum(H_data,H_);
            }

            //compute the gradients (positive - negative) - penalization

            cv_gpu::gemm(H_,V,1.0/V.cols,emp,0,H_grad,cv::GEMM_2_T);
            subtract(positive,H_grad,dW);
            addWeighted(dW,1,gW,-1.0*params.lambda,0,dW);
            addWeighted(Vw,momentum,dW,1,0,Vw);
            addWeighted(gW,1,Vw,params.learning_rate,0,gW);

            subtract(G_,H_,H_data);
            cv_gpu::gemm(H_data,C,1.0/H_.cols,emp,0,dU,cv::GEMM_2_T);
            addWeighted(dU,1,gU,-1.0*params.lambda,0,dU);
            addWeighted(Vu,momentum,dU,1,0,Vu);
            addWeighted(gU,1,Vu,params.learning_rate,0,gU);

            if(params.with_bias)
            {
                cv_gpu::gemm(H_data,one_col_data,1.0/H_.cols,emp,0,dB);
                addWeighted(dB,1,gB,-1.0*params.lambda,0,dB);
                addWeighted(Vb,momentum,dB,1,0,Vb);
                addWeighted(gB,1,Vb,params.learning_rate,0,gB);
            }

            if(!(i%100)) std::cout<< " " << i << std::flush;
        }
        std::cout << std::endl;

    }

	//minibatch
	void rbm_relu_train(std::vector<GpuMat> Vs, std::vector<GpuMat> Cs, GpuMat gW, GpuMat gB, GpuMat gU, std::vector<GpuMat> gBps, V_Parameter_RBM params)
    {
        std::cout << "Training a rectified linear RBM with GPU ...";

        GpuMat dW,dB,dU;
        GpuMat Vw = GpuMat(gW.rows,gW.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vu = GpuMat(gU.rows,gU.cols,CV_32FC1); Vu.setTo(0);
        GpuMat Vb = GpuMat(gB.rows,1,CV_32FC1); Vb.setTo(0);

		std::vector<GpuMat> one_row_data; one_row_data.resize(Vs.size()); 
		std::vector<GpuMat> one_col_data; one_col_data.resize(Vs.size());
        std::vector<GPU_Random> rand_seed;
		for(unsigned int j = 0;j<Vs.size();++j) 
		{ 
			one_row_data[j] = GpuMat(1,Vs[j].cols,CV_32FC1); one_row_data[j].setTo(1); 
			one_col_data[j] = GpuMat(Vs[j].cols,1,CV_32FC1); one_col_data[j].setTo(1);
			rand_seed.push_back(GPU_Random(time(0),gW.rows,Vs[j].cols));
		}

		std::vector<GpuMat> V_; V_.resize(Vs.size());
		std::vector<GpuMat> H_; H_.resize(Vs.size());
		std::vector<GpuMat> UC_; UC_.resize(Vs.size());
		std::vector<GpuMat> X; X.resize(Vs.size());
		std::vector<GpuMat> G_; G_.resize(Vs.size());
		std::vector<GpuMat> B_expand; B_expand.resize(Vs.size());
		std::vector<GpuMat> H_grad; H_grad.resize(Vs.size());
		std::vector<GpuMat> H_data; H_data.resize(Vs.size());
		std::vector<GpuMat> H_data_; H_data_.resize(Vs.size());
		std::vector<GpuMat> V_data; V_data.resize(Vs.size());
		std::vector<GpuMat> V_data_; V_data_.resize(Vs.size());
		std::vector<GpuMat> positive; positive.resize(Vs.size());

        GpuMat emp = GpuMat();

        for(int i = 1;i<=params.max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

            double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);
            unsigned int cd_count = static_cast<unsigned int>(params.cd_round*time_factor + 1);

			for(int j = 0;j<Vs.size();++j)
			{
				cv_gpu::gemm(gB,one_row_data[j],1,emp,0,B_expand[j]);
				cv_gpu::gemm(gW,Vs[j],1,B_expand[j],1,H_[j]);
				cv_gpu::gemm(gU,Cs[j],1,emp,0,UC_[j]);
				add(H_[j],UC_[j],H_data[j]);
				relum(H_data[j],H_[j]);

				cv_gpu::gemm(H_[j],Vs[j],1.0/Vs[j].cols,emp,0,positive[j],cv::GEMM_2_T);
				H_[j].copyTo(G_[j]);

				for(unsigned int k = 0;k<cd_count;++k)
				{
					sample_gaussian(H_[j], H_data[j], rand_seed[j]);//only copy for now
					cv_gpu::gemm(gW,H_data[j],1,gBps[j],1,V_data[j],cv::GEMM_1_T);
					relum(V_data[j],V_[j]);
					cv_gpu::gemm(gW,V_[j],1,B_expand[j],1,H_[j]);
					add(H_[j],UC_[j],H_data[j]);
					relum(H_data[j],H_[j]);
				}

				//compute the gradients (positive - negative) - penalization

				cv_gpu::gemm(H_[j],Vs[j],1.0/Vs[j].cols,emp,0,H_grad[j],cv::GEMM_2_T);
				subtract(positive[j],H_grad[j],dW);
				addWeighted(dW,1,gW,-1.0*params.lambda,0,dW);
				addWeighted(Vw,momentum,dW,1,0,Vw);
				addWeighted(gW,1,Vw,params.learning_rate,0,gW);

				subtract(G_[j],H_[j],H_data[j]);
				cv_gpu::gemm(H_data[j],Cs[j],1.0/H_[j].cols,emp,0,dU,cv::GEMM_2_T);
				addWeighted(dU,1,gU,-1.0*params.lambda,0,dU);
				addWeighted(Vu,momentum,dU,1,0,Vu);
				addWeighted(gU,1,Vu,params.learning_rate,0,gU);

				if(params.with_bias)
				{
					cv_gpu::gemm(H_data[j],one_col_data[j],1.0/H_[j].cols,emp,0,dB);
					addWeighted(dB,1,gB,-1.0*params.lambda,0,dB);
					addWeighted(Vb,momentum,dB,1,0,Vb);
					addWeighted(gB,1,Vb,params.learning_rate,0,gB);
				}
			}

            if(!(i%100)) std::cout<< " " << i << std::flush;
        }
        std::cout << std::endl;

    }


}