#include "trainer/v_gpu_auc.h"
#include "trainer/v_gpu_util.h"
#include <iostream>
#include <cmath>
#include <limits>
#include <ctime>

namespace cv_ann
{

	void auc_non_sharing_relu_train(GpuMat V, GpuMat gWr, GpuMat gWg,GpuMat gB,GpuMat gBp, V_Parameter_AUC params)
    {
        std::cout << "Training a rectified linear auto-encoder with GPU ..." << std::endl;
        std::cout << "Please use low learning rate like 0.001!";

        GpuMat dB,H_,V_;
		GpuMat Vwr = GpuMat(gWr.rows,gWr.cols,CV_32FC1); Vwr.setTo(0);
		GpuMat Vwg = GpuMat(gWg.rows,gWg.cols,CV_32FC1); Vwg.setTo(0);
        GpuMat Vb = GpuMat(gB.rows,1,CV_32FC1); Vb.setTo(0);
		
		GpuMat one_row_data = GpuMat(1,V.cols,CV_32FC1); one_row_data.setTo(1);
		GpuMat one_col_data = GpuMat(V.cols,1,CV_32FC1); one_col_data.setTo(1);

        GpuMat dV_,dWgT,dH,dWg,dHdB;
		GpuMat dWr;
		GpuMat H_grad,H_data;
		GpuMat V_data,V_data_;

		GpuMat emp = GpuMat();

        double last_error = std::numeric_limits<double>::max(),error;
		for(unsigned int i = 1;i<=params.max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);
			
            //forward pass
			cv_gpu::gemm(gB,one_row_data,1,emp,0,H_data);
			cv_gpu::gemm(gWr,V,1,H_data,1,H_data);
			relum(H_data,H_);
			cv_gpu::gemm(gWg,H_,1,gBp,1,V_data,cv::GEMM_1_T);
			relum(V_data,V_);

            //propagate
            subtract(V_,V,dV_); // only support sigmoid and Relu
			cv_gpu::gemm(dV_,H_,1.0/H_.cols,emp,0,dWgT,cv::GEMM_2_T);
			relum_grad(H_,H_grad);
			cv_gpu::gemm(gWg,dV_,1,emp,0,H_data);
			multiply(H_grad,H_data,dH);
			cv_gpu::gemm(dH,V,1.0/V.cols,emp,0,dWr,cv::GEMM_2_T);

            //update
			addWeighted(Vwr,momentum,dWr,-1,0,Vwr);
			addWeighted(gWr,1,Vwr,params.learning_rate,0,gWr);
			
			transpose(dWgT,dWg);
			addWeighted(Vwg,momentum,dWg,-1,0,Vwg);
			addWeighted(gWg,1,Vwg,params.learning_rate,0,gWg);

			//bias
			if(params.with_bias)
            {
				cv_gpu::gemm(dH,one_col_data,1.0/H_.cols,emp,0,dHdB);
				//addWeighted(dHdB,1,gB,params.lambda,0,dB); //L2 regularization
				cv_gpu::gemm(H_grad,one_col_data,params.lambda/H_.cols,dHdB,1,dB); //L1 sparsity (not regularization)
				addWeighted(Vb,momentum,dB,-1,0,Vb);
				addWeighted(gB,1,Vb,params.learning_rate,0,gB);
            }

            //compute error
            error = ave_sqr_sum(dV_,V_data_,V.cols);
            if(!(i%100)) std::cout<< " " << error << std::flush;
			if(abs(error-last_error) < params.error_threshold)
			{
                std::cout << " " << error << std::flush;
				break;
			}
            last_error = error;
        }
        std::cout << std::endl;
		
    }

	void auc_non_sharing_tanh_train(GpuMat V, GpuMat gWr, GpuMat gWg,GpuMat gB,GpuMat gBp, V_Parameter_AUC params)
    {
        std::cout << "Training a rectified linear auto-encoder with GPU ..." << std::endl;
        std::cout << "Please use low learning rate like 0.001!";

        GpuMat dB,H_,V_;
		GpuMat Vwr = GpuMat(gWr.rows,gWr.cols,CV_32FC1); Vwr.setTo(0);
		GpuMat Vwg = GpuMat(gWg.rows,gWg.cols,CV_32FC1); Vwg.setTo(0);
        GpuMat Vb = GpuMat(gB.rows,1,CV_32FC1); Vb.setTo(0);
		
		GpuMat one_row_data = GpuMat(1,V.cols,CV_32FC1); one_row_data.setTo(1);
		GpuMat one_col_data = GpuMat(V.cols,1,CV_32FC1); one_col_data.setTo(1);

        GpuMat dV_,dWgT,dH,dWg,dHdB;
		GpuMat dWr;
		GpuMat H_grad,H_data;
		GpuMat V_data,V_data_;

		GpuMat emp = GpuMat(), buf_H, buf_V;

        double last_error = std::numeric_limits<double>::max(),error;
		for(unsigned int i = 1;i<=params.max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);
			
            //forward pass
			cv_gpu::gemm(gB,one_row_data,1,emp,0,H_data);
			cv_gpu::gemm(gWr,V,1,H_data,1,H_data);
			tanhm(H_data,H_, buf_H);
			cv_gpu::gemm(gWg,H_,1,gBp,1,V_data,cv::GEMM_1_T);
			tanhm(V_data,V_, buf_V);

            //propagate
            subtract(V_,V,dV_); // only support sigmoid and Relu
			cv_gpu::gemm(dV_,H_,1.0/H_.cols,emp,0,dWgT,cv::GEMM_2_T);
			tanhm_grad(H_,H_grad, buf_H);
			cv_gpu::gemm(gWg,dV_,1,emp,0,H_data);
			multiply(H_grad,H_data,dH);
			cv_gpu::gemm(dH,V,1.0/V.cols,emp,0,dWr,cv::GEMM_2_T);

            //update
			addWeighted(Vwr,momentum,dWr,-1,0,Vwr);
			addWeighted(gWr,1,Vwr,params.learning_rate,0,gWr);
			
			transpose(dWgT,dWg);
			addWeighted(Vwg,momentum,dWg,-1,0,Vwg);
			addWeighted(gWg,1,Vwg,params.learning_rate,0,gWg);

			//bias
			if(params.with_bias)
            {
				cv_gpu::gemm(dH,one_col_data,1.0/H_.cols,emp,0,dHdB);
				//addWeighted(dHdB,1,gB,params.lambda,0,dB); //L2 regularization
				cv_gpu::gemm(H_grad,one_col_data,params.lambda/H_.cols,dHdB,1,dB); //L1 sparsity (not regularization)
				addWeighted(Vb,momentum,dB,-1,0,Vb);
				addWeighted(gB,1,Vb,params.learning_rate,0,gB);
            }

            //compute error
            error = ave_sqr_sum(dV_,V_data_,V.cols);
            if(!(i%100)) std::cout<< " " << error << std::flush;
			if(abs(error-last_error) < params.error_threshold)
			{
                std::cout << " " << error << std::flush;
				break;
			}
            last_error = error;
        }
        std::cout << std::endl;
		
    }

}
