#include "trainer/v_gpu_bp.h"
#include "trainer/v_gpu_util.h"
#include <iostream>
#include <cmath>
#include <limits>
#include <ctime>

namespace cv_ann
{

	void backpropagation(
		GpuMat data, GpuMat label, std::vector<GpuMat>& Ws, std::vector<GpuMat>& Bs, V_Parameter_BP params
		)
	{
		std::cout << "Training backpropagation with GPU ..." << std::endl;
			
		std::vector<GpuMat> dW; dW.resize(Ws.size());
		std::vector<GpuMat> dB; dB.resize(Bs.size());
		std::vector<GpuMat> dHdW; dHdW.resize(Ws.size());
		std::vector<GpuMat> dHdB; dHdB.resize(Bs.size());
		std::vector<GpuMat> Vw; Vw.resize(Ws.size());
		std::vector<GpuMat> Vb; Vb.resize(Bs.size());
		for (unsigned int j = 0; j<Ws.size(); ++j)
		{
			Vw[j] = GpuMat(Ws[j].rows, Ws[j].cols, CV_32FC1); Vw[j].setTo(0);
			Vb[j] = GpuMat(Bs[j].rows, Bs[j].cols, CV_32FC1); Vb[j].setTo(0);
		}

		std::vector<GpuMat> H_; H_.resize(Ws.size()+1);
		std::vector<GpuMat> dH; dH.resize(Ws.size()+1);
		std::vector<GpuMat> H_grad; H_grad.resize(Ws.size()+1);
		std::vector<GpuMat> H_data; H_data.resize(Ws.size()+1);

		GpuMat emp = GpuMat();
		GpuMat ones = GpuMat(1, data.cols, CV_32FC1); ones.setTo(1);

		std::function<double(int, double)> recursive_kernel = [&](
				int depth,
				double momentum
			) -> double
		{
			if (depth == Ws.size())
			{
				relum_grad(H_[depth], H_grad[depth]);
				//subtract(H_,H,H_data); // only support sigmoid and Relu
				//multiply(H_grad,H_data,dH);
				subtract(H_[depth], label, dH[depth]); //current dH is wrong

				return ave_sqr_sum(dH[depth], H_data[depth],H_[depth].cols);
			}

			//forward pass
			cv_gpu::gemm(Bs[depth], ones, 1, emp, 0, H_data[depth + 1]);
			cv_gpu::gemm(Ws[depth], H_[depth], 1, H_data[depth + 1], 1, H_data[depth + 1]);
			relum(H_data[depth + 1], H_[depth + 1]);

			double error = recursive_kernel(depth + 1, momentum);

			//propagate
			cv_gpu::gemm(dH[depth + 1], H_[depth], 1.0 / H_[depth].cols, emp, 0, dHdW[depth], cv::GEMM_2_T);
			cv_gpu::gemm(dH[depth + 1], ones, 1.0 / ones.cols, emp, 0, dHdB[depth], cv::GEMM_2_T);

			//update
			//addWeighted(dW,1,gW,params.lambda,0,dW); //L2 regularization
			
			//cv_gpu::gemm(H_grad[depth + 1], H_[depth], params.lambda / H_[depth].cols, emp, 0, W_[depth], cv::GEMM_2_T); //L1 sparsity (not regularization)
			//cv_gpu::gemm(H_grad[depth + 1], C_[depth], params.lambda / C_[depth].cols, emp, 0, A_[depth], cv::GEMM_2_T); //L1 sparsity (not regularization)

			//add(dHdW[depth], W_[depth], dW[depth]);
			//addWeighted(Vw[depth], momentum, dW[depth], -1, 0, Vw[depth]);
			addWeighted(Vw[depth], momentum, dHdW[depth], -1, 0, Vw[depth]);
			addWeighted(Ws[depth], 1, Vw[depth], params.learning_rate, 0, Ws[depth]);

			//add(dHdA[depth], A_[depth], dA[depth]);
			//addWeighted(Va[depth], momentum, dA[depth], -1, 0, Va[depth]);
			addWeighted(Vb[depth], momentum, dHdB[depth], -1, 0, Vb[depth]);
			addWeighted(Bs[depth], 1, Vb[depth], params.learning_rate, 0, Bs[depth]);

			//propagate lower
			cv_gpu::gemm(Ws[depth], dH[depth + 1], 1.0 / dH[depth + 1].cols, emp, 0, H_data[depth], cv::GEMM_1_T);
			relum_grad(H_[depth], H_grad[depth]);
			multiply(H_grad[depth], H_data[depth], dH[depth]);

			return error;
		};
	

		double last_error = std::numeric_limits<double>::max(), error;
		for (unsigned int i = 1; i <= params.max_iteration; ++i)
		{
			if (!((i - 1) % 500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0 / params.max_iteration;
			double momentum = (time_factor*params.momentum + (1 - time_factor)*0.5);

			H_[0] = data;
			error = recursive_kernel(0, momentum);

			if (!(i % 100)) std::cout << " " << error << std::flush;
			if (abs(error - last_error) < params.error_threshold)
			{
				std::cout << " " << error << std::flush;
				break;
			}
			last_error = error;
		}
		std::cout << std::endl;

	}

	void backpropagation_tanh(
		GpuMat data, GpuMat label, std::vector<GpuMat>& Ws, std::vector<GpuMat>& Bs, V_Parameter_BP params
		)
	{
		std::cout << "Training backpropagation with GPU ..." << std::endl;
			
		std::vector<GpuMat> dW; dW.resize(Ws.size());
		std::vector<GpuMat> dB; dB.resize(Bs.size());
		std::vector<GpuMat> dHdW; dHdW.resize(Ws.size());
		std::vector<GpuMat> dHdB; dHdB.resize(Bs.size());
		std::vector<GpuMat> Vw; Vw.resize(Ws.size());
		std::vector<GpuMat> Vb; Vb.resize(Bs.size());
		for (unsigned int j = 0; j<Ws.size(); ++j)
		{
			Vw[j] = GpuMat(Ws[j].rows, Ws[j].cols, CV_32FC1); Vw[j].setTo(0);
			Vb[j] = GpuMat(Bs[j].rows, Bs[j].cols, CV_32FC1); Vb[j].setTo(0);
		}

		std::vector<GpuMat> H_; H_.resize(Ws.size()+1);
		std::vector<GpuMat> dH; dH.resize(Ws.size()+1);
		std::vector<GpuMat> H_grad; H_grad.resize(Ws.size()+1);
		std::vector<GpuMat> H_data; H_data.resize(Ws.size()+1);
		std::vector<GpuMat> buf; buf.resize(Ws.size()+1);

		GpuMat emp = GpuMat();
		GpuMat ones = GpuMat(1, data.cols, CV_32FC1); ones.setTo(1);

		std::function<double(int, double)> recursive_kernel = [&](
				int depth,
				double momentum
			) -> double
		{
			if (depth == Ws.size())
			{
				tanhm_grad(H_[depth], H_grad[depth], buf[depth]);
				//subtract(H_,H,H_data); // only support sigmoid and Relu
				//multiply(H_grad,H_data,dH);
				subtract(H_[depth], label, dH[depth]); //current dH is wrong

				return ave_sqr_sum(dH[depth], H_data[depth],H_[depth].cols);
			}

			//forward pass
			cv_gpu::gemm(Bs[depth], ones, 1, emp, 0, H_data[depth + 1]);
			cv_gpu::gemm(Ws[depth], H_[depth], 1, H_data[depth + 1], 1, H_data[depth + 1]);
			tanhm(H_data[depth + 1], H_[depth + 1], buf[depth+1]);

			double error = recursive_kernel(depth + 1, momentum);

			//propagate
			cv_gpu::gemm(dH[depth + 1], H_[depth], 1.0 / H_[depth].cols, emp, 0, dHdW[depth], cv::GEMM_2_T);
			cv_gpu::gemm(dH[depth + 1], ones, 1.0 / ones.cols, emp, 0, dHdB[depth], cv::GEMM_2_T);

			//update
			//addWeighted(dW,1,gW,params.lambda,0,dW); //L2 regularization
			
			//cv_gpu::gemm(H_grad[depth + 1], H_[depth], params.lambda / H_[depth].cols, emp, 0, W_[depth], cv::GEMM_2_T); //L1 sparsity (not regularization)
			//cv_gpu::gemm(H_grad[depth + 1], C_[depth], params.lambda / C_[depth].cols, emp, 0, A_[depth], cv::GEMM_2_T); //L1 sparsity (not regularization)

			//add(dHdW[depth], W_[depth], dW[depth]);
			//addWeighted(Vw[depth], momentum, dW[depth], -1, 0, Vw[depth]);
			addWeighted(Vw[depth], momentum, dHdW[depth], -1, 0, Vw[depth]);
			addWeighted(Ws[depth], 1, Vw[depth], params.learning_rate, 0, Ws[depth]);

			//add(dHdA[depth], A_[depth], dA[depth]);
			//addWeighted(Va[depth], momentum, dA[depth], -1, 0, Va[depth]);
			addWeighted(Vb[depth], momentum, dHdB[depth], -1, 0, Vb[depth]);
			addWeighted(Bs[depth], 1, Vb[depth], params.learning_rate, 0, Bs[depth]);

			//propagate lower
			cv_gpu::gemm(Ws[depth], dH[depth + 1], 1.0 / dH[depth + 1].cols, emp, 0, H_data[depth], cv::GEMM_1_T);
			tanhm_grad(H_[depth], H_grad[depth], buf[depth]);
			multiply(H_grad[depth], H_data[depth], dH[depth]);

			return error;
		};
	

		double last_error = std::numeric_limits<double>::max(), error;
		for (unsigned int i = 1; i <= params.max_iteration; ++i)
		{
			if (!((i - 1) % 500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0 / params.max_iteration;
			double momentum = (time_factor*params.momentum + (1 - time_factor)*0.5);

			H_[0] = data;
			error = recursive_kernel(0, momentum);

			if (!(i % 100)) std::cout << " " << error << std::flush;
			if (abs(error - last_error) < params.error_threshold)
			{
				std::cout << " " << error << std::flush;
				break;
			}
			last_error = error;
		}
		std::cout << std::endl;

	}



	void backpropagation(
		std::vector<GpuMat> Vs, 
		std::vector<GpuMat> condition, 
		std::vector<GpuMat> Ws, 
		std::vector<GpuMat> As,
		std::vector<int> num_recursions,
		int final_depth,
		V_Parameter_BP params
		)
	{
		std::cout << "Training backpropagation on conditional layers with GPU ..." << std::endl;

		std::vector<GpuMat> dW; dW.resize(final_depth);
		std::vector<GpuMat> dA; dA.resize(final_depth);
		std::vector<GpuMat> W_; W_.resize(final_depth);
		std::vector<GpuMat> A_; A_.resize(final_depth);
		std::vector<GpuMat> dHdW; dHdW.resize(final_depth);
		std::vector<GpuMat> dHdA; dHdA.resize(final_depth);
		std::vector<GpuMat> Vw; Vw.resize(final_depth);
		std::vector<GpuMat> Va; Va.resize(final_depth);
		for (unsigned int j = 0; j<final_depth; ++j)
		{
			Vw[j] = GpuMat(Ws[j].rows, Ws[j].cols, CV_32FC1); Vw[j].setTo(0);
			Va[j] = GpuMat(As[j].rows, As[j].cols, CV_32FC1); Va[j].setTo(0);
		}

		std::vector<GpuMat> H_; H_.resize(final_depth + 1);
		std::vector<GpuMat> C_; C_.resize(final_depth);
		std::vector<GpuMat> dH; dH.resize(final_depth + 1);
		std::vector<GpuMat> H_grad; H_grad.resize(final_depth + 1);
		std::vector<GpuMat> H_data; H_data.resize(final_depth + 1);

		GpuMat emp = GpuMat();
		GpuMat dummy = GpuMat(Vs[0].rows, 1, CV_32FC1); dummy.setTo(0);

		std::function<double(int, int, double)> recursive_kernel = [&](
				int depth,
				int j,
				double momentum
			) -> double
		{
			if (depth == final_depth)
			{
				relum_grad(H_[depth], H_grad[depth]);
				//subtract(H_,H,H_data); // only support sigmoid and Relu
				//multiply(H_grad,H_data,dH);
				subtract(H_[depth], condition[j], dH[depth]); //current dH is wrong

				return ave_sqr_sum(dH[depth], H_data[depth], condition[j].cols);
			}

			//make C_ (history array)
			arrange_data(H_[depth], num_recursions[depth], dummy, C_[depth]);

			//forward pass
			cv_gpu::gemm(As[depth], C_[depth], 1, emp, 0, H_data[depth + 1]);
			cv_gpu::gemm(Ws[depth], H_[depth], 1, H_data[depth + 1], 1, H_data[depth + 1]);
			relum(H_data[depth + 1], H_[depth + 1]);

			double error = recursive_kernel(depth + 1, j, momentum);

			//propagate
			cv_gpu::gemm(dH[depth + 1], H_[depth], 1.0 / H_[depth].cols, emp, 0, dHdW[depth], cv::GEMM_2_T);
			cv_gpu::gemm(dH[depth + 1], C_[depth], 1.0 / C_[depth].cols, emp, 0, dHdA[depth], cv::GEMM_2_T);

			//update
			//addWeighted(dW,1,gW,params.lambda,0,dW); //L2 regularization
			
			//cv_gpu::gemm(H_grad[depth + 1], H_[depth], params.lambda / H_[depth].cols, emp, 0, W_[depth], cv::GEMM_2_T); //L1 sparsity (not regularization)
			//cv_gpu::gemm(H_grad[depth + 1], C_[depth], params.lambda / C_[depth].cols, emp, 0, A_[depth], cv::GEMM_2_T); //L1 sparsity (not regularization)

			//add(dHdW[depth], W_[depth], dW[depth]);
			//addWeighted(Vw[depth], momentum, dW[depth], -1, 0, Vw[depth]);
			addWeighted(Vw[depth], momentum, dHdW[depth], -1, 0, Vw[depth]);
			addWeighted(Ws[depth], 1, Vw[depth], params.learning_rate, 0, Ws[depth]);

			//add(dHdA[depth], A_[depth], dA[depth]);
			//addWeighted(Va[depth], momentum, dA[depth], -1, 0, Va[depth]);
			addWeighted(Va[depth], momentum, dHdA[depth], -1, 0, Va[depth]);
			addWeighted(As[depth], 1, Va[depth], params.learning_rate, 0, As[depth]);

			//propagate lower
			cv_gpu::gemm(Ws[depth], dH[depth + 1], 1.0 / dH[depth + 1].cols, emp, 0, H_data[depth], cv::GEMM_1_T);
			relum_grad(H_[depth], H_grad[depth]);
			multiply(H_grad[depth], H_data[depth], dH[depth]);

			return error;
		};

		double last_error = std::numeric_limits<double>::max(), error;
		for (unsigned int i = 1; i <= params.max_iteration; ++i)
		{
			if (!((i - 1) % 500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0 / params.max_iteration;
			double momentum = (time_factor*params.momentum + (1 - time_factor)*0.5);

			error = 0;
			for (int j = 0; j<Vs.size(); ++j)
			{
				H_[0] = Vs[j];
				//compute error
				error = error + recursive_kernel(0, j, momentum);
			}

			if (!(i % 100)) std::cout << " " << error << std::flush;
			if (abs(error - last_error) < params.error_threshold)
			{
				std::cout << " " << error << std::flush;
				break;
			}
			last_error = error;
		}
		std::cout << std::endl;

	}


}