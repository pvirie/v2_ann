#include "trainer/v_gpu_auc.h"
#include "trainer/v_gpu_util.h"
#include <iostream>
#include <cmath>
#include <limits>
#include <ctime>

namespace cv_ann
{

	cv::Mat auenc_train(
		cv::Mat data,cv::Mat W,cv::Mat B,cv::Mat Bp,
		std::function<cv::Mat(cv::Mat)> A,std::function<cv::Mat(cv::Mat)> Ap,std::function<cv::Mat(cv::Mat)> D,
		int _max_iteration, double _error_steady, double _learning_rate, double _momentum_weight, double _lambda, bool _with_bias)
    {
        std::cout << "Training an auto-encoder with GPU ...";

		cv::Mat float_mat; 
		GpuMat gW,gB,gBp;
		W.convertTo(float_mat,CV_32FC1);
		gW.upload(float_mat);
		B.convertTo(float_mat,CV_32FC1);
		gB.upload(float_mat);
		Bp.convertTo(float_mat,CV_32FC1);
		gBp.upload(float_mat);

		data.convertTo(float_mat,CV_32FC1);
		GpuMat V; V.upload(float_mat);
        GpuMat dW,dB,H_,V_;
		GpuMat Vw = GpuMat(W.rows,W.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vb = GpuMat(B.rows,1,CV_32FC1); Vb.setTo(0);
		
		GpuMat one_row_data = GpuMat(1,V.cols,CV_32FC1); one_row_data.setTo(1);
		GpuMat one_col_data = GpuMat(V.cols,1,CV_32FC1); one_col_data.setTo(1);

        GpuMat dV_,dV_dW,dH,dHdW,dHdB;
		GpuMat dV_dW_t;
		GpuMat H_grad,H_data,H_data_;
		GpuMat V_data,V_data_;

		GpuMat emp = GpuMat();

        double last_error = std::numeric_limits<double>::max(),error;
		for(unsigned int i = 1;i<=_max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

            double time_factor = i*1.0/_max_iteration;
            double momentum = (time_factor*_momentum_weight + (1-time_factor)*0.5);

            //forward pass
			cv_gpu::gemm(gB,one_row_data,1,emp,0,H_data);
			cv_gpu::gemm(gW,V,1,H_data,1,H_data);
			sigmoidm(H_data,H_,H_data_);
			cv_gpu::gemm(gW,H_,1,gBp,1,V_data,cv::GEMM_1_T);
			sigmoidm(V_data,V_,V_data_);

            //propagate
            subtract(V_,V,dV_); // only support sigmoid and Relu
			cv_gpu::gemm(dV_,H_,1.0/H_.cols,emp,0,dV_dW,cv::GEMM_2_T);
			sigmoidm_grad(H_,H_grad,H_data_);
			cv_gpu::gemm(gW,dV_,1,emp,0,H_data);
			multiply(H_grad,H_data,dH);
			cv_gpu::gemm(dH,V,1.0/V.cols,emp,0,dHdW,cv::GEMM_2_T);

            //update
			transpose(dV_dW,dV_dW_t);
			add(dV_dW_t,dHdW,dW);
			addWeighted(dW,1,gW,_lambda,0,dW);
			addWeighted(Vw,momentum,dW,-1,0,Vw);
            addWeighted(gW,1,Vw,_learning_rate,0,gW);

			//bias
            if(_with_bias)
            {
				cv_gpu::gemm(dH,one_col_data,1.0/H_.cols,emp,0,dHdB);
                addWeighted(dHdB,1,gB,_lambda,0,dB);
				addWeighted(Vb,momentum,dB,-1,0,Vb);
                addWeighted(gB,1,Vb,_learning_rate,0,gB);
            }

            //compute error
			sqr(dV_,V_data);
            error = sum(V_data,V_data_)[0]/V.cols;
            if(!(i%100)) std::cout<< " " << error << std::flush;
            if(abs(error-last_error) < _error_steady)
			{
                std::cout << " " << error << std::flush;
				break;
			}
            last_error = error;
        }
        std::cout << std::endl;
		
		cv::Mat oW,oB;
		gW.download(oW);
		gB.download(oB);
		oW.convertTo(W, W.type());
		oB.convertTo(B, B.type());
		return A(W*data + B*cv::Mat::ones(1,V.cols, W.type()));
    }

	cv::Mat conditional_auenc_train(
        cv::Mat data,cv::Mat condition,cv::Mat W,cv::Mat B, cv::Mat U,cv::Mat Bp,
		std::function<cv::Mat(cv::Mat)> A,std::function<cv::Mat(cv::Mat)> Ap,std::function<cv::Mat(cv::Mat)> D,
		int _max_iteration, double _error_steady, double _learning_rate, double _momentum_weight, double _lambda, bool _with_bias)
    {
        std::cout << "Training an auto-encoder with GPU ...";

		cv::Mat float_mat; 
		GpuMat gW,gB,gU,gBp;
		W.convertTo(float_mat,CV_32FC1);
		gW.upload(float_mat);
		B.convertTo(float_mat,CV_32FC1);
		gB.upload(float_mat);
        U.convertTo(float_mat,CV_32FC1);
        gU.upload(float_mat);
		Bp.convertTo(float_mat,CV_32FC1);
		gBp.upload(float_mat);

		data.convertTo(float_mat,CV_32FC1);
		GpuMat V; V.upload(float_mat);
        condition.convertTo(float_mat,CV_32FC1);
        GpuMat C; C.upload(float_mat);

        GpuMat dW,dB,dU,H_,V_;
		GpuMat Vw = GpuMat(W.rows,W.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vu = GpuMat(U.rows,U.cols,CV_32FC1); Vu.setTo(0);
        GpuMat Vb = GpuMat(B.rows,1,CV_32FC1); Vb.setTo(0);
		
		GpuMat one_row_data = GpuMat(1,V.cols,CV_32FC1); one_row_data.setTo(1);
		GpuMat one_col_data = GpuMat(V.cols,1,CV_32FC1); one_col_data.setTo(1);

        GpuMat dV_,dV_dW,dH,dHdW,dHdB,dHdU,UC_;
		GpuMat dV_dW_t;
		GpuMat B_expand,H_grad,H_data,H_data_;
		GpuMat V_data,V_data_;

		GpuMat emp = GpuMat();

        double last_error = std::numeric_limits<double>::max(),error;
		for(unsigned int i = 1;i<=_max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

            double time_factor = i*1.0/_max_iteration;
            double momentum = (time_factor*_momentum_weight + (1-time_factor)*0.5);

            //forward pass
			cv_gpu::gemm(gB,one_row_data,1,emp,0,B_expand);
			cv_gpu::gemm(gW,V,1,B_expand,1,H_data_);
            cv_gpu::gemm(gU,C,1,emp,0,UC_);
            add(H_data_,UC_,H_data);
			sigmoidm(H_data,H_,H_data_);
			cv_gpu::gemm(gW,H_,1,gBp,1,V_data,cv::GEMM_1_T);
			sigmoidm(V_data,V_,V_data_);

            //propagate
            subtract(V_,V,dV_); // only support sigmoid and Relu
			cv_gpu::gemm(dV_,H_,1.0/H_.cols,emp,0,dV_dW,cv::GEMM_2_T);
			sigmoidm_grad(H_,H_grad,H_data_);
			cv_gpu::gemm(gW,dV_,1,emp,0,H_data);
			multiply(H_grad,H_data,dH);
			cv_gpu::gemm(dH,V,1.0/V.cols,emp,0,dHdW,cv::GEMM_2_T);
			cv_gpu::gemm(dH,C,1.0/C.cols,emp,0,dHdU,cv::GEMM_2_T);

            //update
			transpose(dV_dW,dV_dW_t);
			add(dV_dW_t,dHdW,dW);
			addWeighted(dW,1,gW,_lambda,0,dW);
			addWeighted(Vw,momentum,dW,-1,0,Vw);
            addWeighted(gW,1,Vw,_learning_rate,0,gW);

			addWeighted(dHdU,1,gU,_lambda,0,dU);
			addWeighted(Vu,momentum,dU,-1,0,Vu);
            addWeighted(gU,1,Vu,_learning_rate,0,gU);

			//bias
            if(_with_bias)
            {
				cv_gpu::gemm(dH,one_col_data,1.0/H_.cols,emp,0,dHdB);
                addWeighted(dHdB,1,gB,_lambda,0,dB);
				addWeighted(Vb,momentum,dB,-1,0,Vb);
                addWeighted(gB,1,Vb,_learning_rate,0,gB);
            }

            //compute error
			sqr(dV_,V_data);
            error = sum(V_data,V_data_)[0]/V.cols;
            if(!(i%100)) std::cout<< " " << error << std::flush;
            if(abs(error-last_error) < _error_steady)
			{
                std::cout << " " << error << std::flush;
				break;
			}
            last_error = error;
        }
        std::cout << std::endl;
		
        cv::Mat oW,oB,oU;
        gW.download(oW);
        gB.download(oB);
        gU.download(oU);
        oW.convertTo(W, W.type());
        oB.convertTo(B, B.type());
        oU.convertTo(U, U.type());
		return A(W*data + U*condition + B*cv::Mat::ones(1,V.cols, W.type()));
    }

}
