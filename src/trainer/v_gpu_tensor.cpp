#include "trainer/v_gpu_tensor.h"
#include "trainer/v_gpu_util.h"
#include <iostream>
#include <sstream>
#include <limits>

using namespace cv;

namespace cv_ann
{
	V_GPU_Tensor::V_GPU_Tensor()
	{

	}

	V_GPU_Tensor::V_GPU_Tensor(int depth)
	{
		data.resize(depth);
	}

	V_GPU_Tensor::V_GPU_Tensor(GpuMat img)
	{
		push_back(img);
	}

	V_GPU_Tensor::~V_GPU_Tensor()
	{

	}

	void V_GPU_Tensor::download(V_Tensor& local)
	{
		for(int i = 0;i<depth();++i)
		{
			data[i].download(local[i]);
		}
	}

	int V_GPU_Tensor::depth() const
	{
		return data.size();
	}

	void V_GPU_Tensor::push_back(GpuMat slice)
	{
		data.push_back(slice);
	}

	GpuMat& V_GPU_Tensor::operator[](int index)
	{
		return data[index];
	}

	const GpuMat& V_GPU_Tensor::operator[](int index) const
	{
		return data[index];
	}

	void V_GPU_Tensor::convolve(const V_GPU_Tensor& kernel, GpuMat& res, GpuMat& buff, V_GPU_Tensor& buff2) const
	{
		v_convolve(data[0], kernel[0], res, true, true);
		for(int i = 1;i<this->depth();++i)
		{
			v_convolve(data[i], kernel[i], buff, true, true);
			add(res,buff,res);
		}
	}

	std::vector<V_GPU_Tensor> copy_template(const std::vector<V_GPU_Tensor>& x)
	{
		std::vector<V_GPU_Tensor> res;
		for(int i = 0;i<x.size();++i) res.push_back(V_GPU_Tensor(x[i].depth()));
		return res;
	}

	std::vector<V_GPU_Tensor> copy_flipped_template(const std::vector<V_GPU_Tensor>& x)
	{
		std::vector<V_GPU_Tensor> res;
		for(int i = 0;i<x[0].depth();++i) res.push_back(V_GPU_Tensor(x.size()));
		return res;
	}

	void add(const V_GPU_Tensor& a,const V_GPU_Tensor& b, V_GPU_Tensor& res)
	{
		Stream stream;
		for(int i = 0;i<a.depth();++i)
		{
			add(a[i],b[i],res[i], GpuMat(), -1, stream);
		}
		stream.waitForCompletion();
	}

	void subtract(const V_GPU_Tensor& a,const V_GPU_Tensor& b, V_GPU_Tensor& res)
	{
		Stream stream;
		for(int i = 0;i<a.depth();++i)
		{
			subtract(a[i],b[i],res[i], GpuMat(), -1, stream);
		}
		stream.waitForCompletion();
	}

	void multiply(const V_GPU_Tensor& a,const V_GPU_Tensor& b, V_GPU_Tensor& res)
	{
		Stream stream;
		for(int i = 0;i<a.depth();++i)
		{
			multiply(a[i],b[i],res[i], 1.0, -1, stream);
		}
		stream.waitForCompletion();
	}

	void multiply(const V_GPU_Tensor& a, const double b, V_GPU_Tensor& res)
	{
		Stream stream;
		for (int i = 0; i<a.depth(); ++i)
		{
			multiply(a[i], b, res[i], 1.0, -1, stream);
		}
		stream.waitForCompletion();
	}

	void sigmoid(const V_GPU_Tensor& x, V_GPU_Tensor& res, GpuMat& buff)
	{
		Stream stream;
		for(int i = 0;i<x.depth();++i)
		{
			sigmoidm(x[i],res[i],buff);
		}		
		stream.waitForCompletion();
	}

	void grad_sigmoid(const V_GPU_Tensor& x, V_GPU_Tensor& res, GpuMat& buff)
	{
		for(int i = 0;i<x.depth();++i)
		{
			sigmoidm_grad(x[i],res[i],buff);
		}		
	}

	void relu(const V_GPU_Tensor& x, V_GPU_Tensor& res, GpuMat& buff)
	{
		Stream stream;
		for (int i = 0; i<x.depth(); ++i)
		{
			relum(x[i], res[i]);
		}
		stream.waitForCompletion();
	}

	void grad_relu(const V_GPU_Tensor& x, V_GPU_Tensor& res, GpuMat& buff)
	{
		for (int i = 0; i<x.depth(); ++i)
		{
			relum_grad(x[i], res[i]);
		}
	}

	void threshold(const V_GPU_Tensor& x, V_GPU_Tensor& res, double t)
	{
		Stream stream;
		for (int i = 0; i<x.depth(); ++i)
		{
			threshold(x[i], res[i], t, 1.0, CV_THRESH_BINARY);
		}
		stream.waitForCompletion();
	}
		
	void flip(const std::vector<V_GPU_Tensor>& W, std::vector<V_GPU_Tensor>& Wt)
	{
		Stream stream;
		for(int i = 0;i<W.size();++i)
		{
			for(int j = 0;j<W[i].depth();++j)
			{	
				flip(W[i][j],Wt[j][i],-1, stream);
			}	
		}
		stream.waitForCompletion();
	}

	void convolve(const std::vector<V_GPU_Tensor>& W, const V_GPU_Tensor& x, V_GPU_Tensor& res, V_GPU_Tensor& buff, std::vector<V_GPU_Tensor>& buff2)
	{
		for(int i = 0; i<W.size(); ++i)
		{
			res[i].setTo(0);
			x.convolve(W[i], res[i], buff[i], buff2[i]);
		}
	}

	void deconvolve(const V_GPU_Tensor& y, const V_GPU_Tensor& x, std::vector<V_GPU_Tensor>& res, std::vector<V_GPU_Tensor>& buff)
	{
		//y[j] convolve with res[i][j] = x[i]
		//y[j] convolve x[i] = res[i][j]
		//pad y with res size
		for(int i = 0;i<x.depth();++i)
		{
			for(int j = 0;j<y.depth();++j)
			{
				pad(y[j], buff[i][j], (res[i][j].cols - 1) / 2, (res[i][j].rows - 1) / 2);
				cv_gpu::divide(buff[i][j], buff[i][j].rows*buff[i][j].cols, buff[i][j]);
				v_convolve(buff[i][j], x[i], res[i][j], true, false);
			}
		}
	}

	void add(const std::vector<V_GPU_Tensor>& a,const std::vector<V_GPU_Tensor>& b, std::vector<V_GPU_Tensor>& res)
	{
		Stream stream;
		for(int i = 0;i<a.size();++i)
		{
			for(int j = 0;j<a[i].depth();++j)
			{
				add(a[i][j],b[i][j],res[i][j], GpuMat(), -1, stream);
			}
		}
		stream.waitForCompletion();
	}

	void weighted_add(std::vector<V_GPU_Tensor>& self, double w_self,const std::vector<V_GPU_Tensor>& x, double w_x)
	{
		Stream stream;
		for(int i = 0;i<self.size();++i)
		{
			for(int j = 0;j<self[i].depth();++j)
			{
				addWeighted(self[i][j],w_self,x[i][j],w_x,0,self[i][j], -1, stream);
			}
		}	
		stream.waitForCompletion();
	}

	double get_avesqr_error(const V_GPU_Tensor& diff, V_GPU_Tensor& buff)
	{
		double sum = 0;
		for(int i = 0;i<diff.depth();++i)
		{
			#ifdef OPENCV_3_0
				sum += sqrSum(diff[i])[0]/(diff[i].rows * diff[i].cols);
			#else
				sum += sqrSum(diff[i], buff[i])[0]/(diff[i].rows * diff[i].cols);
			#endif
		}
		return sum;
	}

	void init_value(std::vector<V_GPU_Tensor>& x, const std::vector<V_GPU_Tensor>& temp, double value)
	{
		for(int i = 0;i<x.size();++i)
		{
			for(int j = 0;j<x[i].depth();++j)
			{
				x[i][j] = GpuMat(temp[i][j].rows, temp[i][j].cols, temp[i][j].type());
			}
		}	
		set_value(x, value);
	}

	void set_value(std::vector<V_GPU_Tensor>& x, double value)
	{
		for(int i = 0;i<x.size();++i)
		{
			for(int j = 0;j<x[i].depth();++j)
			{
				x[i][j].setTo(value);
			}
		}	
	}

	void pack_grid(const V_GPU_Tensor& x, V_GPU_Tensor& y, const int s, const V_GPU_Tensor& map)
	{
		int l = 0;
		Stream stream;
		for(int k = 0; k<x.depth(); ++k)
		{
			GpuMat src = x[k];
			for(int i = 0;i<s;++i)
			{
				for(int j = 0;j<s;++j)
				{
					remap(src, y[l], map[l*2], map[l*2+1], cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0, stream);
					l += 1;
				}
			}
		}
		stream.waitForCompletion();
	}

	void unpack_grid(const V_GPU_Tensor& x, V_GPU_Tensor& y, const int s, const V_GPU_Tensor& map, V_GPU_Tensor& buff)
	{
		int l = 0;
		Stream stream;
		for(int k = 0; k<x.depth()/(s*s); ++k)
		{
			if(buff[k].size() != x[l].size()) buff[k].create(x[l].rows*s*s,x[l].cols,CV_32FC1);
			for(int m = 0;m < s*s;++m) 
			{
				GpuMat temp = buff[k].rowRange(m*x[l].rows, (m+1)*x[l].rows);
				x[l++].copyTo(temp);
			}
			remap(buff[k], y[k], map[k*2], map[k*2+1], cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0, stream);
		}
		stream.waitForCompletion();
	}

	void create_pack_map(const int data_size, const int width, const int height, const int s, V_GPU_Tensor& map_xy)
	{
		map_xy = V_GPU_Tensor(s*s*data_size*2);
		int l = 0;
		for(int k = 0;k<data_size;++k)
		{
			for(int i = 0;i<s;++i)
				for(int j = 0;j<s;++j)
				{
					//index j,i means row-oriented kernels.
					cv_ann::create_pack_map(j,i,width/s,height/s,s,map_xy[l], map_xy[l+1]);
					l += 2;
				}
		}
	}

	void create_unpack_map(const int data_size, const int width, const int height, const int s, V_GPU_Tensor& map_xy)
	{
		map_xy = V_GPU_Tensor(data_size*2);
		for(int k = 0;k<data_size;++k)
		{
			cv_ann::create_unpack_map(width,height,s,map_xy[k*2],map_xy[k*2+1]);
		}
	}

	void pyr_down(const V_GPU_Tensor& x, V_GPU_Tensor& y)
	{
		int l = 0;
		for(int k = 0; k<x.depth(); ++k)
		{
			cv_gpu::pyrDown(x[k], y[l++]);
		}
	}

}
