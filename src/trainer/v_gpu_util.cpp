#include "trainer/v_gpu_util.h"
#include <iostream>
#include <cmath>
#include <ctime>

namespace cv_ann
{

	#ifdef OPENCV_3_0
	auto conv =	cv::cuda::createConvolution();
	#else
	#endif

	void setup_gpu()
	{
		std::cout << "Device count " << getCudaEnabledDeviceCount() << std::endl;	
	}

	void test(cv::Mat img)
	{
		GpuMat gimg;
		gimg.upload(img);

		cv::Mat kernel = cv::getGaussianKernel(21,1.0,CV_32FC1);
		kernel = kernel*kernel.t();
		GpuMat gkernel;
		gkernel.upload(kernel);
		std::cout << "kernel size: " << gkernel.rows << " " << gkernel.cols << std::endl;

		GpuMat padded;
		v_convolve(gimg, gkernel, padded, true, true);
		GpuMat non_padded;
		v_convolve(gimg, gkernel, non_padded, true, false);

		cv::Mat p,np;
		padded.download(p);
		non_padded.download(np);

		cv::imshow("Padded", p);
		cv::imshow("Non-Padded", np);
	}

	void v_convolve(const GpuMat& src, const GpuMat& kernel, GpuMat& res, bool flip, bool do_pad)
	{
		if(do_pad)
		{
			pad(src,res,(kernel.cols-1)/2,(kernel.rows-1)/2);
			#ifdef OPENCV_3_0
				conv->convolve(res, kernel, res, flip);
			#else
				convolve( res, kernel, res, flip);
			#endif
		}
		else{
			#ifdef OPENCV_3_0
				conv->convolve(src, kernel, res, flip);
			#else
				convolve(src, kernel, res, flip);
			#endif
		}
	}

	void pad(const GpuMat& src, GpuMat& res, int w, int h)
	{
		copyMakeBorder(src,res,h,h,w,w,cv::BORDER_CONSTANT,0, Stream::Null());
	}

	void sigmoidm(const GpuMat& m, GpuMat& dest, GpuMat& _t)
	{
		multiply(m,-1,_t);
		exp(_t,_t);
		add(_t,1,_t);
		divide(1,_t,dest);
	}
	void sigmoidm_grad(const GpuMat& m, GpuMat& dest, GpuMat& _t)
	{
		multiply(m,-1,_t);
		add(_t,1,_t);
		multiply(m,_t,dest);
	}

	void relum(const GpuMat& m, GpuMat& dest)
	{
		max(m,0,dest);
	}
	void relum_grad(const GpuMat& m, GpuMat& dest)
	{
		threshold(m, dest, 0.0, 1.0, cv::THRESH_BINARY, Stream::Null());
	}

	void tanhm(const GpuMat& m, GpuMat& dest, GpuMat& _t)
	{
		multiply(m, 2, _t);
		exp(_t,_t);
		add(_t, -1, dest);
		add(_t, 1, _t);
		divide(dest,_t,dest);
	}
	void tanhm_grad(const GpuMat& m, GpuMat& dest, GpuMat& _t)
	{
		multiply(m, m, _t, -1);
		add(_t, 1, dest);
	}

	void create_pack_map(const int offset_x, const int offset_y, const int width, const int height, const int s, GpuMat& mapx, GpuMat& mapy)
	{
		cv::Mat _mapx = cv::Mat(height,width,CV_32FC1);
		cv::Mat _mapy = cv::Mat(height,width,CV_32FC1);
		float* x = _mapx.ptr<float>(0);
		float* y = _mapy.ptr<float>(0);
		for(int a = 0;a<height;++a)
			for(int b = 0;b<width;++b)
			{
				*(x++) = b*s + offset_x;
				*(y++) = a*s + offset_y;
			}
		mapx.upload(_mapx);
		mapy.upload(_mapy);
	}

	void create_unpack_map(const int width, const int height, const int s, GpuMat& mapx, GpuMat& mapy)
	{
		cv::Mat _mapx = cv::Mat(height,width,CV_32FC1);
		cv::Mat _mapy = cv::Mat(height,width,CV_32FC1);
		float* x = _mapx.ptr<float>(0);
		float* y = _mapy.ptr<float>(0);
		int w = width/s;
		int h = height/s;
		for(int a = 0;a<height;++a)
			for(int b = 0;b<width;++b)
			{
				*(x++) = b/s;
				*(y++) = ((a%s)*s + b%s)*h + a/s;
			}
		mapx.upload(_mapx);
		mapy.upload(_mapy);
	}

	GPU_Random::GPU_Random(int seed, int height, int width) : seed_(seed), width_(width), height_(height)
	{
		//upload
		cv::Mat local = cv::Mat(height,width,CV_8UC1);
		seed_.fill(local,cv::RNG::UNIFORM,0,255);
		temp_.upload(local);
	}

	void GPU_Random::randu(GpuMat& dst)
	{
		lshift(temp_,2,temp2_);
		bitwise_xor(temp_,temp2_,temp_);
		rshift(temp_,2,temp2_);
		bitwise_xor(temp_,temp2_,temp_);
		temp_.convertTo(dst,CV_32FC1,1.0/255,1e-6);
	}

	void sample_bernoulli(const GpuMat& m, GpuMat& dest, GPU_Random& seed)
	{
		seed.randu(dest);
		subtract(m,dest,dest);
		threshold(dest,dest,0.0,1.0,cv::THRESH_BINARY, Stream::Null());
	}

	void sample_gaussian(const GpuMat& m, GpuMat& dest, GPU_Random& seed)
	{
		m.copyTo(dest);
	}

	void arrange_data(const GpuMat& data, int recursive_step, const GpuMat& dummy, GpuMat& hist)
	{
		int num_rows = std::max(data.rows*recursive_step, 1);
		if (hist.empty() || hist.rows != num_rows || hist.cols != data.cols) 
		{
			hist.create( num_rows, data.cols, CV_32FC1);
		}

		if (recursive_step == 0) return;
		for (int j = 0; j<data.cols; ++j)
		{
			GpuMat datum = hist.col(j);
			int f = 0, t;
			for (int s = recursive_step; s>0; --s)
			{
				t = f + data.rows;
				if (j - s >= 0) data.col(j - s).copyTo(datum.rowRange(f, t));
				else dummy.copyTo(datum.rowRange(f, t));
				f = t;
			}
		}
	}

	double ave_sqr_sum(const GpuMat& data, GpuMat& buf, double total)
	{
		#ifdef OPENCV_3_0
			return sqrSum(data)[0] / (total);
		#else
			return sqrSum(data, buf)[0] / (total);
		#endif
	}

}
