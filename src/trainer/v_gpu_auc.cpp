#include "trainer/v_gpu_auc.h"
#include "trainer/v_gpu_util.h"
#include <iostream>
#include <cmath>
#include <limits>
#include <ctime>

namespace cv_ann
{

	void auc_relu_train(GpuMat V, GpuMat gW,GpuMat gB,GpuMat gBp, V_Parameter_AUC params)
    {
        std::cout << "Training a rectified linear auto-encoder with GPU ..." << std::endl;
        std::cout << "Please use low learning rate like 0.001!";

        GpuMat dW,dB,H_,V_;
		GpuMat Vw = GpuMat(gW.rows,gW.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vb = GpuMat(gB.rows,1,CV_32FC1); Vb.setTo(0);
		
		GpuMat one_row_data = GpuMat(1,V.cols,CV_32FC1); one_row_data.setTo(1);
		GpuMat one_col_data = GpuMat(V.cols,1,CV_32FC1); one_col_data.setTo(1);

        GpuMat dV_,dV_dW,dH,dHdW,dHdB;
		GpuMat dV_dW_t;
		GpuMat H_grad,H_data;
		GpuMat V_data,V_data_;

		GpuMat emp = GpuMat();

        double last_error = std::numeric_limits<double>::max(),error;
		for(unsigned int i = 1;i<=params.max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);

            //forward pass
			cv_gpu::gemm(gB,one_row_data,1,emp,0,H_data);
			cv_gpu::gemm(gW,V,1,H_data,1,H_data);
			relum(H_data,H_);
			cv_gpu::gemm(gW,H_,1,gBp,1,V_data,cv::GEMM_1_T);
			relum(V_data,V_);

            //propagate
            subtract(V_,V,dV_); // only support sigmoid and Relu
			cv_gpu::gemm(dV_,H_,1.0/H_.cols,emp,0,dV_dW,cv::GEMM_2_T);
			relum_grad(H_,H_grad);
			cv_gpu::gemm(gW,dV_,1,emp,0,H_data);
			multiply(H_grad,H_data,dH);
			cv_gpu::gemm(dH,V,1.0/V.cols,emp,0,dHdW,cv::GEMM_2_T);

            //update
			transpose(dV_dW,dV_dW_t);
			add(dV_dW_t,dHdW,dW);
			//addWeighted(dW,1,gW,params.lambda,0,dW); //L2 regularization
			cv_gpu::gemm(H_grad,V,params.lambda/H_.cols,dW,1,dW,cv::GEMM_2_T); //L1 sparsity (not regularization)
			addWeighted(Vw,momentum,dW,-1,0,Vw);
			addWeighted(gW,1,Vw,params.learning_rate,0,gW);

			//bias
			if(params.with_bias)
            {
				cv_gpu::gemm(dH,one_col_data,1.0/H_.cols,emp,0,dHdB);
				//addWeighted(dHdB,1,gB,params.lambda,0,dB); //L2 regularization
				cv_gpu::gemm(H_grad,one_col_data,params.lambda/H_.cols,dHdB,1,dB); //L1 sparsity (not regularization)
				addWeighted(Vb,momentum,dB,-1,0,Vb);
				addWeighted(gB,1,Vb,params.learning_rate,0,gB);
            }

            //compute error
            error = ave_sqr_sum(dV_,V_data_,V.cols);
            if(!(i%100)) std::cout<< " " << error << std::flush;
			if(abs(error-last_error) < params.error_threshold)
			{
                std::cout << " " << error << std::flush;
				break;
			}
            last_error = error;
        }
        std::cout << std::endl;
		
    }

	void auc_relu_train(GpuMat V, GpuMat C, GpuMat gW, GpuMat gB, GpuMat gU, GpuMat gBp, V_Parameter_AUC params) 
	{
        std::cout << "Training a rectified linear auto-encoder with GPU ..." << std::endl;
        std::cout << "Please use low learning rate like 0.001!";

        GpuMat dW,dB,dU,H_,V_;
		GpuMat Vw = GpuMat(gW.rows,gW.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vu = GpuMat(gU.rows,gU.cols,CV_32FC1); Vu.setTo(0);
        GpuMat Vb = GpuMat(gB.rows,1,CV_32FC1); Vb.setTo(0);
		
		GpuMat one_row_data = GpuMat(1,V.cols,CV_32FC1); one_row_data.setTo(1);
		GpuMat one_col_data = GpuMat(V.cols,1,CV_32FC1); one_col_data.setTo(1);

        GpuMat dV_,dV_dW,dH,dHdW,dHdB,dHdU,UC_;
		GpuMat dV_dW_t;
		GpuMat B_expand,H_grad,H_data,H_data_;
		GpuMat V_data,V_data_;

		GpuMat emp = GpuMat();

        double last_error = std::numeric_limits<double>::max(),error;
		for(unsigned int i = 1;i<=params.max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

            double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);

            //forward pass
			cv_gpu::gemm(gB,one_row_data,1,emp,0,B_expand);
			cv_gpu::gemm(gW,V,1,B_expand,1,H_data_);
            cv_gpu::gemm(gU,C,1,emp,0,UC_);
            add(H_data_,UC_,H_data);
			relum(H_data,H_);
			cv_gpu::gemm(gW,H_,1,gBp,1,V_data,cv::GEMM_1_T);
			relum(V_data,V_);

            //propagate
            subtract(V_,V,dV_); // only support sigmoid and Relu
			cv_gpu::gemm(dV_,H_,1.0/H_.cols,emp,0,dV_dW,cv::GEMM_2_T);
			relum_grad(H_,H_grad);
			cv_gpu::gemm(gW,dV_,1,emp,0,H_data);
			multiply(H_grad,H_data,dH);
			cv_gpu::gemm(dH,V,1.0/V.cols,emp,0,dHdW,cv::GEMM_2_T);
			cv_gpu::gemm(dH,C,1.0/C.cols,emp,0,dHdU,cv::GEMM_2_T);

            //update
			transpose(dV_dW,dV_dW_t);
			add(dV_dW_t,dHdW,dW);
			//addWeighted(dW,1,gW,params.lambda,0,dW); //L2 regularization
			cv_gpu::gemm(H_grad,V,params.lambda/V.cols,dW,1,dW,cv::GEMM_2_T); //L1 sparsity (not regularization)
			addWeighted(Vw,momentum,dW,-1,0,Vw);
            addWeighted(gW,1,Vw,params.learning_rate,0,gW);

			//addWeighted(dHdU,1,gU,params.lambda,0,dU); //L2 regularization
			cv_gpu::gemm(H_grad,C,params.lambda/C.cols,dHdU,1,dU,cv::GEMM_2_T); //L1 sparsity (not regularization)
			addWeighted(Vu,momentum,dU,-1,0,Vu);
            addWeighted(gU,1,Vu,params.learning_rate,0,gU);

			//bias
            if(params.with_bias)
            {
				cv_gpu::gemm(dH,one_col_data,1.0/H_.cols,emp,0,dHdB);
                //addWeighted(dHdB,1,gB,params.lambda,0,dB); //L2 regularization
				cv_gpu::gemm(H_grad,one_col_data,params.lambda/H_.cols,dHdB,1,dB); //L1 sparsity (not regularization)
				addWeighted(Vb,momentum,dB,-1,0,Vb);
                addWeighted(gB,1,Vb,params.learning_rate,0,gB);
            }

            //compute error
            error = ave_sqr_sum(dV_,V_data_,V.cols);
            if(!(i%100)) std::cout<< " " << error << std::flush;
			if(abs(error-last_error) < params.error_threshold)
			{
                std::cout << " " << error << std::flush;
				break;
			}
            last_error = error;
        }
        std::cout << std::endl;		
	
	}

	void supervise_relu_train(GpuMat V,GpuMat H,GpuMat gW,GpuMat gB,V_Parameter_AUC params)
	{
		std::cout << "Training a rectified linear supervise layer with GPU ..." << std::endl;
        std::cout << "Please use low learning rate like 0.001!";

        GpuMat dW,dB,H_;
		GpuMat Vw = GpuMat(gW.rows,gW.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vb = GpuMat(gB.rows,1,CV_32FC1); Vb.setTo(0);
		
		GpuMat one_row_data = GpuMat(1,V.cols,CV_32FC1); one_row_data.setTo(1);
		GpuMat one_col_data = GpuMat(V.cols,1,CV_32FC1); one_col_data.setTo(1);

        GpuMat dH,dHdW,dHdB;
		GpuMat H_grad,H_data;

		GpuMat emp = GpuMat();

        double last_error = std::numeric_limits<double>::max(),error;
		for(unsigned int i = 1;i<=params.max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);

            //forward pass
			cv_gpu::gemm(gB,one_row_data,1,emp,0,H_data);
			cv_gpu::gemm(gW,V,1,H_data,1,H_data);
			relum(H_data,H_);

            //propagate
			relum_grad(H_,H_grad);
            //subtract(H_,H,H_data); // only support sigmoid and Relu
			//multiply(H_grad,H_data,dH);
			subtract(H_,H,dH); //current dH is wrong
			cv_gpu::gemm(dH,V,1.0/V.cols,emp,0,dHdW,cv::GEMM_2_T);

            //update
			//addWeighted(dW,1,gW,params.lambda,0,dW); //L2 regularization
			cv_gpu::gemm(H_grad,V,params.lambda/H_.cols,dHdW,1,dW,cv::GEMM_2_T); //L1 sparsity (not regularization)
			addWeighted(Vw,momentum,dW,-1,0,Vw);
			addWeighted(gW,1,Vw,params.learning_rate,0,gW);

			//bias
			if(params.with_bias)
            {
				cv_gpu::gemm(dH,one_col_data,1.0/H_.cols,emp,0,dHdB);
				//addWeighted(dHdB,1,gB,params.lambda,0,dB); //L2 regularization
				cv_gpu::gemm(H_grad,one_col_data,params.lambda/H_.cols,dHdB,1,dB); //L1 sparsity (not regularization)
				addWeighted(Vb,momentum,dB,-1,0,Vb);
				addWeighted(gB,1,Vb,params.learning_rate,0,gB);
            }

            //compute error
            error = ave_sqr_sum(dH, H_data, H.cols);
            if(!(i%100)) std::cout<< " " << error << std::flush;
			if(abs(error-last_error) < params.error_threshold)
			{
                std::cout << " " << error << std::flush;
				break;
			}
            last_error = error;
        }
        std::cout << std::endl;	
	
	}

	//with minibatch
	void auc_relu_train(std::vector<GpuMat> Vs, GpuMat gW,GpuMat gB, std::vector<GpuMat> gBps, V_Parameter_AUC params)
    {
        std::cout << "Training a rectified linear auto-encoder with GPU ..." << std::endl;
        std::cout << "Please use low learning rate like 0.001!";

        GpuMat dW,dB;
        GpuMat dV_dW,dHdW,dHdB;
		GpuMat dV_dW_t;
		GpuMat Vw = GpuMat(gW.rows,gW.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vb = GpuMat(gB.rows,1,CV_32FC1); Vb.setTo(0);
		
		std::vector<GpuMat> one_row_data; one_row_data.resize(Vs.size()); 
		std::vector<GpuMat> one_col_data; one_col_data.resize(Vs.size());
		for(unsigned int j = 0;j<Vs.size();++j) 
		{ 
			one_row_data[j] = GpuMat(1,Vs[j].cols,CV_32FC1); one_row_data[j].setTo(1); 
			one_col_data[j] = GpuMat(Vs[j].cols,1,CV_32FC1); one_col_data[j].setTo(1);
		}
		
		std::vector<GpuMat> V_; V_.resize(Vs.size());
		std::vector<GpuMat> H_; H_.resize(Vs.size());
		std::vector<GpuMat> dV; dV.resize(Vs.size());
		std::vector<GpuMat> dH; dH.resize(Vs.size());
		std::vector<GpuMat> H_grad; H_grad.resize(Vs.size());
		std::vector<GpuMat> H_data; H_data.resize(Vs.size());
		std::vector<GpuMat> V_data; V_data.resize(Vs.size());

		GpuMat emp = GpuMat();

        double last_error = std::numeric_limits<double>::max(),error;
		for(unsigned int i = 1;i<=params.max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);

			error = 0;
			for(unsigned int j = 0;j<Vs.size();++j)
			{

				//forward pass
				cv_gpu::gemm(gB,one_row_data[j],1,emp,0,H_data[j]);
				cv_gpu::gemm(gW,Vs[j],1,H_data[j],1,H_data[j]);
				relum(H_data[j],H_[j]);
				cv_gpu::gemm(gW,H_[j],1,gBps[j],1,V_data[j],cv::GEMM_1_T);
				relum(V_data[j],V_[j]);

				//propagate
				subtract(V_[j],Vs[j],dV[j]); // only support sigmoid and Relu
				cv_gpu::gemm(dV[j],H_[j],1.0/H_[j].cols,emp,0,dV_dW,cv::GEMM_2_T);
				relum_grad(H_[j],H_grad[j]);
				cv_gpu::gemm(gW,dV[j],1,emp,0,H_data[j]);
				multiply(H_grad[j],H_data[j],dH[j]);
				cv_gpu::gemm(dH[j],Vs[j],1.0/Vs[j].cols,emp,0,dHdW,cv::GEMM_2_T);

				//update
				transpose(dV_dW,dV_dW_t);
				add(dV_dW_t,dHdW,dW);
				//addWeighted(dW,1,gW,params.lambda,0,dW); //L2 regularization
				cv_gpu::gemm(H_grad[j],Vs[j],params.lambda/H_[j].cols,dW,1,dW,cv::GEMM_2_T); //L1 sparsity (not regularization)
				addWeighted(Vw,momentum,dW,-1,0,Vw);
				addWeighted(gW,1,Vw,params.learning_rate,0,gW);

				//bias
				if(params.with_bias)
				{
					cv_gpu::gemm(dH[j],one_col_data[j],1.0/H_[j].cols,emp,0,dHdB);
					//addWeighted(dHdB,1,gB,params.lambda,0,dB); //L2 regularization
					cv_gpu::gemm(H_grad[j],one_col_data[j],params.lambda/H_[j].cols,dHdB,1,dB); //L1 sparsity (not regularization)
					addWeighted(Vb,momentum,dB,-1,0,Vb);
					addWeighted(gB,1,Vb,params.learning_rate,0,gB);
				}

				//compute error
				error = error + ave_sqr_sum(dV[j],V_data[j],Vs[j].cols);
			}

            if(!(i%100)) std::cout<< " " << error << std::flush;
			if(abs(error-last_error) < params.error_threshold)
			{
                std::cout << " " << error << std::flush;
				break;
			}
            last_error = error;
        }
        std::cout << std::endl;
		
    }

	
	//with minibatch
	void auc_relu_train(std::vector<GpuMat> Vs, std::vector<GpuMat> Cs, GpuMat gW, GpuMat gB, GpuMat gU, std::vector<GpuMat> gBps, V_Parameter_AUC params) 
	{
        std::cout << "Training a rectified linear auto-encoder with GPU ..." << std::endl;
        std::cout << "Please use low learning rate like 0.001!";

        GpuMat dW,dB,dU,dV_dW,dV_dW_t,dHdW,dHdB,dHdU;
		GpuMat Vw = GpuMat(gW.rows,gW.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vu = GpuMat(gU.rows,gU.cols,CV_32FC1); Vu.setTo(0);
        GpuMat Vb = GpuMat(gB.rows,1,CV_32FC1); Vb.setTo(0);
		
		std::vector<GpuMat> one_row_data; one_row_data.resize(Vs.size()); 
		std::vector<GpuMat> one_col_data; one_col_data.resize(Vs.size());
		for(unsigned int j = 0;j<Vs.size();++j) 
		{ 
			one_row_data[j] = GpuMat(1,Vs[j].cols,CV_32FC1); one_row_data[j].setTo(1); 
			one_col_data[j] = GpuMat(Vs[j].cols,1,CV_32FC1); one_col_data[j].setTo(1);
		}

		std::vector<GpuMat> V_; V_.resize(Vs.size());
		std::vector<GpuMat> H_; H_.resize(Vs.size());
		std::vector<GpuMat> UC_; UC_.resize(Vs.size());
		std::vector<GpuMat> dV; dV.resize(Vs.size());
		std::vector<GpuMat> dH; dH.resize(Vs.size());
		std::vector<GpuMat> H_grad; H_grad.resize(Vs.size());
		std::vector<GpuMat> H_data; H_data.resize(Vs.size());
		std::vector<GpuMat> H_data_; H_data_.resize(Vs.size());
		std::vector<GpuMat> V_data; V_data.resize(Vs.size());
		std::vector<GpuMat> B_expand; B_expand.resize(Vs.size());

		GpuMat emp = GpuMat();

        double last_error = std::numeric_limits<double>::max(),error;
		for(unsigned int i = 1;i<=params.max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

            double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);

			error = 0;
			for(unsigned int j = 0;j<Vs.size();++j)
			{
				//forward pass
				cv_gpu::gemm(gB,one_row_data[j],1,emp,0,B_expand[j]);
				cv_gpu::gemm(gW,Vs[j],1,B_expand[j],1,H_data_[j]);
				cv_gpu::gemm(gU,Cs[j],1,emp,0,UC_[j]);
				add(H_data_[j],UC_[j],H_data[j]);
				relum(H_data[j],H_[j]);
				cv_gpu::gemm(gW,H_[j],1,gBps[j],1,V_data[j],cv::GEMM_1_T);
				relum(V_data[j],V_[j]);

				//propagate
				subtract(V_[j],Vs[j],dV[j]); // only support sigmoid and Relu
				cv_gpu::gemm(dV[j],H_[j],1.0/H_[j].cols,emp,0,dV_dW,cv::GEMM_2_T);
				relum_grad(H_[j],H_grad[j]);
				cv_gpu::gemm(gW,dV[j],1,emp,0,H_data[j]);
				multiply(H_grad[j],H_data[j],dH[j]);
				cv_gpu::gemm(dH[j],Vs[j],1.0/Vs[j].cols,emp,0,dHdW,cv::GEMM_2_T);
				cv_gpu::gemm(dH[j],Cs[j],1.0/Cs[j].cols,emp,0,dHdU,cv::GEMM_2_T);

				//update
				transpose(dV_dW,dV_dW_t);
				add(dV_dW_t,dHdW,dW);
				//addWeighted(dW,1,gW,params.lambda,0,dW); //L2 regularization
				cv_gpu::gemm(H_grad[j],Vs[j],params.lambda/Vs[j].cols,dW,1,dW,cv::GEMM_2_T); //L1 sparsity (not regularization)
				addWeighted(Vw,momentum,dW,-1,0,Vw);
				addWeighted(gW,1,Vw,params.learning_rate,0,gW);

				//addWeighted(dHdU,1,gU,params.lambda,0,dU); //L2 regularization
				cv_gpu::gemm(H_grad[j],Cs[j],params.lambda/Cs[j].cols,dHdU,1,dU,cv::GEMM_2_T); //L1 sparsity (not regularization)
				addWeighted(Vu,momentum,dU,-1,0,Vu);
				addWeighted(gU,1,Vu,params.learning_rate,0,gU);

				//bias
				if(params.with_bias)
				{
					cv_gpu::gemm(dH[j],one_col_data[j],1.0/H_[j].cols,emp,0,dHdB);
					//addWeighted(dHdB,1,gB,params.lambda,0,dB); //L2 regularization
					cv_gpu::gemm(H_grad[j],one_col_data[j],params.lambda/H_[j].cols,dHdB,1,dB); //L1 sparsity (not regularization)
					addWeighted(Vb,momentum,dB,-1,0,Vb);
					addWeighted(gB,1,Vb,params.learning_rate,0,gB);
				}

				//compute error
				error = error + ave_sqr_sum(dV[j],V_data[j],Vs[j].cols);
			}

            if(!(i%100)) std::cout<< " " << error << std::flush;
			if(abs(error-last_error) < params.error_threshold)
			{
                std::cout << " " << error << std::flush;
				break;
			}
            last_error = error;
        }
        std::cout << std::endl;		
	
	}
	
	//with minibatch
	void supervise_relu_train(std::vector<GpuMat> Vs, std::vector<GpuMat> Hs, GpuMat gW, GpuMat gB, V_Parameter_AUC params)
	{
		std::cout << "Training a rectified linear supervise layer with GPU ..." << std::endl;
        std::cout << "Please use low learning rate like 0.001!";

        GpuMat dW,dB;
        GpuMat dHdW,dHdB;
		GpuMat Vw = GpuMat(gW.rows,gW.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vb = GpuMat(gB.rows,1,CV_32FC1); Vb.setTo(0);
		
		std::vector<GpuMat> one_row_data; one_row_data.resize(Vs.size()); 
		std::vector<GpuMat> one_col_data; one_col_data.resize(Vs.size());
		for(unsigned int j = 0;j<Vs.size();++j) 
		{ 
			one_row_data[j] = GpuMat(1,Vs[j].cols,CV_32FC1); one_row_data[j].setTo(1); 
			one_col_data[j] = GpuMat(Vs[j].cols,1,CV_32FC1); one_col_data[j].setTo(1);
		}
		
		std::vector<GpuMat> H_; H_.resize(Vs.size());
		std::vector<GpuMat> dH; dH.resize(Vs.size());
		std::vector<GpuMat> H_grad; H_grad.resize(Vs.size());
		std::vector<GpuMat> H_data; H_data.resize(Vs.size());

		GpuMat emp = GpuMat();
		
        double last_error = std::numeric_limits<double>::max(),error;
		for(unsigned int i = 1;i<=params.max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);

			error = 0;
			for(unsigned int j = 0;j<Vs.size();++j)
			{

				//forward pass
				cv_gpu::gemm(gB,one_row_data[j],1,emp,0,H_data[j]);
				cv_gpu::gemm(gW,Vs[j],1,H_data[j],1,H_data[j]);
				relum(H_data[j],H_[j]);

				//propagate
				relum_grad(H_[j],H_grad[j]);
				//subtract(H_,H,H_data); // only support sigmoid and Relu
				//multiply(H_grad,H_data,dH);
				subtract(H_[j],Hs[j],dH[j]); //current dH is wrong
				cv_gpu::gemm(dH[j],Vs[j],1.0/Vs[j].cols,emp,0,dHdW,cv::GEMM_2_T);

				//update
				//addWeighted(dW,1,gW,params.lambda,0,dW); //L2 regularization
				cv_gpu::gemm(H_grad[j],Vs[j],params.lambda/H_[j].cols,dHdW,1,dW,cv::GEMM_2_T); //L1 sparsity (not regularization)
				addWeighted(Vw,momentum,dW,-1,0,Vw);
				addWeighted(gW,1,Vw,params.learning_rate,0,gW);

				//bias
				if(params.with_bias)
				{
					cv_gpu::gemm(dH[j],one_col_data[j],1.0/H_[j].cols,emp,0,dHdB);
					//addWeighted(dHdB,1,gB,params.lambda,0,dB); //L2 regularization
					cv_gpu::gemm(H_grad[j],one_col_data[j],params.lambda/H_[j].cols,dHdB,1,dB); //L1 sparsity (not regularization)
					addWeighted(Vb,momentum,dB,-1,0,Vb);
					addWeighted(gB,1,Vb,params.learning_rate,0,gB);
				}

				//compute error
				error = error + ave_sqr_sum(dH[j], H_data[j],Hs[j].cols);
			}

            if(!(i%100)) std::cout<< " " << error << std::flush;
			if(abs(error-last_error) < params.error_threshold)
			{
                std::cout << " " << error << std::flush;
				break;
			}
            last_error = error;
        }
        std::cout << std::endl;
	}

	//with minibatch	
	void auc_relu_train(
		std::vector<GpuMat> Vs, 
		std::vector<GpuMat> Xs, 
		std::vector<GpuMat> C1,
		std::vector<GpuMat> C2,
		GpuMat gW, 
		GpuMat gR,
		GpuMat gU1,
		GpuMat gU2,
		std::vector<GpuMat> gBp_data,
		std::vector<GpuMat> gBp_resd,
		V_Parameter_AUC params
		)
	{
		std::cout << "Training a rectified linear auto-encoder with GPU ..." << std::endl;
		std::cout << "Please use low learning rate like 0.001!";

		GpuMat dW, dR, dU1, dU2, dV_dW, dV_dW_t, dX_dR, dX_dR_t, dHdW, dHdR, dHdU1, dHdU2;
		GpuMat Vw = GpuMat(gW.rows, gW.cols, CV_32FC1); Vw.setTo(0);
		GpuMat Vr = GpuMat(gR.rows, gR.cols, CV_32FC1); Vr.setTo(0);
		GpuMat Vu1 = GpuMat(gU1.rows, gU1.cols, CV_32FC1); Vu1.setTo(0);
		GpuMat Vu2 = GpuMat(gU2.rows, gU2.cols, CV_32FC1); Vu2.setTo(0);
		
		std::vector<GpuMat> V_; V_.resize(Vs.size());
		std::vector<GpuMat> X_; X_.resize(Xs.size());
		std::vector<GpuMat> H_; H_.resize(Vs.size());
		std::vector<GpuMat> dV; dV.resize(Vs.size());
		std::vector<GpuMat> dX; dX.resize(Xs.size());
		std::vector<GpuMat> dH; dH.resize(Vs.size());
		std::vector<GpuMat> H_grad; H_grad.resize(Vs.size());
		std::vector<GpuMat> H_data; H_data.resize(Vs.size());
		std::vector<GpuMat> V_data; V_data.resize(Vs.size());
		std::vector<GpuMat> X_data; X_data.resize(Xs.size());
		std::vector<GpuMat> B_expand; B_expand.resize(Vs.size());

		GpuMat emp = GpuMat();

		double last_error = std::numeric_limits<double>::max(), error;
		for (unsigned int i = 1; i <= params.max_iteration; ++i)
		{
			if (!((i - 1) % 500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0 / params.max_iteration;
			double momentum = (time_factor*params.momentum + (1 - time_factor)*0.5);

			error = 0;
			for (unsigned int j = 0; j<Vs.size(); ++j)
			{
				//forward pass
				cv_gpu::gemm(gW, Vs[j], 1, emp, 0, H_data[j]);
				cv_gpu::gemm(gR, Xs[j], 1, H_data[j], 1, H_data[j]);
				cv_gpu::gemm(gU1, C1[j], 1, H_data[j], 1, H_data[j]);
				cv_gpu::gemm(gU2, C2[j], 1, H_data[j], 1, H_data[j]);
				relum(H_data[j], H_[j]);
				cv_gpu::gemm(gW, H_[j], 1, gBp_data[j], 1, V_data[j], cv::GEMM_1_T);
				relum(V_data[j], V_[j]);
				cv_gpu::gemm(gR, H_[j], 1, gBp_resd[j], 1, X_data[j], cv::GEMM_1_T);
				relum(X_data[j], X_[j]);

				//propagate
				subtract(V_[j], Vs[j], dV[j]); // only support sigmoid and Relu
				subtract(X_[j], Xs[j], dX[j]); // only support sigmoid and Relu
				cv_gpu::gemm(dV[j], H_[j], 1.0 / H_[j].cols, emp, 0, dV_dW, cv::GEMM_2_T);
				cv_gpu::gemm(dX[j], H_[j], 1.0 / H_[j].cols, emp, 0, dX_dR, cv::GEMM_2_T);
				cv_gpu::gemm(gW, dV[j], 1, emp, 0, H_data[j]);
				cv_gpu::gemm(gR, dX[j], 1, H_data[j], 1, H_data[j]);
				
				relum_grad(H_[j], H_grad[j]);
				multiply(H_grad[j], H_data[j], dH[j]);

				cv_gpu::gemm(dH[j], Vs[j], 1.0 / Vs[j].cols, emp, 0, dHdW, cv::GEMM_2_T);
				cv_gpu::gemm(dH[j], Xs[j], 1.0 / Xs[j].cols, emp, 0, dHdR, cv::GEMM_2_T);
				cv_gpu::gemm(dH[j], C1[j], 1.0 / C1[j].cols, emp, 0, dHdU1, cv::GEMM_2_T);
				cv_gpu::gemm(dH[j], C2[j], 1.0 / C2[j].cols, emp, 0, dHdU2, cv::GEMM_2_T);

				//update
				transpose(dV_dW, dV_dW_t);
				add(dV_dW_t, dHdW, dW);
				cv_gpu::gemm(H_grad[j], Vs[j], params.lambda / Vs[j].cols, dW, 1, dW, cv::GEMM_2_T); //L1 sparsity (not regularization)
				addWeighted(Vw, momentum, dW, -1, 0, Vw);
				addWeighted(gW, 1, Vw, params.learning_rate, 0, gW);

				transpose(dX_dR, dX_dR_t);
				add(dX_dR_t, dHdR, dR);
				cv_gpu::gemm(H_grad[j], Xs[j], params.lambda / Xs[j].cols, dR, 1, dR, cv::GEMM_2_T); //L1 sparsity (not regularization)
				addWeighted(Vr, momentum, dR, -1, 0, Vr);
				addWeighted(gR, 1, Vr, params.learning_rate, 0, gR);

				cv_gpu::gemm(H_grad[j], C1[j], params.lambda / C1[j].cols, dHdU1, 1, dU1, cv::GEMM_2_T); //L1 sparsity (not regularization)
				addWeighted(Vu1, momentum, dU1, -1, 0, Vu1);
				addWeighted(gU1, 1, Vu1, params.learning_rate, 0, gU1);

				cv_gpu::gemm(H_grad[j], C2[j], params.lambda / C2[j].cols, dHdU2, 1, dU2, cv::GEMM_2_T); //L1 sparsity (not regularization)
				addWeighted(Vu2, momentum, dU2, -1, 0, Vu2);
				addWeighted(gU2, 1, Vu2, params.learning_rate, 0, gU2);

				//compute error
				error = error + ave_sqr_sum(dV[j], V_data[j],Vs[j].cols);
			}

			if (!(i % 100)) std::cout << " " << error << std::flush;
			if (abs(error - last_error) < params.error_threshold)
			{
				std::cout << " " << error << std::flush;
				break;
			}
			last_error = error;
		}
		std::cout << std::endl;
	}


}
