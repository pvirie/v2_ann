#include "trainer/v_gpu_auc.h"
#include "trainer/v_gpu_util.h"
#include <iostream>
#include <cmath>
#include <limits>
#include <ctime>

namespace cv_ann
{

	V_GPU_Tensor auc_train(std::vector<V_GPU_Tensor>& data, std::vector<V_GPU_Tensor>& Ws, V_Parameter_AUC params)
	{
		int batch_size = int(params.batchPortion*data.size());
		int current = 0;
		
		V_GPU_Tensor V(Ws[0].depth()), H(Ws.size()), H_(Ws.size()), V_(Ws[0].depth()), dV(Ws[0].depth()), gH(Ws.size()), dH(Ws.size());
		std::vector<V_GPU_Tensor> dVdW = copy_template(Ws);
		std::vector<V_GPU_Tensor> dHdW = copy_template(Ws);
		std::vector<V_GPU_Tensor> dW = copy_template(Ws);
		std::vector<V_GPU_Tensor> dWs = copy_template(Ws);
		std::vector<V_GPU_Tensor> Vw = copy_template(Ws);
		std::vector<V_GPU_Tensor> Wst = copy_flipped_template(Ws);
		std::vector<V_GPU_Tensor> dVdWt = copy_flipped_template(Ws);
		
		flip(Ws, Wst);
		init_value(Vw, Ws, 0);
		init_value(dVdWt, Wst, 0);
		init_value(dHdW, Ws, 0);
		init_value(dWs, Ws, 0);

		GpuMat buff_H;
		GpuMat buff_V;
		V_GPU_Tensor buff_Hs(Ws.size());
		V_GPU_Tensor buff_Vs(Wst.size());
		std::vector<V_GPU_Tensor> buff_Ws = copy_template(Ws);
		std::vector<V_GPU_Tensor> buff_Wst = copy_template(Wst);
		std::vector<V_GPU_Tensor> buff_padded_Vs = copy_template(Ws);
		std::vector<V_GPU_Tensor> buff_padded_Hs = copy_template(Wst);

		double last_error = std::numeric_limits<double>::max(),error;
		for(int i = 1;i<=params.max_iteration;++i)
		{
			if(!((i-1)%100)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);
			
			error = 0;
			set_value(dWs, 0);
			for(int j = 0;j < batch_size; ++j)
			{
				V = data[current++];
				if(current >= data.size()) current = 0;

				//forward pass
				//std::cout << "forward pass" << std::endl;
				convolve(Ws, V, H, buff_Hs, buff_padded_Vs); //std::cout << "forward pass 1" << std::endl;
				relu(H, H, buff_H); //std::cout << "forward pass 2 " << std::endl;
				convolve(Wst, H, V_, buff_Vs, buff_padded_Hs); //std::cout << "forward pass 3" << std::endl;
				relu(V_, V_, buff_V);

				//propagate
				//std::cout << "propagate" << std::endl;
				subtract(V_, V, dV); //std::cout << "propagate 1" << std::endl;
				deconvolve(H, dV, dVdWt, buff_Wst); //std::cout << "propagate 2" << std::endl;
				convolve(Ws, dV, H_, buff_Hs, buff_padded_Vs); //std::cout << "propagate 3" << std::endl;
				grad_relu(H, gH, buff_H); //std::cout << "propagate 4" << std::endl;
				multiply(H_, gH, dH); //std::cout << "propagate 5" << std::endl;
				deconvolve(V, dH, dHdW, buff_Ws);

				//update
				//std::cout << "update" << std::endl;
				flip(dVdWt, dVdW); //std::cout << "update 1" << std::endl;
				add(dVdW, dHdW, dW); //std::cout << "update 2" << std::endl;
				if(params.lambda > 0) weighted_add(dW,1.0,Ws,params.lambda); //std::cout << "update 3" << std::endl;
				weighted_add(dWs, 1.0, dW, 1.0); 
	
				error += get_avesqr_error(dV, buff_Vs)/batch_size;
			}
			
			//std::cout << "update batch" << std::endl;
			weighted_add(Vw, params.momentum, dWs, -1.0/batch_size); //std::cout << "update batch 1" << std::endl;
			weighted_add(Ws, 1.0, Vw, params.learning_rate); //std::cout << "update batch 2" << std::endl;
			flip(Ws, Wst);

			//compute error
			if(!(i%10)) std::cout<< " " << error << std::flush;
			if(abs(error-last_error) < params.error_threshold)
			{
				std::cout << " " << error << std::flush;
				break;
			}
			last_error = error;
		}

		return V_GPU_Tensor();
	}


	V_GPU_Tensor auc_train(
		std::vector<V_GPU_Tensor>& data,
		std::vector<V_GPU_Tensor>& Ws,   //residue
		std::vector<V_GPU_Tensor>& condition,
		std::vector<V_GPU_Tensor>& Us,
		V_Parameter_AUC params
		)
	{
		int batch_size = int(params.batchPortion*data.size());
		int current = 0;
		
		V_GPU_Tensor V, H(Ws.size()), H_(Ws.size()), V_(Ws[0].depth()), dV(Ws[0].depth()), gH(Ws.size()), dH(Ws.size());
		V_GPU_Tensor C, C_(Us[0].depth()), dC(Us[0].depth()), VC_(Ws[0].depth());
		std::vector<V_GPU_Tensor> dVdW = copy_template(Ws);
		std::vector<V_GPU_Tensor> dHdW = copy_template(Ws);
		std::vector<V_GPU_Tensor> dW = copy_template(Ws);
		std::vector<V_GPU_Tensor> dWs = copy_template(Ws);
		std::vector<V_GPU_Tensor> Vw = copy_template(Ws);
		std::vector<V_GPU_Tensor> Wst = copy_flipped_template(Ws);
		std::vector<V_GPU_Tensor> dVdWt = copy_flipped_template(Ws);

		flip(Ws, Wst);
		init_value(Vw, Ws, 0);
		init_value(dVdWt, Wst, 0);
		init_value(dHdW, Ws, 0);
		init_value(dWs, Ws, 0);

		std::vector<V_GPU_Tensor> dU = copy_template(Us);
		std::vector<V_GPU_Tensor> dUs = copy_template(Us);
		std::vector<V_GPU_Tensor> Vu = copy_template(Us);
		std::vector<V_GPU_Tensor> Ust = copy_flipped_template(Us);
		std::vector<V_GPU_Tensor> dCdU = copy_template(Us);
		std::vector<V_GPU_Tensor> dCdUt = copy_flipped_template(Us);
		
		flip(Us, Ust);
		init_value(Vu, Us, 0);
		init_value(dCdU, Ust, 0);
		init_value(dCdUt, Us, 0);
		init_value(dUs, Us, 0);

		GpuMat buff_H;
		GpuMat buff_V;
		V_GPU_Tensor buff_Hs(Ws.size());
		V_GPU_Tensor buff_Vs(Wst.size());
		std::vector<V_GPU_Tensor> buff_Ws = copy_template(Ws);
		std::vector<V_GPU_Tensor> buff_Wst = copy_template(Wst);
		std::vector<V_GPU_Tensor> buff_padded_VWs = copy_template(Ws);
		std::vector<V_GPU_Tensor> buff_padded_Hs = copy_template(Wst);

		GpuMat buff_C;
		V_GPU_Tensor buff_Cs(Us.size());
		std::vector<V_GPU_Tensor> buff_Us = copy_template(Us);
		std::vector<V_GPU_Tensor> buff_Ust = copy_template(Ust);
		std::vector<V_GPU_Tensor> buff_padded_VUs = copy_template(Us);
		std::vector<V_GPU_Tensor> buff_padded_Cs = copy_template(Ust);

		double last_error = std::numeric_limits<double>::max(),error;
		for(int i = 1;i<=params.max_iteration;++i)
		{
			if(!((i-1)%50)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);
			
			error = 0;
			set_value(dWs, 0);
			for(int j = 0;j < batch_size; ++j)
			{
				C = condition[current];
				V = data[current++];
				if(current >= data.size()) current = 0;

				//forward pass
				//int debug = 0;
				//std::cout << "forward pass" << std::endl;
				convolve(Ws, V, H, buff_Hs, buff_padded_VWs);// std::cout << "pass " << debug++ << std::endl;
				relu(H, H, buff_H);

				convolve(Us, V, C_, buff_Cs, buff_padded_VUs);
				relu(C_, C_, buff_C);
				subtract(C_, C, dC);
				deconvolve(V, dC, dCdU, buff_Us);

				convolve(Wst, H, V_, buff_Vs, buff_padded_Hs);
				convolve(Ust, C_, VC_, buff_Vs, buff_padded_Cs); // If we use C instead of C_, something weird happens. Need to prove.
				add(V_, VC_, V_);
				relu(V_, V_, buff_V);

				//propagate
				//std::cout << "propagate" << std::endl;
				subtract(V_, V, dV);
				deconvolve(H, dV, dVdWt, buff_Wst);

				//deconvolve(C, dV, dCdUt, buff_Ust);

				convolve(Ws, dV, H_, buff_Hs, buff_padded_VWs);
				grad_relu(H, gH, buff_H);
				multiply(H_, gH, dH);
				deconvolve(V, dH, dHdW, buff_Ws);

				//update
				//std::cout << "update" << std::endl;
				flip(dVdWt, dVdW);
				add(dVdW, dHdW, dW);
				if(params.lambda > 0) weighted_add(dW,1.0,Ws,params.lambda);
				add(dWs, dW, dWs); 
	
				//flip(dCdUt, dU);
				//add(dCdU, dU, dU);
				//if(params.lambda > 0) weighted_add(dU,1.0,Us,params.lambda);
				//add(dUs, dU, dUs); 
				if(params.lambda > 0) weighted_add(dCdU,1.0,Us,params.lambda);
				add(dUs, dCdU, dUs); 

				double individual_error = get_avesqr_error(dV, buff_Vs);
				if(error < individual_error) error = individual_error;
			}
			
			//std::cout << "update batch" << std::endl;
			weighted_add(Vw, params.momentum, dWs, -1.0/batch_size);
			weighted_add(Ws, 1.0, Vw, params.learning_rate);
			flip(Ws, Wst);

			weighted_add(Vu, params.momentum, dUs, -1.0/batch_size);
			weighted_add(Us, 1.0, Vu, params.learning_rate);
			flip(Us, Ust);

			//compute error
			if(!(i%10)) std::cout<< " " << error << std::flush;
			if(abs(error-last_error) < params.error_threshold)
			{
				std::cout << " " << error << std::flush;
				break;
			}
			last_error = error;
		}

		return V_GPU_Tensor();
	}

}
