#include "network/v_tensor.h"
#include <iostream>
#include <sstream>
#include <limits>

using namespace cv;

namespace cv_ann
{
	V_Tensor::V_Tensor()
	{

	}

	V_Tensor::V_Tensor(cv::Mat img)
	{
		push_back(img);
	}

	V_Tensor::V_Tensor(int depth)
	{
		data.resize(depth);
	}

	V_Tensor::~V_Tensor()
	{

	}

	int V_Tensor::depth() const
	{
		return data.size();
	}

	void V_Tensor::push_back(cv::Mat slice)
	{
		data.push_back(slice);
	}

	cv::Mat& V_Tensor::operator[](int index)
	{
		return data[index];
	}

	const cv::Mat& V_Tensor::operator[](int index) const
	{
		return data[index];
	}

	cv::Mat V_Tensor::operator*(const V_Tensor& x) const
	{
		if(this->depth() != x.depth())
		{
			std::cout << "Error: V_Tensor: operator*: unequal depth." << std::endl;
			return cv::Mat();
		}

		cv::Mat sum;
		if(this->depth() > 0)
		{
			cv::filter2D(x[0],sum,-1,data[0],cv::Point(-1,-1),0,BORDER_DEFAULT);	
			for(int i = 1;i<this->depth();++i)
			{
				cv::Mat dst;
				cv::filter2D(x[i],dst,-1,data[i],cv::Point(-1,-1),0,BORDER_DEFAULT);
				sum = sum + dst;
			}
		}else{
			std::cout << "Error: V_Tensor: operator*: tensor is empty." << std::endl;
			return cv::Mat();
		}

		return sum;
	}

	V_Tensor V_Tensor::operator* (const double x) const
	{
		V_Tensor y;
		for (int i = 0; i<this->depth(); ++i)
		{
			y.push_back(data[i]*x);
		}
		return y;
	}

	V_Tensor V_Tensor::operator+ (const V_Tensor& x) const
	{
		if(this->depth() != x.depth())
		{
			std::cout << "Error: V_Tensor: operator*: unequal depth." << std::endl;
			return cv::Mat();
		}

		V_Tensor y;
		for(int i = 0; i<this->depth(); ++i)
		{
			y.push_back(data[i] + x[i]);
		}

		return y;
	}

	V_Tensor operator* (const std::vector<V_Tensor>& W, const V_Tensor& x)
	{
		V_Tensor y;
		for(int i = 0; i<W.size(); ++i)
		{
			y.push_back(W[i]*x);
		}

		return y;
	}

	void sigmoidm(const cv::Mat& m, cv::Mat& r)
	{
		cv::exp(-m,r);
		cv::divide(1,r+1,r);
	}

	V_Tensor sigmoidm(const V_Tensor& x)
	{
		V_Tensor res(x.depth());
		for(int i = 0;i<x.depth();++i)
		{
			sigmoidm(x[i],res[i]);
		}
		return res;
	}	

	void relum(const cv::Mat& m, cv::Mat& r)
	{
		r = cv::max(m, 0);
	}

	V_Tensor relum(const V_Tensor& x)
	{
		V_Tensor res(x.depth());
		for (int i = 0; i<x.depth(); ++i)
		{
			relum(x[i], res[i]);
		}
		return res;
	}

	V_Tensor threshold(const V_Tensor& x, double t)
	{
		V_Tensor res(x.depth());
		for (int i = 0; i<x.depth(); ++i)
		{
			cv::threshold(x[i], res[i], t, 1.0, CV_THRESH_BINARY);
		}
		return res;
	}

	V_Tensor pack_grid(const V_Tensor& x, const int s, V_Tensor& map)
	{
		V_Tensor y;
		int l = 0;
		for(int k = 0; k<x.depth(); ++k)
		{
			cv::Mat src = x[k];
			for(int i = 0;i<s;++i)
			{
				for(int j = 0;j<s;++j)
				{
					cv::Mat dst = cv::Mat(src.rows/s, src.cols/s, src.type());
					cv::Mat _map;

					if(map.depth() <= l)
					{
						//create map here		
						_map = cv::Mat(dst.rows,dst.cols,CV_16SC2);
						short* d = _map.ptr<short>(0);
						for(int a = 0;a<dst.rows;++a)
							for(int b = 0;b<dst.cols;++b)
							{
								d[0] = b*s + j;
								d[1] = a*s + i;
								d += 2;
							}

						map.push_back(_map);
					}else{
						_map = map[l];
					}

					cv::remap(src, dst, _map, cv::Mat(), cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
					y.push_back(dst);
					l += 1;
				}
			}
		}
		return y;
	}

	
	V_Tensor unpack_grid(const V_Tensor& x, const int s, V_Tensor& map)
	{
		V_Tensor y;
		int l = 0;
		for(int k = 0; k<x.depth()/(s*s); ++k)
		{
			std::vector<cv::Mat> tmp;
			for(int m = 0;m < s*s;++m) tmp.push_back(x[l++]);
			
			cv::Mat _map;
			cv::Mat src;
			cv::merge(tmp, src);
			cv::Mat dst = cv::Mat(src.rows*s, src.cols*s, src.type());
			src = src.reshape(1, src.rows*src.cols*s);
			if(map.depth() <= k)
			{
				//create map here		
				_map = cv::Mat(dst.rows,dst.cols,CV_16SC2);
				short* d = _map.ptr<short>(0);
				for(int a = 0;a<dst.rows;++a)
					for(int b = 0;b<dst.cols;++b)
					{
						d[0] = b%s;
						d[1] = ((a/s)*(dst.cols/s) + b/s)*s + a%s;
						d += 2;
					}

				map.push_back(_map);
			}else{
				_map = map[l];
			}

			cv::remap(src, dst, _map, cv::Mat(), cv::INTER_NEAREST, cv::BORDER_CONSTANT, 0);
			y.push_back(dst);
		}

		return y;
	}

	void pyr_down(const V_Tensor& x, V_Tensor& y)
	{
		int l = 0;
		for (int k = 0; k<x.depth(); ++k)
		{
			pyrDown(x[k], y[l++]);
		}
	}

	bool save(std::string file_name, std::vector<V_Tensor>& W)
	{
        if(W.size() == 0) return false;
        cv::FileStorage fs(file_name, cv::FileStorage::WRITE);
        if(!fs.isOpened()) return false;

        fs << "D4" << ((int) W.size());
        fs << "W" << "[";
        for(unsigned int i = 0;i<W.size();++i)
        {
            fs << "{";
				fs << "D3" << ((int) W[i].depth());
				fs << "data" << "[";
				for(unsigned int j = 0;j<W[i].depth();++j)
				{
					fs << "{";
					fs << "M" << W[i][j];
					fs << "}";
				}
				fs << "]";
            fs << "}";
        }
        fs << "]";
        fs.release();
        return true;		
	}

	bool load(std::string file_name, std::vector<V_Tensor>& W)
	{
        cv::FileStorage fs(file_name, cv::FileStorage::READ);
        if(!fs.isOpened()) return false;
        int D4 = int(fs["D4"]);
        W.clear();
        FileNode tl = fs["W"];
        for(unsigned int i = 0;i<D4;++i)
        {
            FileNode fn = tl[i];
			int depth;
			fn["D3"] >> depth;
			W.push_back(V_Tensor(depth));
			FileNode tll = fn["data"];
			for(unsigned int j = 0;j<depth;++j)
			{
				FileNode fnn = tll[j];
				fnn["M"] >> W[i][j];
			}
        }
        fs.release();
        return true;		
	}

}
