#include "network/v_network.h"
#include <iostream>
#include <sstream>
#include <limits>
#include <ctime>

using namespace cv;

namespace cv_ann
{
	V_Network::V_Network() 
	{

	}

	V_Network::~V_Network() 
	{
	}

	void V_Network::add_layer(std::shared_ptr<V_Layer> layer)
	{
		layers_.push_back(layer);
	}

	std::shared_ptr<V_Layer> V_Network::operator[](int index)
	{
		return layers_[index];
	}

	int V_Network::size()
	{
		return layers_.size();
	}
}
