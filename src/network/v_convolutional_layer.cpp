#include "network/v_convolutional_layer.h"
#include <iostream>
#include <sstream>
#include <limits>

using namespace cv;

namespace cv_ann
{

	V_Convolutional_Layer::V_Convolutional_Layer()
	{
	}

	V_Convolutional_Layer::~V_Convolutional_Layer()
	{
	}

	V_Tensor V_Convolutional_Layer::activate(V_Tensor v)
	{
		return relum(W*v);
	}

	V_Tensor V_Convolutional_Layer::project(V_Tensor h)
	{
		return relum(Wt*h);
	}

	void V_Convolutional_Layer::set_weight(std::vector<V_Tensor> U)
	{
		W = U;
		Wt.resize(W[0].depth());
		for(int i = 0;i<W.size();++i)
		{
			for(int j = 0;j<W[i].depth();++j)
			{	
				cv::Mat dst;
				cv::flip(W[i][j],dst,-1);
				Wt[j].push_back(dst);
			}	
		}
	}


	V_Conditional_Convolutional_Layer::V_Conditional_Convolutional_Layer()
	{
	}

	V_Conditional_Convolutional_Layer::~V_Conditional_Convolutional_Layer()
	{
	}

	V_Tensor V_Conditional_Convolutional_Layer::activate_condition(V_Tensor v)
	{
		return relum(U*v);
	}

	V_Tensor V_Conditional_Convolutional_Layer::project(V_Tensor h, V_Tensor c)
	{
		return relum(Wt*h + Ut*c);
	}

	void V_Conditional_Convolutional_Layer::set_weight(std::vector<V_Tensor> _W, std::vector<V_Tensor> _U)
	{
		W = _W;
		Wt.resize(W[0].depth());
		for(int i = 0;i<W.size();++i)
		{
			for(int j = 0;j<W[i].depth();++j)
			{	
				cv::Mat dst;
				cv::flip(W[i][j],dst,-1);
				Wt[j].push_back(dst);
			}	
		}

		U = _U;
		Ut.resize(U[0].depth());
		for(int i = 0;i<U.size();++i)
		{
			for(int j = 0;j<U[i].depth();++j)
			{	
				cv::Mat dst;
				cv::flip(U[i][j],dst,-1);
				Ut[j].push_back(dst);
			}	
		}
	}

}
