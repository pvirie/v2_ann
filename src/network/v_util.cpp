#include "network/v_util.h"
#include <iostream>
#include <cmath>
#include <ctime>
#include <fstream>

namespace cv_ann
{
    cv::RNG rand_seed = cv::RNG(std::time(0));

	double sigmoid(double x)
	{
		return 1/(1+exp(-x));
	}

	cv::Mat sigmoid_activate(cv::Mat m)
	{
		cv::Mat r;
		cv::exp(-m,r);
		return 1/(1+r);
	}
	cv::Mat sigmoid_grad(cv::Mat m)
	{
        return m.mul(1-m);
	}
	cv::Mat sigmoid_sample(cv::Mat m)
	{
		return sample_bernoulli(sigmoid_activate(m));
	}

	cv::Mat logm(cv::Mat x)
	{
		cv::log(x,x);
		return x;
	}

	cv::Mat expm(cv::Mat x)
	{
		cv::exp(x,x);
		return x;
	}

	cv::Mat relu_activate(cv::Mat m)
	{
		return cv::max(m,0);
	}
	cv::Mat relu_grad(cv::Mat m)
	{
        cv::Mat r = cv::Mat(m.rows,m.cols,m.type());
        cv::MatConstIterator_<double> it = m.begin<double>(), it_end = m.end<double>();
		cv::MatIterator_<double> rit = r.begin<double>(), rit_end = r.end<double>();
        for(; it != it_end; ++it,++rit)
            *rit = ( *it > 0 ? 1.0:0.0);
		return r;
	}
	cv::Mat relu_sample(cv::Mat m)
	{
		return cv::max(sample_gaussian(m,sigmoid_activate(m)),0);
	}

	cv::Mat identity(cv::Mat m)
	{
		return m;
	}

	cv::Mat to_column_vector(cv::Mat m)
	{
		return m.reshape(0,m.total());
	}

	cv::Mat to_column_matrix(cv::Mat m,int columns)
	{
		return m.reshape(0,m.total()/columns);
	}

	cv::Mat sample_bernoulli(cv::Mat m)
	{
        cv::Mat r = cv::Mat(m.rows,m.cols,m.type());
        //rand_seed.fill(r,cv::RNG::UNIFORM,0,1);

        cv::MatConstIterator_<double> it = m.begin<double>(), it_end = m.end<double>();
		cv::MatIterator_<double> rit = r.begin<double>(), rit_end = r.end<double>();
        for(; it != it_end; ++it,++rit)
            *rit = ( *it >= rand_seed.uniform(0.0,1.0) ? 1.0:0.0);

		return r;
	}

    cv::Mat sample_gaussian(cv::Mat m, cv::Mat sd)
    {
        cv::Mat r = cv::Mat(m.rows,m.cols,m.type());
        if(sd.empty()) sd = cv::Mat::ones(m.rows,m.cols,m.type());
        //rand_seed.fill(r,cv::RNG::NORMAL,m,sd);

        cv::MatConstIterator_<double> it = m.begin<double>(), it_end = m.end<double>();
        cv::MatConstIterator_<double> st = sd.begin<double>(), st_end = sd.end<double>();
        cv::MatIterator_<double> rit = r.begin<double>(), rit_end = r.end<double>();
        for(; it != it_end; ++it,++rit,++st)
            *rit = rand_seed.gaussian(*st) + *it;

        return r;
    }


	cv::Mat log_sum_exp_over_cols(cv::Mat m)
	{
		cv::Mat mx,sum;
		cv::reduce(m,mx,0,CV_REDUCE_MAX);
		cv::reduce(expm(m-cv::repeat(mx,m.rows,1)),sum,0,CV_REDUCE_SUM);
		return logm(sum) + mx;
	}

	cv::Mat threshold(cv::Mat m, double value)
	{
        cv::Mat r = cv::Mat(m.rows,m.cols,m.type());

        cv::MatConstIterator_<double> it = m.begin<double>(), it_end = m.end<double>();
		cv::MatIterator_<double> rit = r.begin<double>(), rit_end = r.end<double>();
        for(; it != it_end; ++it,++rit)
            *rit = ( *it >= value ? 1.0:0.0);

		return r;
	}

    void print_mat(cv::Mat m, bool with_data)
	{
		std::cout << m.rows << " by " << m.cols << std::endl;
        if(!with_data) return;
		for(int i = 0;i<m.rows;++i)
		{
			for(int j = 0;j<m.cols;++j)
			{
				std::cout << m.at<double>(i,j) << " ";
			}
			std::cout <<std::endl;
		}
	}

	void save_mat(std::string filename, cv::Mat m)
	{
		std::ofstream file;
		file.open(filename, std::ios::out);
		file << m;
		file.close();
	}

	cv::Mat tie(
		cv::Mat data,cv::Mat labels,
		cv::Mat W,cv::Mat B,
		std::function<cv::Mat(cv::Mat)> A,
		int _max_iteration, double _error_steady, double _learning_rate, double _momentum_weight, double _lambda, bool _with_bias)
    {
        std::cout << "Tying data ...";
		
        cv::Mat dW,dB,H_;
		cv::Mat H = labels;
        cv::Mat V = data;
        cv::Mat Vw = cv::Mat::zeros(W.rows,W.cols,CV_64FC1);
        cv::Mat Vb = cv::Mat::zeros(B.rows,1,CV_64FC1);
		
        cv::Mat one_row_data = cv::Mat::ones(1,V.cols,CV_64FC1);
        cv::Mat one_row_output = cv::Mat::ones(1,H.rows,CV_64FC1);
		cv::Mat one_col_data = cv::Mat::ones(V.cols,1,CV_64FC1);

        double last_error = std::numeric_limits<double>::max(),error;
		for(unsigned int i = 1;i<=_max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

            double time_factor = i*1.0/_max_iteration;
            double momentum = (time_factor*_momentum_weight + (1-time_factor)*0.5);

            //forward pass
            H_ = A(W*V + B*one_row_data);

            //propagate
            cv::Mat dH_ = (H_ - H); // only support sigmoid and Relu
            cv::Mat dH_dW = dH_*V.t()/V.cols;

            //update
			dW = dH_dW + _lambda*W;
            Vw = Vw*momentum - dW;
			W = W + Vw*_learning_rate;

			//bias
            if(_with_bias)
            {
                cv::Mat dHdB = dH_*one_col_data/H_.cols;
                dB = dHdB + _lambda*B;
                Vb = Vb*momentum - dB;
                B = B + Vb*_learning_rate;
            }

            //compute error
            cv::Mat error_m = one_row_output*(dH_).mul(dH_)*one_row_data.t();
            error = error_m.at<double>(0,0)/V.cols;
            if(!(i%100)) std::cout<< " " << error << std::flush;
			if(abs(error-last_error) < _error_steady) 
			{
				std::cout << " " << error << std::flush;
				break;
			}
            last_error = error;
        }
        std::cout << std::endl;

        return A(W*V + B*one_row_data);
    }

}
