#ifndef CFUGUE_ADAPTOR_H
#define CFUGUE_ADAPTOR_H

#include <iostream>
#include <cmath>
#include <ctime>
#include <opencv2/opencv.hpp>

namespace v_cfugue
{
	cv::Mat musicstring_to_mat(std::string musicstring, int squeeze = 5);
	std::string mat_to_musicstring(cv::Mat mat, int squeeze = 5);

	std::string file_to_musicstring(std::string filename);
	void musicstring_to_file(std::string filename, std::string musicstring);
	void read_list_file(std::string filename, std::vector<cv::Mat> &data, std::vector<int> &conditions);

	void init_player();
	void play_musicstring(std::string musicstring);
	void dispose_player();

	int note_to_index(std::string note);
	std::string extract_note(std::string in);

};

#endif