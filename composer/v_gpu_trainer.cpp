#include "v_gpu_trainer.h"
#include <iostream>
#include <cmath>
#include <limits>
#include <ctime>

namespace cv_ann
{
	void modify(const GpuMat& m, GpuMat& dest, int num_factors, int activate)
	{
		m.copyTo(dest);
		dest.rowRange(cv::Range(dest.rows - num_factors, dest.rows)).setTo(0);
		if (activate >= 0) dest.row(dest.rows - activate - 1).setTo(1);
	}
	void prelum(const GpuMat& m, GpuMat& dest, int num_factors, int activate)
	{
		max(m, 0, dest);
		dest.rowRange(cv::Range(dest.rows - num_factors, dest.rows)).setTo(0);
		if (activate >= 0) dest.row(dest.rows - activate - 1).setTo(1);
	}
	void replace_dH(const GpuMat& H, const GpuMat& pH, const GpuMat& m, GpuMat& dest, int num_factors, int activate, GpuMat& buff)
	{
		subtract(H, pH, dest);
		dest.rowRange(cv::Range(0, dest.rows - num_factors)).setTo(0);
		m.copyTo(buff);
		buff.rowRange(cv::Range(buff.rows - num_factors, buff.rows)).setTo(0);
		add(dest, buff, dest);
	}
	void sample_gaussian(const GpuMat& m, GpuMat& dest, GPU_Random& seed, int num_factors, int activate)
	{
		m.copyTo(dest);
	}

	//with minibatch
	void supervise_relu_train(std::vector<GpuMat> Vs, std::vector<GpuMat> Hs, GpuMat gW, GpuMat gB, V_Parameter_AUC params, int num_factors, int activate)
	{
		std::cout << "Training a rectified linear supervise layer with GPU ..." << std::endl;
		std::cout << "Please use low learning rate like 0.001!";

		GpuMat dW, dB;
		GpuMat dHdW, dHdB;
		GpuMat Vw = GpuMat(gW.rows, gW.cols, CV_32FC1); Vw.setTo(0);
		GpuMat Vb = GpuMat(gB.rows, 1, CV_32FC1); Vb.setTo(0);

		std::vector<GpuMat> one_row_data; one_row_data.resize(Vs.size());
		std::vector<GpuMat> one_col_data; one_col_data.resize(Vs.size());
		for (unsigned int j = 0; j<Vs.size(); ++j)
		{
			one_row_data[j] = GpuMat(1, Vs[j].cols, CV_32FC1); one_row_data[j].setTo(1);
			one_col_data[j] = GpuMat(Vs[j].cols, 1, CV_32FC1); one_col_data[j].setTo(1);
		}

		std::vector<GpuMat> H_; H_.resize(Vs.size());
		std::vector<GpuMat> dH; dH.resize(Vs.size());
		std::vector<GpuMat> H_grad; H_grad.resize(Vs.size());
		std::vector<GpuMat> H_data; H_data.resize(Vs.size());

		GpuMat emp = GpuMat();

		double last_error = std::numeric_limits<double>::max(), error;
		for (unsigned int i = 1; i <= params.max_iteration; ++i)
		{
			if (!((i - 1) % 500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0 / params.max_iteration;
			double momentum = (time_factor*params.momentum + (1 - time_factor)*0.5);

			error = 0;
			for (unsigned int j = 0; j<Vs.size(); ++j)
			{

				//forward pass
				cv_gpu::gemm(gB, one_row_data[j], 1, emp, 0, H_data[j]);
				cv_gpu::gemm(gW, Vs[j], 1, H_data[j], 1, H_data[j]);
				relum(H_data[j], H_[j]);

				//propagate
				relum_grad(H_[j], H_grad[j]);
				//subtract(H_,H,H_data); // only support sigmoid and Relu
				//multiply(H_grad,H_data,dH);
				subtract(H_[j], Hs[j], dH[j]); //current dH is wrong
				cv_gpu::gemm(dH[j], Vs[j], 1.0 / Vs[j].cols, emp, 0, dHdW, cv::GEMM_2_T);

				//update
				//addWeighted(dW,1,gW,params.lambda,0,dW); //L2 regularization
				cv_gpu::gemm(H_grad[j], Vs[j], params.lambda / H_[j].cols, dHdW, 1, dW, cv::GEMM_2_T); //L1 sparsity (not regularization)
				addWeighted(Vw, momentum, dW, -1, 0, Vw);
				addWeighted(gW, 1, Vw, params.learning_rate, 0, gW);

				//bias
				if (params.with_bias)
				{
					cv_gpu::gemm(dH[j], one_col_data[j], 1.0 / H_[j].cols, emp, 0, dHdB);
					//addWeighted(dHdB,1,gB,params.lambda,0,dB); //L2 regularization
					cv_gpu::gemm(H_grad[j], one_col_data[j], params.lambda / H_[j].cols, dHdB, 1, dB, cv::GEMM_2_T); //L1 sparsity (not regularization)
					addWeighted(Vb, momentum, dB, -1, 0, Vb);
					addWeighted(gB, 1, Vb, params.learning_rate, 0, gB);
				}

				//compute error
				error = error + ave_sqr_sum(dH[j], H_data[j], Hs[j].cols);
			}

			if (!(i % 100)) std::cout << " " << error << std::flush;
			if (abs(error - last_error) < params.error_threshold)
			{
				std::cout << " " << error << std::flush;
				break;
			}
			last_error = error;
		}
		std::cout << std::endl;


	}

	//with minibatch
	void auc_relu_train(std::vector<GpuMat> Vs, std::vector<GpuMat> Cs, GpuMat gW, GpuMat gB, GpuMat gU, std::vector<GpuMat> gBps, V_Parameter_AUC params, int num_factors, int activate)
	{
        std::cout << "Training a rectified linear auto-encoder with GPU ..." << std::endl;
        std::cout << "Please use low learning rate like 0.001!";

        GpuMat dW,dB,dU,dV_dW,dV_dW_t,dHdW,dHdB,dHdU;
		GpuMat Vw = GpuMat(gW.rows,gW.cols,CV_32FC1); Vw.setTo(0);
        GpuMat Vu = GpuMat(gU.rows,gU.cols,CV_32FC1); Vu.setTo(0);
        GpuMat Vb = GpuMat(gB.rows,1,CV_32FC1); Vb.setTo(0);
		
		std::vector<GpuMat> one_row_data; one_row_data.resize(Vs.size()); 
		std::vector<GpuMat> one_col_data; one_col_data.resize(Vs.size());
		for(unsigned int j = 0;j<Vs.size();++j) 
		{ 
			one_row_data[j] = GpuMat(1,Vs[j].cols,CV_32FC1); one_row_data[j].setTo(1); 
			one_col_data[j] = GpuMat(Vs[j].cols,1,CV_32FC1); one_col_data[j].setTo(1);
		}

		std::vector<GpuMat> V_; V_.resize(Vs.size());
		std::vector<GpuMat> H_; H_.resize(Vs.size());
		std::vector<GpuMat> pH_; pH_.resize(Vs.size());
		std::vector<GpuMat> UC_; UC_.resize(Vs.size());
		std::vector<GpuMat> dV; dV.resize(Vs.size());
		std::vector<GpuMat> dH; dH.resize(Vs.size());
		std::vector<GpuMat> H_grad; H_grad.resize(Vs.size());
		std::vector<GpuMat> H_data; H_data.resize(Vs.size());
		std::vector<GpuMat> H_data_; H_data_.resize(Vs.size());
		std::vector<GpuMat> V_data; V_data.resize(Vs.size());
		std::vector<GpuMat> B_expand; B_expand.resize(Vs.size());

		GpuMat emp = GpuMat();

        double last_error = std::numeric_limits<double>::max(),error;
		for(unsigned int i = 1;i<=params.max_iteration;++i)
        {
            if(!((i-1)%500)) std::cout << std::endl << i << " >";

            double time_factor = i*1.0/params.max_iteration;
			double momentum = (time_factor*params.momentum + (1-time_factor)*0.5);

			error = 0;
			for(unsigned int j = 0;j<Vs.size();++j)
			{
				//forward pass
				cv_gpu::gemm(gB,one_row_data[j],1,emp,0,B_expand[j]);
				cv_gpu::gemm(gW,Vs[j],1,B_expand[j],1,H_data_[j]);
				cv_gpu::gemm(gU,Cs[j],1,emp,0,UC_[j]);
				add(H_data_[j],UC_[j],H_data[j]);
				relum(H_data[j], H_[j]); // <
				modify(H_[j], pH_[j], num_factors, activate); // <
				cv_gpu::gemm(gW,pH_[j],1,gBps[j],1,V_data[j],cv::GEMM_1_T);
				relum(V_data[j], V_[j]);

				//propagate
				subtract(V_[j],Vs[j],dV[j]); // only support sigmoid and Relu
				cv_gpu::gemm(dV[j],pH_[j],1.0/pH_[j].cols,emp,0,dV_dW,cv::GEMM_2_T);
				cv_gpu::gemm(gW, dV[j], 1, emp, 0, H_grad[j]);
				replace_dH(H_[j], pH_[j], H_grad[j], H_data[j], num_factors, activate, H_data_[j]);   // <
				relum_grad(H_[j], H_grad[j]);
				multiply(H_grad[j],H_data[j],dH[j]);
				cv_gpu::gemm(dH[j],Vs[j],1.0/Vs[j].cols,emp,0,dHdW,cv::GEMM_2_T);
				cv_gpu::gemm(dH[j],Cs[j],1.0/Cs[j].cols,emp,0,dHdU,cv::GEMM_2_T);

				//update
				transpose(dV_dW, dV_dW_t);
				dV_dW_t.rowRange(cv::Range(dV_dW_t.rows - num_factors, dV_dW_t.rows)).setTo(0); // <
				add(dV_dW_t,dHdW,dW);
				//addWeighted(dW,1,gW,params.lambda,0,dW); //L2 regularization
				cv_gpu::gemm(H_grad[j],Vs[j],params.lambda/Vs[j].cols,dW,1,dW,cv::GEMM_2_T); //L1 sparsity (not regularization)
				addWeighted(Vw,momentum,dW,-1,0,Vw);
				addWeighted(gW,1,Vw,params.learning_rate,0,gW);

				//addWeighted(dHdU,1,gU,params.lambda,0,dU); //L2 regularization
				cv_gpu::gemm(H_grad[j],Cs[j],params.lambda/Cs[j].cols,dHdU,1,dU,cv::GEMM_2_T); //L1 sparsity (not regularization)
				addWeighted(Vu,momentum,dU,-1,0,Vu);
				addWeighted(gU,1,Vu,params.learning_rate,0,gU);

				//bias
				if(params.with_bias)
				{
					cv_gpu::gemm(dH[j],one_col_data[j],1.0/H_[j].cols,emp,0,dHdB);
					//addWeighted(dHdB,1,gB,params.lambda,0,dB); //L2 regularization
					cv_gpu::gemm(H_grad[j],one_col_data[j],params.lambda/H_[j].cols,dHdB,1,dB,cv::GEMM_2_T); //L1 sparsity (not regularization)
					addWeighted(Vb,momentum,dB,-1,0,Vb);
					addWeighted(gB,1,Vb,params.learning_rate,0,gB);
				}

				//compute error
				error = error + ave_sqr_sum(dV[j], V_data[j], Vs[j].cols);
			}

            if(!(i%100)) std::cout<< " " << error << std::flush;
			if(abs(error-last_error) < params.error_threshold)
			{
                std::cout << " " << error << std::flush;
				break;
			}
            last_error = error;
        }
        std::cout << std::endl;		
	
	}

	//minibatch
	void rbm_relu_train(std::vector<GpuMat> Vs, std::vector<GpuMat> Cs, GpuMat gW, GpuMat gB, GpuMat gU, std::vector<GpuMat> gBps, V_Parameter_RBM params, int num_factors, int activate)
	{
		std::cout << "Training a rectified linear RBM with GPU ...";

		GpuMat dW, dB, dU;
		GpuMat Vw = GpuMat(gW.rows, gW.cols, CV_32FC1); Vw.setTo(0);
		GpuMat Vu = GpuMat(gU.rows, gU.cols, CV_32FC1); Vu.setTo(0);
		GpuMat Vb = GpuMat(gB.rows, 1, CV_32FC1); Vb.setTo(0);

		std::vector<GpuMat> one_row_data; one_row_data.resize(Vs.size());
		std::vector<GpuMat> one_col_data; one_col_data.resize(Vs.size());
		std::vector<GPU_Random> rand_seed;
		for (unsigned int j = 0; j<Vs.size(); ++j)
		{
			one_row_data[j] = GpuMat(1, Vs[j].cols, CV_32FC1); one_row_data[j].setTo(1);
			one_col_data[j] = GpuMat(Vs[j].cols, 1, CV_32FC1); one_col_data[j].setTo(1);
			rand_seed.push_back(GPU_Random(time(0), gW.rows, Vs[j].cols));
		}

		std::vector<GpuMat> V_; V_.resize(Vs.size());
		std::vector<GpuMat> H_; H_.resize(Vs.size());
		std::vector<GpuMat> UC_; UC_.resize(Vs.size());
		std::vector<GpuMat> X; X.resize(Vs.size());
		std::vector<GpuMat> G_; G_.resize(Vs.size());
		std::vector<GpuMat> B_expand; B_expand.resize(Vs.size());
		std::vector<GpuMat> H_grad; H_grad.resize(Vs.size());
		std::vector<GpuMat> H_data; H_data.resize(Vs.size());
		std::vector<GpuMat> H_data_; H_data_.resize(Vs.size());
		std::vector<GpuMat> V_data; V_data.resize(Vs.size());
		std::vector<GpuMat> V_data_; V_data_.resize(Vs.size());
		std::vector<GpuMat> positive; positive.resize(Vs.size());

		GpuMat emp = GpuMat();

		for (int i = 1; i <= params.max_iteration; ++i)
		{
			if (!((i - 1) % 500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0 / params.max_iteration;
			double momentum = (time_factor*params.momentum + (1 - time_factor)*0.5);
			unsigned int cd_count = static_cast<unsigned int>(params.cd_round*time_factor + 1);

			for (int j = 0; j<Vs.size(); ++j)
			{
				cv_gpu::gemm(gB, one_row_data[j], 1, emp, 0, B_expand[j]);
				cv_gpu::gemm(gW, Vs[j], 1, B_expand[j], 1, H_[j]);
				cv_gpu::gemm(gU, Cs[j], 1, emp, 0, UC_[j]);
				add(H_[j], UC_[j], H_data[j]);
				prelum(H_data[j], H_[j], num_factors, activate);

				cv_gpu::gemm(H_[j], Vs[j], 1.0 / Vs[j].cols, emp, 0, positive[j], cv::GEMM_2_T);
				H_[j].copyTo(G_[j]);

				for (unsigned int k = 0; k<cd_count; ++k)
				{
					sample_gaussian(H_[j], H_data[j], rand_seed[j], num_factors, activate);
					cv_gpu::gemm(gW, H_data[j], 1, gBps[j], 1, V_data[j], cv::GEMM_1_T);
					prelum(V_data[j], V_[j], num_factors, activate);
					cv_gpu::gemm(gW, V_[j], 1, B_expand[j], 1, H_[j]);
					add(H_[j], UC_[j], H_data[j]);
					prelum(H_data[j], H_[j], num_factors, activate);
				}

				//compute the gradients (positive - negative) - penalization

				cv_gpu::gemm(H_[j], Vs[j], 1.0 / Vs[j].cols, emp, 0, H_grad[j], cv::GEMM_2_T);
				subtract(positive[j], H_grad[j], dW);
				addWeighted(dW, 1, gW, -1.0*params.lambda, 0, dW);
				addWeighted(Vw, momentum, dW, 1, 0, Vw);
				addWeighted(gW, 1, Vw, params.learning_rate, 0, gW);

				subtract(G_[j], H_[j], H_data[j]);
				cv_gpu::gemm(H_data[j], Cs[j], 1.0 / H_[j].cols, emp, 0, dU, cv::GEMM_2_T);
				addWeighted(dU, 1, gU, -1.0*params.lambda, 0, dU);
				addWeighted(Vu, momentum, dU, 1, 0, Vu);
				addWeighted(gU, 1, Vu, params.learning_rate, 0, gU);

				if (params.with_bias)
				{
					cv_gpu::gemm(H_data[j], one_col_data[j], 1.0 / H_[j].cols, emp, 0, dB);
					addWeighted(dB, 1, gB, -1.0*params.lambda, 0, dB);
					addWeighted(Vb, momentum, dB, 1, 0, Vb);
					addWeighted(gB, 1, Vb, params.learning_rate, 0, gB);
				}
			}

			if (!(i % 100)) std::cout << " " << i << std::flush;
		}
		std::cout << std::endl;

	}
}
