#include <iostream>
#include <opencv2\opencv.hpp>
#include "cfugue_adaptor.h"
#include "string_util.h"
#include "v_temporal_network.h"
#include "v_composer_network.h"

using namespace cv_ann;

void experiment(double lambda)
{
	std::stringstream ss;
	ss << "./composer_" << std::to_string(lambda).substr(2) << ".ann"; 

	int num_initial = 50;
	cv_ann::V_Composer_Network ann;
	if (!ann.load(ss.str()))
	{
		std::cout << "Cannot loaded the trained parameter file(s). Initiating traning sequence..." << std::endl;
		return;
	}
	
	int length = 20;
	cv::Mat sum_errors = cv::Mat::zeros(length, 1, CV_64FC1);
	std::function<double(cv::Mat, int)> run_sequence = [&](cv::Mat musicmat, int label) -> double
	{
		cv::Mat condition = cv::Mat::zeros(3, 1, CV_64FC1); 
		condition.row(label).setTo(1);
		std::vector<cv::Mat> out_music_vector;
		int i = 0;
		for (; i<num_initial; ++i)
		{
			out_music_vector.push_back(musicmat.col(i).t());
		}

		cv::Mat initial_frames;
		for (int k = num_initial; k>0; --k)
		{
			cv::Mat tmp = out_music_vector[out_music_vector.size() - k];
			initial_frames.push_back(tmp);
		}	
		cv::Mat errors;
		double ave_error = ann.sample_sequence_and_compare(initial_frames.t(), condition, length, errors);
		sum_errors = sum_errors + errors;
		return ave_error;
	};

	std::vector<std::string> test_list;
	std::vector<int> labels;
	test_list.push_back("../data/musicstring/beeth/mond_2.mis"); labels.push_back(0);
	test_list.push_back("../data/musicstring/beeth/appass_1.mis"); labels.push_back(0);
	test_list.push_back("../data/musicstring/beeth/beethoven_hammerklavier_1.mis"); labels.push_back(0);
	test_list.push_back("../data/musicstring/chopin/chpn-p2.mis"); labels.push_back(1);
	test_list.push_back("../data/musicstring/chopin/chpn_op25_e4.mis"); labels.push_back(1);
	test_list.push_back("../data/musicstring/chopin/chpn-p19.mis"); labels.push_back(1);
	test_list.push_back("../data/musicstring/mozart/mz_333_3.mis"); labels.push_back(2);
	test_list.push_back("../data/musicstring/mozart/mz_331_2.mis"); labels.push_back(2);
	test_list.push_back("../data/musicstring/mozart/mz_332_1.mis"); labels.push_back(2);
	
	double sum_error = 0;
	for(int i = 0;i < test_list.size(); ++i)
	{
		cv::Mat musicmat = v_cfugue::musicstring_to_mat(v_cfugue::file_to_musicstring(test_list[i]));
		sum_error += run_sequence(musicmat, labels[i]);
	}
	sum_errors = sum_errors/test_list.size();
	std::cout << sum_errors << std::endl;
	std::cout << ss.str() << ": Average factor error: " << sum_error/test_list.size() << std::endl;

	ss.str("");
	ss << "./report_" << std::to_string(lambda).substr(2) << ".txt"; 

	std::ofstream fs(ss.str(), std::ofstream::out);
	if(fs.is_open())
	{
		std::cout << "Saving experiment results!" << std::endl;
		fs << lambda << std::endl;
		fs << sum_error/test_list.size() << std::endl;
		for(int i = 0;i<length;++i)
		{
			fs << sum_errors.at<double>(i) << std::endl;
		}
	}
}

void train(double lambda)
{
	std::stringstream ss, ss2;
	ss << "./composer_" << std::to_string(lambda).substr(2) << ".ann"; 
	//ss2 << "./composer_" << std::to_string(lambda).substr(2) << ".abp"; 
	ss2 << "./composer_shared.abp"; 

	cv_ann::setup_gpu();

	std::string content = v_cfugue::file_to_musicstring("../data/musicstring/beeth/mond_2.mis");
	cv::Mat musicmat = v_cfugue::musicstring_to_mat(content);

	cv_ann::save_mat("musicmat_input.txt", musicmat.t());

	int num_recursion = 20;
	cv_ann::V_Composer_Network ann;
	if(!ann.load(ss.str()))
	{
		std::cout << "Cannot loaded the trained parameter file(s): " << ss.str() << " Initiating traning sequence..." << std::endl;

		std::vector<cv::Mat> raws;
		std::vector<int> labels;

		v_cfugue::read_list_file("../composer/train_data.list", raws, labels);

		
		std::vector<cv::Mat> data; data.resize(raws.size());
		std::vector<cv::Mat> conditions; conditions.resize(labels.size());
		int min_length = raws[0].cols;
		for (int i = 0; i < labels.size(); ++i)
		{
			if(min_length > raws[i].cols) min_length = raws[i].cols;
			conditions[i] = cv::Mat::zeros(3, 1, CV_64FC1); 
			conditions[i].row(labels[i]).setTo(1);
		}
		for(int i = 0; i < raws.size(); ++i)
		{
			data[i] = raws[i].colRange(0, min_length);		
		}

		ann.add_layer(musicmat.rows, 1, num_recursion);
		ann.add_layer(musicmat.rows / 4, musicmat.rows / 2, num_recursion);
		ann.add_layer(musicmat.rows / 8, musicmat.rows / 2, num_recursion);
		ann.add_layer(3, musicmat.rows / 2, num_recursion);
		ann.add_layer(1, musicmat.rows / 2, num_recursion);
		if(!ann.load(ss2.str()))
		{
			std::cout << "Cannot loaded the backprop parameter file(s): " << ss2.str() << " Initiating traning sequence..." << std::endl;
			ann.train_decomposition(data, conditions, lambda);
			ann.save(ss2.str());
		}else{
			ann.set_residue_layer(1, 100);
			ann.set_residue_layer(2, 100);
			ann.set_residue_layer(3, 100);
			ann.set_residue_layer(4, 100);
		}
		ann.train_reconstruction(data, conditions, lambda);
		ann.save(ss.str());
	}

}

void play() {
	
	std::string content = v_cfugue::file_to_musicstring("../data/musicstring/beeth/elise.mis");

	cv::Mat musicmat = v_cfugue::musicstring_to_mat(content, 5);
	content = v_cfugue::mat_to_musicstring(musicmat, 5);	

	v_cfugue::init_player();
	v_cfugue::play_musicstring(content);
	v_cfugue::dispose_player();
}


int main(int argc, char* argv[])
{
	//train(0.001);
	//train(0.0025);
	//train(0.005);
	//train(0.0075);
	//train(0.01);
	//train(0.025);
	//train(0.05);
	//train(0.075);
	//train(0.1);
	experiment(0.001);
	//experiment(0.0025);
	//experiment(0.005);
	//experiment(0.0075);
	//experiment(0.01);
	//experiment(0.025);
	//play();

	return 0;
}