#include "cfugue_adaptor.h"
#include <CFugueLib.h>
#include "string_util.h"
#include <sstream>

using namespace std;

namespace v_cfugue
{
	int note_to_index(std::string note)
	{
		int octave = 0;
		if(note.length() > 2) octave = note.at(2) - 48;
		note = to_lower(std::string(note.begin(), note.begin() + 2));
		int i = -1;
		if(i++ > -10 && note.compare("c ") == 0) return octave*12 + i;
		else if(i++ > -1 && note.compare("c#") == 0) return octave*12 + i;
		else if(i++ > -1 && note.compare("d ") == 0) return octave*12 + i;
		else if(i++ > -1 && note.compare("eb") == 0) return octave*12 + i;
		else if(i++ > -1 && note.compare("e ") == 0) return octave*12 + i;
		else if(i++ > -1 && note.compare("f ") == 0) return octave*12 + i;
		else if(i++ > -1 && note.compare("f#") == 0) return octave*12 + i;
		else if(i++ > -1 && note.compare("g ") == 0) return octave*12 + i;
		else if(i++ > -1 && note.compare("g#") == 0) return octave*12 + i;
		else if(i++ > -1 && note.compare("a ") == 0) return octave*12 + i;
		else if(i++ > -1 && note.compare("bb") == 0) return octave*12 + i;
		else if(i++ > -1 && note.compare("b ") == 0) return octave*12 + i;
		return -1;
	}

	std::string index_to_note(int i)
	{
		std::ostringstream ss;
		switch(i%12)
		{
		case 0: ss << "C"; break;
		case 1: ss << "C#"; break;
		case 2: ss << "D"; break;
		case 3: ss << "Eb"; break;
		case 4: ss << "E"; break;
		case 5: ss << "F"; break;
		case 6: ss << "F#"; break;
		case 7: ss << "G"; break;
		case 8: ss << "G#"; break;
		case 9: ss << "A"; break;
		case 10: ss << "Bb"; break;
		case 11: ss << "B"; break;
		}
		ss << i/12;
		ss << "/0.25a90d0";
		return ss.str();
	}

	std::string extract_note(std::string in)
	{
		char note[3];
		
		if(in.length() > 3)
		{
			note[2] = in.at(2);
			if(note[2] < 48 || note[2] > 57) note[2] = ' ';
		}

		if(in.length() > 2)
		{
			note[1] = in.at(1);
			if(note[1] != '#' && note[1] != 'b') 
			{
				if(note[1] >= 48 && note[1] <= 57) note[2] = note[1];
				note[1] = ' ';
			}
		}

		if(in.length() > 1)
		{
			note[0] = in.at(0);
		}

		return note;
	}

	cv::Mat musicstring_to_mat(std::string musicstring, int squeeze)
	{
		vector<string> elements;
		split_string(musicstring, elements, ' ');

		int max_so_far = 0, current;
		string token;
		for(int i = 0;i<elements.size();++i)
		{
			token = to_lower(trim(elements[i]));
			if(token.length() <= 0) continue;
			if(token.at(0) == '@') 
			{
				current = stoi(std::string(token.begin() + 1, token.end()));
				if(current > max_so_far) 
					max_so_far = current;
			}
		}

		//12 notes on all 11 octaves
		cv::Mat out = cv::Mat::zeros(12*11, max_so_far/squeeze + 1, CV_64FC1);
		auto current_col = out.col(0);

		for(int i = 0;i<elements.size();++i)
		{
			token = to_lower(trim(elements[i]));
			if(token.length() <= 0) continue;
			switch(token.at(0)) 
			{
				case 'v':
				break;
				case '@':
					current_col = out.col(stoi(std::string(token.begin() + 1, token.end()))/squeeze);
				break;
				case 'a':
				case 'b':
				case 'c':
				case 'd':
				case 'e':
				case 'f':
				case 'g':
					current_col.at<double>(note_to_index(extract_note(token))) = 1;
				break;
			}
		}

		return out;
	}

	std::string mat_to_musicstring(cv::Mat mat, int squeeze)
	{
		cv::Mat trans = mat.t();
		std::ostringstream ss;

		auto iter = trans.begin<double>();
		auto end = trans.end<double>();

		for(int j = 0;j<mat.cols;++j)
		{
			int track = 0;
			for(int i = 0; i<mat.rows;++i)
			{
				if(*iter++ > 0.5 && track < 16)
				{
					ss << "V" << track++ << " ";
					ss << "@" << j*squeeze << " ";
					ss << index_to_note(i) << " ";
				}
			}
		}
		return ss.str();
	}

	std::string replace_at(std::string in, std::function<int (int)> process)
	{
		std::string at_string = std::string(in.begin() + 1, in.end());
		std::ostringstream ss;
		ss << '@';
		ss << process(stoi(at_string));
		return ss.str();
	}

	std::string replace_duration(std::string in, std::function<float (float)> process)
	{
		auto start = std::find(in.begin(), in.end(), '/');
		if(start == in.end()) return in;
		auto end = std::find_if(start + 1, in.end(), std::isalpha);
		std::string duration_string = std::string(start + 1, end);
		
		std::ostringstream ss;
		ss << std::string(in.begin(), start + 1);
		ss << process(stof(duration_string));
		ss << "a90d0";
		//ss << std::string(end, in.end());
		return ss.str();
	}

	std::string format(std::string in)
	{
		std::string out;
		vector<string> elements;
		split_string(in, elements, ' ');

		string token;
		for(int i = 0;i<elements.size();++i)
		{
			token = to_lower(trim(elements[i]));
			if(token.length() <= 0) continue;
			switch(token.at(0)) 
			{
				case 'v':
					out.append(elements[i] + " ");
				break;
				case '@':
					out.append(replace_at(elements[i], [](int d) -> int { return d/20;} ) + " ");
				break;
				case 'a':
				case 'b':
				case 'c':
				case 'd':
				case 'e':
				case 'f':
				case 'g':
					out.append(replace_duration(elements[i], [](float d) -> float { return 0.25;} ) + " ");
				break;
			}
		}

		return out;
	}

	////////////////////////////// FILE SECTION /////////////////////////////////

	std::string file_to_musicstring(std::string filename)
	{
		std::string temp;
		std::string content;
		std::ifstream file;
		file.open(filename, std::ios::in);
		while(std::getline(file, temp))
		{
			content.append(temp);
		}
		return format(content);
	}

	void musicstring_to_file(std::string filename, std::string musicstring)
	{
		std::ofstream file;
		file.open(filename, std::ios::out);
		file << format(musicstring);
		file.close();
	}

	void read_list_file(std::string filename, std::vector<cv::Mat> &data, std::vector<int> &conditions)
	{
		data.clear();
		conditions.clear();
		
		std::string temp;
		std::string content;
		std::ifstream file;
		file.open(filename, std::ios::in);
		while(std::getline(file, temp))
		{
			if(temp[0] == '#') continue;
			std::vector<std::string> tokens;
			split_string(temp, tokens, ' ');

			//std::cout << tokens[0] << " " << tokens[1] << std::endl;
			std::string content = v_cfugue::file_to_musicstring(tokens[0]);
			cv::Mat musicmat = v_cfugue::musicstring_to_mat(content);
			data.push_back(musicmat);
			conditions.push_back(atoi(tokens[1].c_str()));
		}
	}

	////////////////////////////// PLAYER SECTION ///////////////////////////////

	bool has_inited = false;
	CFugue::Player* player = nullptr;
	void init_player()
	{
		if(!has_inited) 
		{
			int nPortID = MIDI_MAPPER, nTimerRes = 20;
			{
				unsigned int nOutPortCount = CFugue::GetMidiOutPortCount();
				if(nOutPortCount <= 0)
					exit(fprintf(stderr, "No MIDI Output Ports found !!"));
				if(nOutPortCount > 1)
				{
					_tprintf(_T("\n---------  The MIDI Out Port indices  --------\n\n"));
					/////////////////////////////////////////
					/// List the MIDI Ports
					for(unsigned int i=0; i < nOutPortCount; ++i)
					{
						std::string portName = CFugue::GetMidiOutPortName(i);
						std::wcout << "\t" << i << "\t: " << portName.c_str() << "\n";
					}
					//////////////////////////////////////////
					/// Chose a Midi output port
					_tprintf(_T("\nChoose your MIDI Port ID for the play:"));
					_tscanf(_T("%d"), &nPortID);
				}
				else
				{
					std::string portName = CFugue::GetMidiOutPortName(0);
					std::wcout << "\nUsing the MIDI output port: " << portName.c_str();
				}
			}
			player = new CFugue::Player(nPortID, nTimerRes);
			has_inited = true;
		}
	}

	void dispose_player()
	{
		if(player != nullptr) delete player;
	}

	void play_musicstring(std::string musicstring) 
	{
		if(player == nullptr) std::cout << "player must be initialized!" << std::endl;
		//player.Play(" V0 C D E F G A B V1 C/0.25 D/0.25 E/0.25 F/0.25 G A B "); // Play the Music Notes on the default MIDI output port
		player->Play(musicstring.c_str());
	}

}