#ifndef V_COMPOSER_NETWORK_H
#define V_COMPOSER_NETWORK_H

#include "network/v_util.h"
#include <vector>
#include <functional>

#ifdef OPENCV_3_0
#include <opencv2/cudaarithm.hpp>
using namespace cv::cuda;
#define cv_gpu cv::cuda
#else
#include <opencv2/gpu/gpu.hpp>
using namespace cv::gpu;
#define cv_gpu cv::gpu
#endif

namespace cv_ann
{

	class V_Composer_Network
    {
    protected:

        std::vector<cv::Mat> Ws_; //data to factor
		std::vector<cv::Mat> Us_; //data to residue
		std::vector<cv::Mat> Ss_; //residue to residue
        
		std::vector<cv::Mat> As_; // past data to factor
		std::vector<cv::Mat> Bs_; // past data to residue
		std::vector<cv::Mat> Cs_; // past residue to residue

		//same layer
		std::vector<cv::Mat> Ds_; // past data to data
		std::vector<cv::Mat> Es_; // past data to residue
		std::vector<cv::Mat> Fs_; // past residue to residue

        std::vector<int> recursive_steps_;

		//create a multiple copy of historical data for eact time step.
        static void arrange_data(std::vector<cv::Mat> data, int recursive_step, std::vector<cv::Mat> &hist);
        static cv::Mat arrange_data(cv::Mat data, int recursive_step);
		
		//create a historical data for the latest time step.
		static cv::Mat extract_hist(cv::Mat data, int recursive_step);
		static cv::Mat compose_hist(std::vector<cv::Mat> data, int recursive_step);
		
		static std::pair<cv::Mat, cv::Mat> sample(cv::Mat V_, cv::Mat W_, cv::Mat B_, cv::Mat Bp_, int iteration = 10);

    public:
		
        V_Composer_Network();
        ~V_Composer_Network();

        // t and t-1: recursive_steps = 1; t t-1 t-2: recursive steps = 2
		void add_layer(int num_factors, int num_residues, int recursive_steps);
		void set_residue_layer(int layer, int num_residues);

		void train_decomposition(std::vector<cv::Mat> data, std::vector<cv::Mat> conditions, double lambda = 0.0, bool pretrain = true);
		void train_reconstruction(std::vector<cv::Mat> data, std::vector<cv::Mat> conditions, double lambda = 0.0);

		double sample_sequence_and_compare(cv::Mat initial_frames, cv::Mat condition, int length, cv::Mat &errors, int compare_layer = -1);

        bool save(std::string file_name = "./composer.ann");
        bool load(std::string file_name = "./composer.ann");
    };

	void upload(cv::Mat data, cv_gpu::GpuMat &gpu);
	void upload(std::vector<cv::Mat> data, std::vector<cv_gpu::GpuMat> &gpu);
	void upload(cv::Mat transform, std::vector<cv::Mat> data, std::vector<cv_gpu::GpuMat> &gpu);
	void upload(cv::Mat T1, cv::Mat T2, std::vector<cv::Mat> data1, std::vector<cv::Mat> data2, std::vector<cv_gpu::GpuMat> &gpu);

	void download(cv_gpu::GpuMat gpu, cv::Mat &data);
	void download(const std::vector<cv_gpu::GpuMat> &gpu, std::vector<cv::Mat> &data);

}

#endif
