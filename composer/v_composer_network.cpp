#include "v_composer_network.h"
#include "trainer/v_gpu_bp.h"
#include "trainer/v_gpu_auc.h"
#include "trainer/v_gpu_rbm.h"
#include "util.h"
#include <iostream>
#include <sstream>
#include <limits>
#include <ctime>

using namespace cv;

namespace cv_ann
{

    V_Composer_Network::V_Composer_Network()
    {
	}

    V_Composer_Network::~V_Composer_Network()
    {	
	}

    void V_Composer_Network::add_layer(int num_factors, int num_residues, int recursive_steps)
    {
		if (this->Es_.size() > 0)
		{
			this->Ws_.push_back(cv::Mat::zeros(num_factors, this->Ds_.back().rows, CV_64FC1));
			this->Us_.push_back(cv::Mat::zeros(num_residues, this->Ds_.back().rows, CV_64FC1));
			this->Ss_.push_back(cv::Mat::zeros(num_residues, this->Es_.back().rows, CV_64FC1));
			this->As_.push_back(cv::Mat::zeros(num_factors, this->Ds_.back().rows*max(this->recursive_steps_[this->recursive_steps_.size() - 1], 1), CV_64FC1));
			this->Bs_.push_back(cv::Mat::zeros(num_residues, this->Ds_.back().rows*max(this->recursive_steps_[this->recursive_steps_.size() - 1], 1), CV_64FC1));
			this->Cs_.push_back(cv::Mat::zeros(num_residues, this->Es_.back().rows*max(this->recursive_steps_[this->recursive_steps_.size() - 1], 1), CV_64FC1));
		}
		this->Ds_.push_back(cv::Mat::zeros(num_factors, num_factors*max(recursive_steps, 1), CV_64FC1));
		this->Es_.push_back(cv::Mat::zeros(num_residues, num_factors*max(recursive_steps, 1), CV_64FC1));
		this->Fs_.push_back(cv::Mat::zeros(num_residues, num_residues*max(recursive_steps, 1), CV_64FC1));

        this->recursive_steps_.push_back(recursive_steps);
    }
	
    void V_Composer_Network::set_residue_layer(int layer, int num_residues)
    {
		int num_factors = this->Ds_[layer].rows;
		int recursive_steps = this->recursive_steps_[layer];
		if (layer > 0)
		{
			this->Us_[layer - 1] = cv::Mat::zeros(num_residues, this->Ds_[layer - 1].rows, CV_64FC1);
			this->Ss_[layer - 1] = cv::Mat::zeros(num_residues, this->Es_[layer - 1].rows, CV_64FC1);
			this->Bs_[layer - 1] = cv::Mat::zeros(num_residues, this->Ds_[layer - 1].rows*max(this->recursive_steps_[layer - 1], 1), CV_64FC1);
			this->Cs_[layer - 1] = cv::Mat::zeros(num_residues, this->Es_[layer - 1].rows*max(this->recursive_steps_[layer - 1], 1), CV_64FC1);
		}
		this->Es_[layer] = cv::Mat::zeros(num_residues, num_factors*max(recursive_steps, 1), CV_64FC1);
		this->Fs_[layer] = cv::Mat::zeros(num_residues, num_residues*max(recursive_steps, 1), CV_64FC1);
    }

	void V_Composer_Network::train_decomposition(std::vector<cv::Mat> data, std::vector<cv::Mat> conditions, double lambda, bool pretrain)
    {		

		///init random values
        {
            cv::RNG rand_seed = cv::RNG(std::time(0));
            for(unsigned int j = 1;j<Es_.size();++j)
            {
                rand_seed.fill(Ws_[j - 1],cv::RNG::NORMAL, 0, 0.001);
				rand_seed.fill(As_[j - 1], cv::RNG::NORMAL, 0, 0.001);
			}
        }

		std::vector<cv::Mat> V_; V_.resize(data.size());
		std::vector<cv::Mat> H_; H_.resize(data.size());
		for(int j = 0;j<data.size();++j) 
		{
			V_[j] = data[j];
			H_[j] = conditions[j]*cv::Mat::ones(1, data[j].cols, CV_64FC1);
		}
		
		//train layer
        int i = pretrain? 1 : Es_.size()-3;
        for(;i<Es_.size()-3;++i)
        {
			std::vector<cv::Mat> hist;
            cv::Mat bias = cv::Mat::zeros(Ds_[i].rows,1,CV_64FC1);
			V_Composer_Network::arrange_data(V_, this->recursive_steps_[i], hist);

			cv_ann::V_Parameter_AUC params;
			params.error_threshold = 1e-6;
			params.lambda = 0.001;
			params.learning_rate = 0.01;
			params.max_iteration = 10000;
			params.momentum = 0.9;
			params.with_bias = false;

			cv_gpu::GpuMat gW,gB,gA;
			std::vector<cv_gpu::GpuMat> gVs, gHs, gDs;
			upload(Ws_[i - 1], gW);
			upload(bias, gB);
			upload(As_[i - 1], gA);
			upload(V_, gVs);
			upload(hist, gHs);
			upload(Ds_[i - 1], hist, gDs);
			
			cv_ann::auc_relu_train(gVs, gHs, gW, gB, gA, gDs, params);
				
			download(gW, Ws_[i - 1]);
			download(gA, As_[i - 1]);
			
			for(int k = 0;k<V_.size();++k)
			{
				V_[k] = relu_activate(Ws_[i - 1] * V_[k] + As_[i - 1] * hist[k]);
			}
        }
		
		{

			std::vector<cv_gpu::GpuMat> gVs, gHs, gWs, gAs;
			upload(Ws_, gWs);
			upload(As_, gAs);
			upload(data, gVs);
			upload(H_, gHs);

			cv_ann::V_Parameter_BP params;
			params.error_threshold = 1e-6;
			params.lambda = 0.001;
			params.learning_rate = 0.01;
			params.max_iteration = 20000;
			params.momentum = 0.9;

			cv_ann::backpropagation(gVs, gHs, gWs, gAs, this->recursive_steps_, Es_.size() - 2, params);

			download(gWs, Ws_);
			download(gAs, As_);
		}
    }

	void V_Composer_Network::train_reconstruction(std::vector<cv::Mat> data, std::vector<cv::Mat> conditions, double lambda)
	{
		///init random values
		{
			cv::RNG rand_seed = cv::RNG(std::time(0));
			for (unsigned int j = 1; j<Es_.size(); ++j)
			{
				rand_seed.fill(Us_[j - 1], cv::RNG::NORMAL, 0, 0.01);
				rand_seed.fill(Ss_[j - 1], cv::RNG::NORMAL, 0, 0.01);
				rand_seed.fill(Bs_[j - 1], cv::RNG::NORMAL, 0, 0.01);
				rand_seed.fill(Cs_[j - 1], cv::RNG::NORMAL, 0, 0.01);
			}
		}

		std::vector<cv::Mat> resd; resd.resize(data.size());
		for (int i = 0; i < data.size(); ++i)
		{
			resd[i] = cv::Mat::zeros(Fs_[0].rows, data[i].cols, CV_64FC1);
		}

		//train first layer bias
		std::vector<cv::Mat> hist_data;
		std::vector<cv::Mat> hist_resd;

		std::function<void(int)> train_bias = [&](int depth)
		{
			cv::Mat bias_data = cv::Mat::zeros(Ds_[depth].rows, 1, CV_64FC1);
			cv::Mat bias_resd = cv::Mat::zeros(Es_[depth].rows, 1, CV_64FC1);

			V_Composer_Network::arrange_data(data, this->recursive_steps_[depth], hist_data);
			V_Composer_Network::arrange_data(resd, this->recursive_steps_[depth], hist_resd);

			cv_ann::V_Parameter_AUC params;
			params.error_threshold = 1e-6;
			params.lambda = 0.0;
			params.learning_rate = 0.002;
			params.max_iteration = 20000;
			params.momentum = 0.9;
			params.with_bias = false;

			cv_gpu::GpuMat gD, gE, gF, gBias_data, gBias_resd;
			std::vector<cv_gpu::GpuMat> gVs, gXs, gH_resd, gH_data;
			upload(Ds_[depth], gD);
			upload(Es_[depth], gE);
			upload(Fs_[depth], gF);
			upload(bias_data, gBias_data);
			upload(bias_resd, gBias_resd);
			upload(hist_data, gH_data);
			upload(hist_resd, gH_resd);
			upload(data, gVs);
			upload(resd, gXs);

			cv_ann::supervise_relu_train(gH_data, gVs, gD, gBias_data, params);
			cv_ann::supervise_relu_train(gH_data, gXs, gE, gBias_resd, params);
			cv_ann::supervise_relu_train(gH_resd, gXs, gF, gBias_resd, params);

			download(gD, Ds_[depth]);
			download(gE, Es_[depth]);
			download(gF, Fs_[depth]);
		};

		train_bias(0);
		
		//train layer
		std::vector<cv::Mat> next_data; next_data.resize(data.size());
		unsigned int i = 1;
		for (; i<Es_.size() - 1; ++i)
		{
			for (unsigned int k = 0; k < data.size(); ++k)
			{
				next_data[k] = relu_activate(Ws_[i - 1] * data[k] + As_[i - 1] * hist_data[k]);
			}

			cv_ann::V_Parameter_AUC params;
			params.error_threshold = 1e-8;
			params.lambda = lambda;
			params.learning_rate = 0.01;
			params.max_iteration = 20000;
			params.momentum = 0.9;
			params.with_bias = false;

			
			cv::Mat bias_factor_residue = cv::Mat::zeros(Us_[i - 1].rows, 1, CV_64FC1);
			cv_gpu::GpuMat gS, gU, gB, gC, gBias_factor_residue;
			std::vector<cv_gpu::GpuMat> gXs, gVs, gH_data, gH_resd, gBias_data, gBias_resd;
			upload(Ss_[i - 1], gS);
			upload(Us_[i - 1], gU);
			upload(Bs_[i - 1], gB);
			upload(Cs_[i - 1], gC);
			
			upload(data, gVs);
			upload(resd, gXs);
			upload(hist_data, gH_data);
			upload(hist_resd, gH_resd);
			upload(bias_factor_residue, gBias_factor_residue);
			upload(Ds_[i - 1], Ws_[i - 1].t(), hist_data, next_data, gBias_data);
			upload(Es_[i - 1], Fs_[i - 1], hist_data, hist_resd, gBias_resd);

			//Es_ past data to residue
			//Fs_ past residue to residue
			
			cv_ann::auc_relu_train(gVs, gXs, gH_data, gH_resd, gU, gS, gB, gC, gBias_data, gBias_resd, params);
			
			download(gU, Us_[i - 1]); // data to residue
			download(gS, Ss_[i - 1]); // residue to residue
			download(gB, Bs_[i - 1]); // past data to residue
			download(gC, Cs_[i - 1]); // past residue to residue

			for (unsigned int k = 0; k<data.size(); ++k)
			{
				resd[k] = relu_activate(Us_[i - 1] * data[k] + Ss_[i - 1] * resd[k] + Bs_[i - 1] * hist_data[k] + Cs_[i - 1] * hist_resd[k]);
				data[k] = next_data[k];
			}
			
			train_bias(i);
		}

		//train belief net layer (only bottom level recursion)
		{
			cv::Mat bias_resd = cv::Mat::zeros(Es_[i].rows, 1, CV_64FC1);

			cv_ann::V_Parameter_RBM params;
			params.cd_round = 10;
			params.lambda = 0.0;
			params.learning_rate = 0.001;
			params.max_iteration = 10000;
			params.momentum = 0.9;
			params.with_bias = false;

			cv_gpu::GpuMat gS, gB, gU;
			std::vector<cv_gpu::GpuMat> gVs, gXs, gBias_resd;
			upload(Ss_[i - 1], gS);
			upload(Us_[i - 1], gU);
			upload(bias_resd, gB);
			upload(data, gVs);
			upload(resd, gXs);
			upload(Es_[i - 1], Fs_[i - 1], hist_data, hist_resd, gBias_resd);

			cv_ann::rbm_relu_train(gXs, gVs, gS, gB, gU, gBias_resd, params);

			download(gS, Ss_[i - 1]);
			download(gU, Us_[i - 1]);
		}

	}

	void V_Composer_Network::arrange_data(std::vector<cv::Mat> data, int recursive_step, std::vector<cv::Mat> &hists)
	{
		hists.clear();
		for (int k = 0; k<data.size(); ++k)
		{
			cv::Mat dummy = cv::Mat::zeros(data[k].rows, 1, CV_64FC1);
			int num_rows = max(data[k].rows*recursive_step, 1);
			cv::Mat hist = cv::Mat(num_rows, data[k].cols, CV_64FC1);

			for (int j = 0; j<data[k].cols; ++j)
			{
				int f = 0, t;
				cv::Mat datum = hist.col(j);
				if (recursive_step == 0) datum = cv::Mat::zeros(data[k].rows, 1, CV_64FC1);
				for (int s = recursive_step; s>0; --s)
				{
					t = f + data[k].rows;
					if (j - s >= 0) data[k].col(j - s).copyTo(datum.rowRange(f, t));
					else dummy.copyTo(datum.rowRange(f, t));
					f = t;
				}
			}

			hists.push_back(hist);
		}
	}

	cv::Mat V_Composer_Network::arrange_data(cv::Mat data, int recursive_step)
	{
		cv::Mat dummy = cv::Mat::zeros(data.rows, 1, CV_64FC1);

		int num_rows = max(data.rows*recursive_step, 1);
		cv::Mat hist = cv::Mat(num_rows, data.cols, CV_64FC1);

		for (int j = 0; j<data.cols; ++j)
		{
			int f = 0, t;
			cv::Mat datum = hist.col(j);
			if (recursive_step == 0) datum = cv::Mat::zeros(data.rows, 1, CV_64FC1);
			for (int s = recursive_step; s>0; --s)
			{
				t = f + data.rows;
				if (j - s >= 0) data.col(j - s).copyTo(datum.rowRange(f, t));
				else dummy.copyTo(datum.rowRange(f, t));
				f = t;
			}
		}

		return hist;
	}

	cv::Mat V_Composer_Network::extract_hist(cv::Mat data, int recursive_step)
	{
		cv::Mat res;
        if(recursive_step == 0) res = cv::Mat::zeros(data.rows,1,CV_64FC1);
		for(int i = recursive_step; i>0; --i)
		{
			res.push_back(data.col(data.cols - i));		
		}
		return res;
	}

	cv::Mat V_Composer_Network::compose_hist(std::vector<cv::Mat> data, int recursive_step)
	{
		cv::Mat res;
        if(recursive_step == 0) res = cv::Mat::zeros(data[0].rows,1,CV_64FC1);
		for(int i = recursive_step; i>0; --i)
		{
			res.push_back(data[data.size() - i]);		
		}
		return res;
	}

	std::pair<cv::Mat, cv::Mat> V_Composer_Network::sample(cv::Mat X_, cv::Mat S_, cv::Mat B_, cv::Mat Bp_, int iteration)
    {
        cv::Mat H_;
        cv::Mat S_t = S_.t();

        for(int j = 0;j<iteration;++j)
        {
			H_ = relu_activate(S_*X_ + B_);
			X_ = relu_sample(S_t*H_ + Bp_);
        }

        return std::make_pair(X_,H_);
    }


	double V_Composer_Network::sample_sequence_and_compare(cv::Mat initial_frames, cv::Mat condition, int length, cv::Mat &errors, int compare_layer)
	{
		if(errors.empty() || errors.rows != length || errors.cols != 1) {
			errors.create(length, 1, CV_64FC1);
			errors.setTo(0);
		}

		int i,j;
		cv::Mat hist_data, hist_resd;
		cv::Mat data = initial_frames;
		cv::Mat resd = cv::Mat::zeros(Fs_[0].rows, data.cols, CV_64FC1);
		cv::Mat V_ = condition;

		//propagate upward
		std::vector<std::vector<cv::Mat>> hist_data_layer_step;
		std::vector<std::vector<cv::Mat>> hist_resd_layer_step;
		hist_data_layer_step.resize(Ds_.size());
		hist_resd_layer_step.resize(Ds_.size());
		for (i = 0; i<Ds_.size() - 1; ++i)
		{
			for (j = 0; j<initial_frames.cols; ++j)
			{
				hist_data_layer_step[i].push_back(data.col(j));
				hist_resd_layer_step[i].push_back(resd.col(j));
			}
			hist_data = arrange_data(data, this->recursive_steps_[i]);
			hist_resd = arrange_data(resd, this->recursive_steps_[i]);
			resd = relu_activate(Us_[i] * data + Ss_[i] * resd + Bs_[i] * hist_data + Cs_[i] * hist_resd);
			data = relu_activate(Ws_[i] * data + As_[i] * hist_data);
		}
		for(j = 0; j<initial_frames.cols; ++j)
		{
			hist_data_layer_step[i].push_back(data.col(j));
			hist_resd_layer_step[i].push_back(resd.col(j));
		}

		if (compare_layer < 0) compare_layer = Es_.size() - 3;
		double sum_error = 0;
		for(int k = 0; k < length; ++k)
		{
			cv::Mat RBM_bot_bias = 
				Es_[Es_.size() - 2] * compose_hist(hist_data_layer_step[Es_.size() - 2], this->recursive_steps_[Es_.size() - 2]) +
				Fs_[Fs_.size() - 2] * compose_hist(hist_resd_layer_step[Fs_.size() - 2], this->recursive_steps_[Fs_.size() - 2]);
			cv::Mat X_ = relu_sample(RBM_bot_bias);
			X_ = V_Composer_Network::sample(
				X_, 
				Ss_[Ss_.size() - 1], 
				Us_[Us_.size() - 1] * V_ +
				Bs_[Bs_.size() - 2] * compose_hist(hist_data_layer_step[Bs_.size() - 2], this->recursive_steps_[Bs_.size() - 2]) +
				Cs_[Cs_.size() - 2] * compose_hist(hist_resd_layer_step[Cs_.size() - 2], this->recursive_steps_[Cs_.size() - 2]),
				RBM_bot_bias
				).first;
			
			hist_data_layer_step[Es_.size() - 2].push_back(V_);
			hist_resd_layer_step[Es_.size() - 2].push_back(X_);

			for (i = Es_.size() - 3; i>0; --i)
			{
				V_ = relu_activate(
					Ws_[i].t() * V_ +
					Us_[i].t() * X_ +
					Ds_[i] * compose_hist(hist_data_layer_step[i], this->recursive_steps_[i])
					);
				X_ = relu_activate(
					Ss_[i].t() * X_ +
					Es_[i] * compose_hist(hist_data_layer_step[i], this->recursive_steps_[i]) +
					Fs_[i] * compose_hist(hist_resd_layer_step[i], this->recursive_steps_[i])
					);

				hist_data_layer_step[i].push_back(V_);
				hist_resd_layer_step[i].push_back(X_);
			}
			V_ = relu_activate(
				Ws_[i].t() * V_ +
				Us_[i].t() * X_ +
				Ds_[i] * compose_hist(hist_data_layer_step[i], this->recursive_steps_[i])
				);
			X_ = cv::Mat::zeros(Fs_[0].rows, X_.cols, CV_64FC1);
			//V_ = cv_ann::threshold(V_, 0.5);
			hist_data_layer_step[i].push_back(V_);
			hist_resd_layer_step[i].push_back(X_);

			for (i = 0; i<compare_layer; ++i)
			{
				V_ = relu_activate(
					Ws_[i] * V_ + 
					As_[i] * compose_hist(hist_data_layer_step[i], this->recursive_steps_[i])
					);
			}
			V_ = relu_activate(
				Ws_[i] * V_ + 
				As_[i] * compose_hist(hist_data_layer_step[i], this->recursive_steps_[i])
			);
			
			cv::Mat diff = V_ - hist_data_layer_step[i+1].back();
			cv::Mat out;
			cv::multiply(diff, diff, out);
			sum_error += cv::sum(out)[0];
			errors.at<double>(k) = (cv::sum(out)[0]);

			double minVal, maxVal;
			int minIdx, maxIdx;
			cv::minMaxIdx(V_, &minVal, &maxVal, &minIdx, &maxIdx);
			V_ = cv::Mat::zeros(V_.rows, 1, CV_64FC1);
			V_.row(maxIdx).setTo(1);
		}
		
		return sum_error/length;
	}

    bool V_Composer_Network::save(std::string file_name)
    {
        cv::FileStorage fs(file_name, cv::FileStorage::WRITE);
        if(!fs.isOpened()) return false;
        fs << "num_layers" << ((int) Es_.size());
        fs << "layers" << "[";
            fs << "{";
			fs << "D" << Ds_[0];
			fs << "E" << Es_[0];
			fs << "F" << Fs_[0];
            fs << "recur" << recursive_steps_[0];
            fs << "}";
        for(unsigned int i = 1;i<Es_.size();++i)
        {
            fs << "{";
			fs << "W" << Ws_[i - 1];
			fs << "U" << Us_[i - 1];
			fs << "S" << Ss_[i - 1];
			fs << "A" << As_[i - 1];
			fs << "B" << Bs_[i - 1];
			fs << "C" << Cs_[i - 1];
			fs << "D" << Ds_[i];
			fs << "E" << Es_[i];
			fs << "F" << Fs_[i];
            fs << "recur" << recursive_steps_[i];
            fs << "}";
        }
        fs << "]";
        fs.release();
        return true;
    }
	
    bool V_Composer_Network::load(std::string file_name)
    {
        cv::FileStorage fs(file_name, cv::FileStorage::READ);
        if(!fs.isOpened()) return false;
        int num_layers = int(fs["num_layers"]);
		Ws_.resize(num_layers - 1);
		Us_.resize(num_layers - 1);
		Ss_.resize(num_layers - 1);
		As_.resize(num_layers - 1);
		Bs_.resize(num_layers - 1);
		Cs_.resize(num_layers - 1);
		Ds_.resize(num_layers);
		Es_.resize(num_layers);
		Fs_.resize(num_layers);
		this->recursive_steps_.resize(num_layers);
        FileNode tl = fs["layers"];
        {
            FileNode fn = tl[0];
			fn["D"] >> Ds_[0];
			fn["E"] >> Es_[0];
			fn["F"] >> Fs_[0];
			fn["recur"] >> this->recursive_steps_[0];
        }
        for(unsigned int i = 1;i<num_layers;++i)
        {
			FileNode fn = tl[i];
			fn["W"] >> Ws_[i - 1];
			fn["U"] >> Us_[i - 1];
			fn["S"] >> Ss_[i - 1];
			fn["A"] >> As_[i - 1];
			fn["B"] >> Bs_[i - 1];
			fn["C"] >> Cs_[i - 1];
			fn["D"] >> Ds_[i];
			fn["E"] >> Es_[i];
			fn["F"] >> Fs_[i];
			fn["recur"] >> this->recursive_steps_[i];
        }
        fs.release();
        return true;
    }

	void upload(cv::Mat data, cv_gpu::GpuMat &gpu)
	{
		cv::Mat float_mat;
		data.convertTo(float_mat, CV_32FC1);
		gpu.upload(float_mat);
	}
	
	void upload(std::vector<cv::Mat> data, std::vector<cv_gpu::GpuMat> &gpu)
	{
		gpu.resize(data.size());
		for (unsigned int k = 0; k<data.size(); ++k)
		{
			cv::Mat float_mat;
			data[k].convertTo(float_mat, CV_32FC1);
			gpu[k].upload(float_mat);
		}
	}

	void upload(cv::Mat T, std::vector<cv::Mat> data, std::vector<cv_gpu::GpuMat> &gpu)
	{
		gpu.resize(data.size());
		for (unsigned int k = 0; k<data.size(); ++k)
		{
			cv::Mat float_mat;
			cv::Mat temp = T*data[k];
			temp.convertTo(float_mat, CV_32FC1);
			gpu[k].upload(float_mat);
		}
	}

	void upload(cv::Mat T1, cv::Mat T2, std::vector<cv::Mat> data1, std::vector<cv::Mat> data2, std::vector<cv_gpu::GpuMat> &gpu)
	{
		gpu.resize(data1.size());
		for (unsigned int k = 0; k<data1.size(); ++k)
		{
			cv::Mat float_mat;
			cv::Mat temp = T1*data1[k] + T2*data2[k];
			temp.convertTo(float_mat, CV_32FC1);
			gpu[k].upload(float_mat);
		}
	}

	void download(cv_gpu::GpuMat gpu, cv::Mat &data)
	{
		cv::Mat temp;
		gpu.download(temp);
		temp.convertTo(data, data.type());
	}

	void download(const std::vector<cv_gpu::GpuMat> &gpu, std::vector<cv::Mat> &data)
	{
		for(int i = 0; i < gpu.size(); ++i)
		{
			download(gpu[i], data[i]);
		}
	}

}
