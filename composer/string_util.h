#ifndef STRING_UTIL_H
#define STRING_UTIL_H

#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <vector>
#include <iostream>
#include <fstream>

// trim from both ends
static inline std::string trim(std::string s) {
	return std::string(
		std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))),
		std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base()
	);
}

static void split_string(std::string in, std::vector<std::string> &out, char delim)
{
	std::istringstream f(in);
	std::string s;    
	while (std::getline(f, s, delim)) {
		out.push_back(s);
	}
}

// trim from both ends
static inline std::string to_lower(std::string s) {
	std::string out;
	out.resize(s.length());
	std::transform(s.begin(), s.end(), out.begin(), ::tolower);
    return out;
}


#endif