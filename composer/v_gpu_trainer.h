#ifndef V_GPU_TRAINER_H
#define V_GPU_TRAINER_H

#include <vector>
#include "trainer/v_gpu_auc.h"
#include "trainer/v_gpu_rbm.h"
#include "trainer/v_gpu_util.h"

namespace cv_ann
{
	void modify(const GpuMat& x, GpuMat& dst, int num_factors, int activate);
	void prelum(const GpuMat& x, GpuMat& dst, int num_factors, int activate);
	void replace_dH(const GpuMat& H, const GpuMat& pH, const GpuMat& x, GpuMat& dst, int num_factors, int activate, GpuMat& buff);
	void sample_gaussian(const GpuMat& x, GpuMat& dst, GPU_Random& seed, int num_factors, int activate);

	//tie with minibatch
	void supervise_relu_train(
		std::vector<GpuMat> data, std::vector<GpuMat> labels, GpuMat W, GpuMat B,
		V_Parameter_AUC params, int num_factors, int activate
		);

	//with minibatch
	//Bp will not be trained, so it must have the dimension of (W.cols x data.cols).
	void auc_relu_train(
		std::vector<GpuMat> data, std::vector<GpuMat> condition, GpuMat W, GpuMat B, GpuMat U, std::vector<GpuMat> Bp,
		V_Parameter_AUC params, int num_factors, int activate
		);

	void rbm_relu_train(
		std::vector<GpuMat> data, std::vector<GpuMat> condition, GpuMat W, GpuMat B, GpuMat U, std::vector<GpuMat> Bp,
		V_Parameter_RBM params, int num_factors, int activate
		);

}

#endif
