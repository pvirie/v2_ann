#ifndef V_LSTM_H
#define V_LSTM_H

#include <vector>
#include <functional>
#include <opencv2/opencv.hpp>
#include <stack>

#ifdef OPENCV_3_0
	#include <opencv2/cudaarithm.hpp>
	using namespace cv::cuda;
#else
	#include <opencv2/gpu/gpu.hpp>
	using namespace cv::gpu;
#endif

namespace cv_ann
{

	struct V_Parameter_LSTM
	{
		V_Parameter_LSTM() : learning_rate(1), error_threshold(1e-5), max_iteration(20000), momentum(0.9), lambda(0.0), batchPortion(1.0) {}
		V_Parameter_LSTM(double _learning_rate) : learning_rate(_learning_rate), error_threshold(1e-6), max_iteration(10000), momentum(0.9), lambda(0.0), batchPortion(1.0) {}

		double learning_rate;
		double error_threshold;
		int max_iteration;
		double momentum;
		double lambda;
		double batchPortion;
	};

	struct LSTM_Activation{

		LSTM_Activation(GpuMat& i0, GpuMat& i1, GpuMat& i2, GpuMat& i3, GpuMat& i4, GpuMat& i5, GpuMat& i6, GpuMat& i7):
			input(i0), previous_memory(i1), previous_output(i2), forget_gate(i3), input_gate(i4), output_gate(i5), pre_memory(i6), tanh_memory(i7)
		{
		}

		GpuMat input;
		GpuMat previous_memory;
		GpuMat previous_output;
		GpuMat forget_gate;
		GpuMat input_gate;
		GpuMat output_gate;
		GpuMat pre_memory;
		GpuMat tanh_memory;
	};

	class V_Long_Short_term_Memory
    {
    protected:
		GpuMat Wf_x;
		GpuMat Wf_h;
		GpuMat Bf;

		GpuMat Wi_x;
		GpuMat Wi_h;
		GpuMat Bi;

		GpuMat Wc_x;
		GpuMat Wc_h;
		GpuMat Bc;

		GpuMat Wo_x;
		GpuMat Wo_h;
		GpuMat Bo;

		//input, previous_memory, previous_output, forget_gate, input_gate, output_gate, pre_memory, tanhMemory
		std::stack<LSTM_Activation> stack_;

		// sum

		GpuMat sWf_x;
		GpuMat sWf_h;
		GpuMat sBf;

		GpuMat sWi_x;
		GpuMat sWi_h;
		GpuMat sBi;

		GpuMat sWc_x;
		GpuMat sWc_h;
		GpuMat sBc;

		GpuMat sWo_x;
		GpuMat sWo_h;
		GpuMat sBo;

		// momentum

		GpuMat mWf_x;
		GpuMat mWf_h;
		GpuMat mBf;

		GpuMat mWi_x;
		GpuMat mWi_h;
		GpuMat mBi;

		GpuMat mWc_x;
		GpuMat mWc_h;
		GpuMat mBc;

		GpuMat mWo_x;
		GpuMat mWo_h;
		GpuMat mBo;

		double sum_error;

		static void init_weight(GpuMat& weight, cv::Size s);
		static void update(GpuMat& weight, GpuMat& delta, double deltaWeight, GpuMat& momentum, double momentum_weight, V_Parameter_LSTM params);

    public:
		void initialize_weights(int input_size, int output_size, int memory_size);
		std::pair<GpuMat, GpuMat> activate(GpuMat input, GpuMat memory_state, GpuMat previous_output, bool push_stack = false);
		std::tuple<GpuMat, GpuMat, GpuMat> back_propagate(GpuMat target_grad, GpuMat memory_grad, GpuMat previous_output_grad, int num_samples, int num_sequence, bool pop_stack = false);
		double update_weights(V_Parameter_LSTM params, double momentum);
		void reset_gradients();
		void reset_momentums();

		void train(std::vector<GpuMat> inputs, std::vector<GpuMat> outputs, V_Parameter_LSTM param = V_Parameter_LSTM());

        V_Long_Short_term_Memory();
        ~V_Long_Short_term_Memory();
    };

	class Deep_LSTM
	{
	protected:
		std::vector<V_Long_Short_term_Memory> cells;

	public:
		Deep_LSTM()
		{
		}

		~Deep_LSTM()
		{
		}
		
		void set_layers(int input_size, std::vector<int> output_sizes);

		void train(std::vector<GpuMat> inputs, std::vector<GpuMat> outputs, V_Parameter_LSTM params = V_Parameter_LSTM());
		void activate(std::vector<GpuMat> inputs, std::vector<GpuMat> &outputs);
	};


}

#endif
