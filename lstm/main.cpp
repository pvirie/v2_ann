#include <iostream>
#include <opencv2/opencv.hpp>
#include "lstm.h"
#include "trainer/v_gpu_util.h"

using namespace cv_ann;

int main(int argc, char* argv[])
{
	cv_ann::Deep_LSTM lstms;
	
	cv::Mat fifth	= (cv::Mat_<float>(1, 8)	<< 1, 1, 1, 1, 1, 0, 0, 0);
	cv::Mat forth	= (cv::Mat_<float>(1, 8)	<< 1, 1, 1, 0, 0, 0, 1, 1);
	cv::Mat third	= (cv::Mat_<float>(1, 8)	<< 1, 1, 0, 0, 1, 1, 0, 0);
	cv::Mat second = (cv::Mat_<float>(1, 8)		<< 1, 0, 1, 0, 1, 0, 1, 0);
	cv::Mat first = (cv::Mat_<float>(1, 8)		<< 1, 1, 1, 1, 0, 0, 0, 0);
	
	std::vector<GpuMat> inputs; 
	inputs.push_back(GpuMat(first));
	inputs.push_back(GpuMat(second));
	inputs.push_back(GpuMat(third));
	inputs.push_back(GpuMat(forth));
	inputs.push_back(GpuMat(fifth));

	//is even, must run many times to get a good starting point. perfect final error is less than 0.05
	cv::Mat fifth_o	= (cv::Mat_<float>(1, 8)	<< 1, 1, 0, 1, 0, 0, 0, 0);
	cv::Mat forth_o	= (cv::Mat_<float>(1, 8)	<< 1, 0, 0, 1, 1, 0, 0, 1);
	cv::Mat third_o = (cv::Mat_<float>(1, 8)	<< 1, 0, 1, 0, 0, 1, 0, 1);
	cv::Mat second_o = (cv::Mat_<float>(1, 8)	<< 0, 0, 0, 0, 1, 1, 1, 1);
	cv::Mat first_o = (cv::Mat_<float>(1, 8)	<< 0, 0, 0, 0, 0, 0, 0, 0);

	std::vector<GpuMat> outputs;
	outputs.push_back(GpuMat(first_o));
	outputs.push_back(GpuMat(second_o));
	outputs.push_back(GpuMat(third_o));
	outputs.push_back(GpuMat(forth_o));
	outputs.push_back(GpuMat(fifth_o));

	std::vector<int> layer_sizes;
	layer_sizes.push_back(10);
	layer_sizes.push_back(1);

	lstms.set_layers(first.rows, layer_sizes);
	lstms.train(inputs, outputs, V_Parameter_LSTM(10));
		
	std::cout << "Finished training" << std::endl;
	
	cv::Mat in_sixth	= (cv::Mat_<float>(1, 10)	<< 1, 0, 1, 1, 1, 0, 1, 1, 0, 0);
	cv::Mat in_fifth	= (cv::Mat_<float>(1, 10)	<< 1, 1, 1, 1, 1, 0, 0, 0, 0, 0);
	cv::Mat in_forth	= (cv::Mat_<float>(1, 10)	<< 1, 1, 1, 1, 0, 0, 0, 0, 1, 1);
	cv::Mat in_third = (cv::Mat_<float>(1, 10)		<< 1, 1, 1, 0, 0, 0, 1, 1, 1, 0);
	cv::Mat in_second = (cv::Mat_<float>(1, 10)		<< 1, 1, 0, 0, 1, 1, 0, 0, 1, 1);
	cv::Mat in_first = (cv::Mat_<float>(1, 10)		<< 1, 0, 1, 0, 1, 0, 1, 0, 1, 0);

	inputs.clear();
	inputs.push_back(GpuMat(in_first));
	inputs.push_back(GpuMat(in_second));
	inputs.push_back(GpuMat(in_third));
	inputs.push_back(GpuMat(in_forth));
	inputs.push_back(GpuMat(in_fifth));
	inputs.push_back(GpuMat(in_sixth));

	std::vector<GpuMat> outputs_;
	lstms.activate(inputs, outputs_);

	for (int i = outputs_.size() - 1; i >= 0; --i)
	{
		cv::Mat out;
		outputs_[i].download(out);
		cv::threshold(out, out, 0.5, 1.0, CV_THRESH_BINARY);
		std::cout << out << std::endl;
	}
	
	return 0;
}