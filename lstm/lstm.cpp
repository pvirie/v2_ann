#include "lstm.h"
#include "trainer/v_gpu_util.h"
#include <iostream>
#include <sstream>
#include <limits>
#include <ctime>
#include <tuple>

namespace cv_ann
{

	V_Long_Short_term_Memory::V_Long_Short_term_Memory()
    {
	}

    V_Long_Short_term_Memory::~V_Long_Short_term_Memory()
    {	
	}


	void V_Long_Short_term_Memory::init_weight(GpuMat& weight, cv::Size s)
	{
		cv::Mat temp(s, CV_32FC1);
		cv::RNG rand_seed = cv::RNG(std::time(0));
		rand_seed.fill(temp,cv::RNG::UNIFORM,-0.01,0.01);
		weight.upload(temp);
	}

	void V_Long_Short_term_Memory::initialize_weights(int input_size, int output_size, int memory_size)
	{
		init_weight(Wf_x, cv::Size(input_size, memory_size));
		init_weight(Wf_h, cv::Size(output_size, memory_size));
		Bf.create(cv::Size(1, memory_size), CV_32FC1);
		Bf.setTo(1);

		init_weight(Wc_x, cv::Size(input_size, memory_size));
		init_weight(Wc_h, cv::Size(output_size, memory_size));
		Bc.create(cv::Size(1, memory_size), CV_32FC1);
		Bc.setTo(0);

		init_weight(Wi_x, cv::Size(input_size, memory_size));
		init_weight(Wi_h, cv::Size(output_size, memory_size));
		Bi.create(cv::Size(1, memory_size), CV_32FC1);
		Bi.setTo(1);

		init_weight(Wo_x, cv::Size(input_size, output_size));
		init_weight(Wo_h, cv::Size(output_size, output_size));
		Bo.create(cv::Size(1, output_size), CV_32FC1);
		Bo.setTo(1);

		sWf_x.create(Wf_x.size(), Wf_x.type());
		sWf_h.create(Wf_h.size(), Wf_h.type());
		sBf.create(Bf.size(), Bf.type());
		sWi_x.create(Wi_x.size(), Wi_x.type());
		sWi_h.create(Wi_h.size(), Wi_h.type());
		sBi.create(Bi.size(), Bi.type());
		sWc_x.create(Wc_x.size(), Wc_x.type());
		sWc_h.create(Wc_h.size(), Wc_h.type());
		sBc.create(Bc.size(), Bc.type());
		sWo_x.create(Wo_x.size(), Wo_x.type());
		sWo_h.create(Wo_h.size(), Wo_h.type());
		sBo.create(Bo.size(), Bo.type());

		mWf_x.create(Wf_x.size(), Wf_x.type());
		mWf_h.create(Wf_h.size(), Wf_h.type());
		mBf.create(Bf.size(), Bf.type());
		mWi_x.create(Wi_x.size(), Wi_x.type());
		mWi_h.create(Wi_h.size(), Wi_h.type());
		mBi.create(Bi.size(), Bi.type());
		mWc_x.create(Wc_x.size(), Wc_x.type());
		mWc_h.create(Wc_h.size(), Wc_h.type());
		mBc.create(Bc.size(), Bc.type());
		mWo_x.create(Wo_x.size(), Wo_x.type());
		mWo_h.create(Wo_h.size(), Wo_h.type());
		mBo.create(Bo.size(), Bo.type());
	}

	std::pair<GpuMat, GpuMat> V_Long_Short_term_Memory::activate(GpuMat x, GpuMat c, GpuMat h, bool push_stack)
	{
		if (c.empty())
		{
			c = GpuMat(Wc_h.rows, x.cols, CV_32FC1);
			c.setTo(0);
		}

		if (h.empty())
		{
			h = GpuMat(Wo_h.rows, x.cols, CV_32FC1);
			h.setTo(0);
		}

		GpuMat buffer;
		GpuMat H, X; 
		
		GpuMat bias;
		GpuMat ones = GpuMat(1, x.cols, CV_32FC1);
		ones.setTo(1);

		GpuMat forget_gate;
		gemm(Bf, ones, 1.0, GpuMat(), 0.0, bias);
		gemm(Wf_h, h, 1.0, bias, 1.0, H);
		gemm(Wf_x, x, 1.0, H, 1.0, X);
		sigmoidm(X, forget_gate, buffer);

		GpuMat input_gate;
		gemm(Bi, ones, 1.0, GpuMat(), 0.0, bias);
		gemm(Wi_h, h, 1.0, bias, 1.0, H);
		gemm(Wi_x, x, 1.0, H, 1.0, X);
		sigmoidm(X, input_gate, buffer);

		GpuMat output_gate;
		gemm(Bo, ones, 1.0, GpuMat(), 0.0, bias);
		gemm(Wo_h, h, 1.0, bias, 1.0, H);
		gemm(Wo_x, x, 1.0, H, 1.0, X);
		sigmoidm(X, output_gate, buffer);

		GpuMat pre_memory;
		gemm(Bc, ones, 1.0, GpuMat(), 0.0, bias);
		gemm(Wc_h, h, 1.0, bias, 1.0, H);
		gemm(Wc_x, x, 1.0, H, 1.0, X);
		tanhm(X, pre_memory, buffer);

		GpuMat m1, m2, memory;
		multiply(forget_gate, c, m1);
		multiply(input_gate, pre_memory, m2);
		add(m1, m2, memory);

		GpuMat output, tanhMemory;
		tanhm(memory, tanhMemory, buffer);
		multiply(output_gate, tanhMemory, output);

		if (push_stack)
		{
			stack_.push(LSTM_Activation(x, c, h, forget_gate, input_gate, output_gate, pre_memory, tanhMemory));
		}

		return std::make_pair(memory, output);
	}


	std::tuple<GpuMat, GpuMat, GpuMat> V_Long_Short_term_Memory::back_propagate(GpuMat target_grad, GpuMat memory_grad, GpuMat previous_output_grad, int num_samples, int num_sequence, bool pop_stack)
	{
		if (memory_grad.empty())
		{
			memory_grad = GpuMat(Wc_h.rows, target_grad.cols, CV_32FC1);
			memory_grad.setTo(0);
		}

		if (previous_output_grad.empty())
		{
			previous_output_grad = GpuMat(Wo_h.rows, target_grad.cols, CV_32FC1);
			previous_output_grad.setTo(0);
		}

		GpuMat buffer;
		GpuMat ones = GpuMat(1, num_samples, CV_32FC1);
		ones.setTo(1);

		auto activations = stack_.top();
		
		//input, previous_memory, previous_output, forget_gate, input_gate, output_gate, pre_memory, tanhMemory
		GpuMat x = activations.input;
		GpuMat c = activations.previous_memory;
		GpuMat h = activations.previous_output;
		GpuMat forget_gate = activations.forget_gate;
		GpuMat input_gate = activations.input_gate;
		GpuMat output_gate = activations.output_gate;
		GpuMat pre_memory = activations.pre_memory;
		GpuMat tanhMemory = activations.tanh_memory;

		if (pop_stack) stack_.pop();

		//h_grad = target_grad
		//h_grad = previous_output_grad
		GpuMat h_grad;
		addWeighted(target_grad, 1.0, previous_output_grad, 1.0, 0.0, h_grad);

		double denom = 1.0 / num_samples;
		double length_denom = denom / num_sequence;

		//dmem = h_grad.output_gate.tanhm_grad(tanhC)
		//dmem = memory_grad
		GpuMat dmem;
		GpuMat Ct_grad;
		multiply(h_grad, output_gate, dmem);
		tanhm_grad(tanhMemory, Ct_grad, buffer);
		multiply(dmem, Ct_grad, dmem);
		addWeighted(dmem, 1.0, memory_grad, 1.0, 0.0, dmem);

		//dot = h_grad.tanhC
		//dWxo = dot.sigmoidm_grad(output_gate)*x
		//dWho = dot.sigmoidm_grad(output_gate)*h
		//dWbo = dot.sigmoidm_grad(output_gate)*ones
		GpuMat dot, sigout, dotxsigmoidout;
		multiply(h_grad, tanhMemory, dot);
		sigmoidm_grad(output_gate, sigout, buffer);
		multiply(dot, sigout, dotxsigmoidout);
		gemm(dotxsigmoidout, x, length_denom, sWo_x, 1.0, sWo_x, cv::GEMM_2_T, Stream::Null());
		gemm(dotxsigmoidout, h, length_denom, sWo_h, 1.0, sWo_h, cv::GEMM_2_T, Stream::Null());
		gemm(dotxsigmoidout, ones, length_denom, sBo, 1.0, sBo, cv::GEMM_2_T, Stream::Null());

		//dpre_mem = dmem.input_gate
		//dWxc = dpre_mem.tanhm_grad(pre_memory)*x
		//dWhc = dpre_mem.tanhm_grad(pre_memory)*h 
		//dWbc = dpre_mem.tanhm_grad(pre_memory)*ones
		GpuMat dpre_mem, tanhmem, dpre_memxtanhmem;
		multiply(dmem, input_gate, dpre_mem);
		tanhm_grad(pre_memory, tanhmem, buffer);
		multiply(dpre_mem, tanhmem, dpre_memxtanhmem);
		gemm(dpre_memxtanhmem, x, length_denom, sWc_x, 1.0, sWc_x, cv::GEMM_2_T, Stream::Null());
		gemm(dpre_memxtanhmem, h, length_denom, sWc_h, 1.0, sWc_h, cv::GEMM_2_T, Stream::Null());
		gemm(dpre_memxtanhmem, ones, length_denom, sBc, 1.0, sBc, cv::GEMM_2_T, Stream::Null());

		//dit = dmem.pre_memory
		//dWxi = dit.sigmoidm_grad(input_gate)*x
		//dWhi = dit.sigmoidm_grad(input_gate)*h
		//dWbi = dit.sigmoidm_grad(input_gate)*ones
		GpuMat dit, sigin, ditxsigmoidin;
		multiply(dmem, pre_memory, dit);
		sigmoidm_grad(input_gate, sigin, buffer);
		multiply(dit, sigin, ditxsigmoidin);
		gemm(ditxsigmoidin, x, length_denom, sWi_x, 1.0, sWi_x, cv::GEMM_2_T, Stream::Null());
		gemm(ditxsigmoidin, h, length_denom, sWi_h, 1.0, sWi_h, cv::GEMM_2_T, Stream::Null());
		gemm(ditxsigmoidin, ones, length_denom, sBi, 1.0, sBi, cv::GEMM_2_T, Stream::Null());

		//dft = dmem.c
		//dWxf = dft.sigmoidm_grad(forget_gate)*x
		//dWhf = dft.sigmoidm_grad(forget_gate)*h
		//dWbf = dft.sigmoidm_grad(forget_gate)*ones
		GpuMat dft, sigf, dftxsigmoidf;
		multiply(dmem, c, dft);
		sigmoidm_grad(forget_gate, sigf, buffer);
		multiply(dft, sigf, dftxsigmoidf);
		gemm(dftxsigmoidf, x, length_denom, sWf_x, 1.0, sWf_x, cv::GEMM_2_T, Stream::Null());
		gemm(dftxsigmoidf, h, length_denom, sWf_h, 1.0, sWf_h, cv::GEMM_2_T, Stream::Null());
		gemm(dftxsigmoidf, ones, length_denom, sBf, 1.0, sBf, cv::GEMM_2_T, Stream::Null());

		//dc = dmem.forget_gate
		GpuMat dc;
		multiply(dmem, forget_gate, dc);

		//dh = dot.sigmoidm_grad(output_gate)*Who
		//dh = dpre_mem.tanhm_grad(pre_memory)*Whc
		//dh = dit.sigmoidm_grad(input_gate)*Whi
		//dh = dft.sigmoidm_grad(forget_gate)*Whf
		GpuMat dh;
		gemm(Wo_h, dotxsigmoidout, 1.0, GpuMat(), 0.0, dh, cv::GEMM_1_T, Stream::Null());
		gemm(Wc_h, dpre_memxtanhmem, 1.0, dh, 1.0, dh, cv::GEMM_1_T, Stream::Null());
		gemm(Wi_h, ditxsigmoidin, 1.0, dh, 1.0, dh, cv::GEMM_1_T, Stream::Null());
		gemm(Wf_h, dftxsigmoidf, 1.0, dh, 1.0, dh, cv::GEMM_1_T, Stream::Null());

		//dx = dot.sigmoidm_grad(output_gate)*Wxo
		//dx = dpre_mem.tanhm_grad(pre_memory)*Wxc
		//dx = dit.sigmoidm_grad(input_gate)*Wxi
		//dx = dft.sigmoidm_grad(forget_gate)*Wxf
		GpuMat dx;
		gemm(Wo_x, dotxsigmoidout, 1.0, GpuMat(), 0.0, dx, cv::GEMM_1_T, Stream::Null());
		gemm(Wc_x, dpre_memxtanhmem, 1.0, dx, 1.0, dx, cv::GEMM_1_T, Stream::Null());
		gemm(Wi_x, ditxsigmoidin, 1.0, dx, 1.0, dx, cv::GEMM_1_T, Stream::Null());
		gemm(Wf_x, dftxsigmoidf, 1.0, dx, 1.0, dx, cv::GEMM_1_T, Stream::Null());

		return std::make_tuple(dc, dh, dx);
	}


    //http://colah.github.io/posts/2015-08-Understanding-LSTMs/
	void V_Long_Short_term_Memory::train(std::vector<GpuMat> inputs, std::vector<GpuMat> outputs, V_Parameter_LSTM params)
	{
		if (inputs.size() <= 0 || outputs.size() <= 0 || inputs.size() != outputs.size()) 
		{
			std::cout << "Invalid training parameters:" << inputs.size() << " " << outputs.size() << std::endl;
			return;
		}

		initialize_weights(inputs[0].rows, outputs[0].rows, outputs[0].rows);

		reset_momentums();

		double last_error = std::numeric_limits<double>::max(), error;
		for (unsigned int i = 1; i <= params.max_iteration; ++i)
		{
			if (!((i - 1) % 500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0 / params.max_iteration;
			double momentum = (time_factor*params.momentum + (1 - time_factor)*0.5);

			reset_gradients();

			std::vector<GpuMat> gens;
			GpuMat c = GpuMat();
			GpuMat h = GpuMat();
			for (int step = 0; step < inputs.size(); ++step)
			{
				GpuMat x = inputs[step];
				auto tuple = this->activate(x, c, h, true);

				c = std::get<0>(tuple);
				h = std::get<1>(tuple);

				gens.push_back(h);
			}

			GpuMat c_grad;
			GpuMat h_grad;
			GpuMat x_grad;
			for (int step = inputs.size()-1; step > 0; --step)
			{
				// <memory, output_> = activate
				// error gradient = (output - output_)

				GpuMat output_grad, buf;
				subtract(gens[step], outputs[step], output_grad);
				sum_error = sum_error + ave_sqr_sum(output_grad, buf, inputs.size());

				auto back_grads = back_propagate(output_grad, c_grad, h_grad, outputs[step].cols, inputs.size(), true);

				c_grad = std::get<0>(back_grads);
				h_grad = std::get<1>(back_grads);
				x_grad = std::get<2>(back_grads);
			}

			error = update_weights(params, momentum) / inputs.size();
			
			if (!(i % 100)) std::cout << " " << error << std::flush;
			if (abs(error - last_error) < params.error_threshold)
			{
				std::cout << " " << error << std::flush;
				break;
			}
			last_error = error;
		}
		std::cout << std::endl;
	}


	double V_Long_Short_term_Memory::update_weights(V_Parameter_LSTM params, double momentum)
	{
		double deltaWeight = 1.0;
		update(Wf_x, sWf_x, deltaWeight, mWf_x, momentum, params);
		update(Wf_h, sWf_h, deltaWeight, mWf_h, momentum, params);
		update(Bf, sBf, deltaWeight, mBf, momentum, params);
		update(Wi_x, sWi_x, deltaWeight, mWi_x, momentum, params);
		update(Wi_h, sWi_h, deltaWeight, mWi_h, momentum, params);
		update(Bi, sBi, deltaWeight, mBi, momentum, params);
		update(Wc_x, sWc_x, deltaWeight, mWc_x, momentum, params);
		update(Wc_h, sWc_h, deltaWeight, mWc_h, momentum, params);
		update(Bc, sBc, deltaWeight, mBc, momentum, params);
		update(Wo_x, sWo_x, deltaWeight, mWo_x, momentum, params);
		update(Wo_h, sWo_h, deltaWeight, mWo_h, momentum, params);
		update(Bo, sBo, deltaWeight, mBo, momentum, params);
		return sum_error;
	}

	void V_Long_Short_term_Memory::update(GpuMat& weight, GpuMat& delta, double deltaWeight, GpuMat& momentum, double momentum_weight, V_Parameter_LSTM params)
	{
		GpuMat temp;
		addWeighted(delta, 1.0, weight, params.lambda, 0.0, temp);
		addWeighted(momentum, momentum_weight, temp, -deltaWeight, 0, momentum);
		addWeighted(weight, 1, momentum, params.learning_rate, 0, weight);
	}

	void V_Long_Short_term_Memory::reset_gradients()
	{
		sWf_x.setTo(0);
		sWf_h.setTo(0);
		sBf.setTo(0);
		sWi_x.setTo(0);
		sWi_h.setTo(0);
		sBi.setTo(0);
		sWc_x.setTo(0);
		sWc_h.setTo(0);
		sBc.setTo(0);
		sWo_x.setTo(0);
		sWo_h.setTo(0);
		sBo.setTo(0);
		sum_error = 0;
	}

	void V_Long_Short_term_Memory::reset_momentums()
	{
		mWf_x.setTo(0);
		mWf_h.setTo(0);
		mBf.setTo(0);
		mWi_x.setTo(0);
		mWi_h.setTo(0);
		mBi.setTo(0);
		mWc_x.setTo(0);
		mWc_h.setTo(0);
		mBc.setTo(0);
		mWo_x.setTo(0);
		mWo_h.setTo(0);
		mBo.setTo(0);
	}


	void Deep_LSTM::set_layers(int input_size, std::vector<int> output_sizes)
	{
		cells.resize(output_sizes.size());
		int s = input_size;
		for (int i = 0; i < cells.size(); ++i)
		{
			cells[i].initialize_weights(s, output_sizes[i], output_sizes[i]);
			s = output_sizes[i];
		}
	}

	void Deep_LSTM::train(std::vector<GpuMat> inputs, std::vector<GpuMat> outputs, V_Parameter_LSTM params)
	{
		
		if (inputs.size() <= 0 || outputs.size() <= 0 || inputs.size() != outputs.size())
		{
			std::cout << "Invalid training parameters:" << inputs.size() << " " << outputs.size() << std::endl;
			return;
		}

		// <memory, output_> = activate
		// error gradient = (output - output_)

		for (int i = 0; i < cells.size(); ++i) cells[i].reset_momentums();

		double last_error = std::numeric_limits<double>::max(), error;
		for (unsigned int i = 1; i <= params.max_iteration; ++i)
		{
			if (!((i - 1) % 500)) std::cout << std::endl << i << " >";

			double time_factor = i*1.0 / params.max_iteration;
			double momentum = (time_factor*params.momentum + (1 - time_factor)*0.5);

			for (int i = 0; i < cells.size(); ++i) cells[i].reset_gradients();

			std::vector<GpuMat> gens;
			std::vector<std::pair<GpuMat, GpuMat>> previous_states;
			previous_states.resize(cells.size());

			for (int i = 0; i < inputs.size(); ++i)
			{
				GpuMat x = inputs[i];
				std::pair<GpuMat, GpuMat> state;
				for (int j = 0; j < cells.size(); ++j)
				{
					state = cells[j].activate(x, previous_states[j].first, previous_states[j].second, true);
					previous_states[j] = state;
					x = state.second;
				}
				gens.push_back(state.second);
			}

			std::vector<std::tuple<GpuMat, GpuMat, GpuMat>> previous_grads;
			previous_grads.resize(cells.size());
			double sum_error = 0;
			for (int step = inputs.size() - 1; step > 0; --step)
			{
				GpuMat output_grad, buf;
				subtract(gens[step], outputs[step], output_grad);
				sum_error = sum_error + ave_sqr_sum(output_grad, buf, inputs.size());

				for (int j = cells.size() - 1; j >= 0; --j)
				{
					auto back_grads = cells[j].back_propagate(output_grad, std::get<0>(previous_grads[j]), std::get<1>(previous_grads[j]), outputs[step].cols, inputs.size(), true);
					previous_grads[j] = back_grads;
					output_grad = std::get<2>(back_grads);
				}
			}

			for (int j = 0; j < cells.size();++j) cells[j].update_weights(params, momentum);

			error = sum_error / inputs.size();

			if (!(i % 100)) std::cout << " " << error << std::flush;
			if (abs(error - last_error) < params.error_threshold)
			{
				std::cout << " " << error << std::flush;
				break;
			}
			last_error = error;
		}
		std::cout << std::endl;
	}

	void Deep_LSTM::activate(std::vector<GpuMat> inputs, std::vector<GpuMat>& outputs)
	{
		outputs.clear();
		//memory_state, previous_output
		std::vector<std::pair<GpuMat, GpuMat>> previous_states;
		previous_states.resize(cells.size());
		
		for (int i = 0; i < inputs.size(); ++i)
		{
			GpuMat x = inputs[i];
			for (int j = 0; j < cells.size(); ++j)
			{
				auto state = cells[j].activate(x, previous_states[j].first, previous_states[j].second);
				previous_states[j] = state;
				x = state.second;
			}
			outputs.push_back(x);
		}
	}


}
