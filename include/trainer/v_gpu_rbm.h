#ifndef V_GPU_RBM_H
#define V_GPU_RBM_H

#include <vector>
#include "trainer/v_gpu_tensor.h"

namespace cv_ann
{
	struct V_Parameter_RBM
	{
		V_Parameter_RBM(): learning_rate(0.1), cd_round(10), max_iteration(10000), momentum(0.9), lambda(0.0), batchPortion(1.0), with_bias(true) {}
		V_Parameter_RBM(double _learning_rate): learning_rate(_learning_rate), cd_round(10), max_iteration(10000), momentum(0.9), lambda(0.0), batchPortion(1.0), with_bias(true) {}

		double learning_rate;
		double cd_round;
		int max_iteration;
		double momentum;
		double lambda;
		double batchPortion;
		bool with_bias;
	};


	//Bp will not be trained, so it must have the dimension of (W.rows x data.cols).
	//S is the sampling function for activation function A
	cv::Mat rbm_train(
		cv::Mat data,cv::Mat W,cv::Mat B,cv::Mat Bp,
		std::function<cv::Mat(cv::Mat)> A,std::function<cv::Mat(cv::Mat)> Ap,std::function<cv::Mat(cv::Mat)> S,
		int _max_iteration = 5000, int _cd_round = 10, 
		double _learning_rate = 0.1, double _momentum_weight = 0.9, 
		double _lambda = 0.0, bool _with_bias = true);

    //train p(v,c) = exp(hWv + hUc)/Z, later we can do p(c|v) by p(v,c)/ (sum_v p(v,c))
    cv::Mat bimodal_rbm_train(
        cv::Mat data,cv::Mat condition,cv::Mat W,cv::Mat B, cv::Mat U,cv::Mat Bp,
        std::function<cv::Mat(cv::Mat)> A,std::function<cv::Mat(cv::Mat)> Ap,std::function<cv::Mat(cv::Mat)> S,
        int _max_iteration = 5000, int _cd_round = 10,
        double _learning_rate = 0.1, double _momentum_weight = 0.9,
        double _lambda = 0.0, bool _with_bias = true);

    //train p(v|c) = exp(hWv + hUc)/Z where c is the static condition and should not be used for backward inference.
    //faster than bimodal_rbm_train
    cv::Mat conditional_rbm_train(
        cv::Mat data,cv::Mat condition,cv::Mat W,cv::Mat B, cv::Mat U,cv::Mat Bp,
        std::function<cv::Mat(cv::Mat)> A,std::function<cv::Mat(cv::Mat)> Ap,std::function<cv::Mat(cv::Mat)> S,
        int _max_iteration = 5000, int _cd_round = 10,
        double _learning_rate = 0.1, double _momentum_weight = 0.9,
        double _lambda = 0.0, bool _with_bias = true);


	//conditional_rbm_train
	//Bp will not be trained, so it must have the dimension of (W.rows x data.cols).
	void rbm_relu_train(
		GpuMat data, GpuMat condition, GpuMat W, GpuMat B, GpuMat U, GpuMat Bp,
		V_Parameter_RBM params = V_Parameter_RBM(0.001)
		);

	//minibatch
	//Bp will not be trained, so it must have the dimension of (W.rows x data.cols).
	void rbm_relu_train(
		std::vector<GpuMat> data, std::vector<GpuMat> condition, GpuMat W, GpuMat B, GpuMat U, std::vector<GpuMat> Bp,
		V_Parameter_RBM params = V_Parameter_RBM(0.001)
		);

}

#endif