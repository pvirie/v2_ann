#ifndef V_GPU_UTIL_H
#define V_GPU_UTIL_H

#include <opencv2/opencv.hpp>

#ifdef OPENCV_3_0
#include <opencv2/cudaarithm.hpp>
using namespace cv::cuda;
#define cv_gpu cv::cuda
#else
#include <opencv2/gpu/gpu.hpp>
using namespace cv::gpu;
#define cv_gpu cv::gpu
#endif

namespace cv_ann
{
	void setup_gpu();
	void test(cv::Mat img);
	void v_convolve(const GpuMat& src, const GpuMat& kernel, GpuMat&  res, bool flip = true, bool do_pad = true);
	void pad(const GpuMat& src, GpuMat& res, int w, int h);
	void sigmoidm(const GpuMat& x, GpuMat& dst,GpuMat& buf);
	void sigmoidm_grad(const GpuMat& sigx, GpuMat& dst,GpuMat& buf);
	void relum(const GpuMat& x, GpuMat& dst);
	void relum_grad(const GpuMat& relux, GpuMat& dst);
	void tanhm(const GpuMat& x, GpuMat& dst,GpuMat& buf);
	void tanhm_grad(const GpuMat& tanhx, GpuMat& dst,GpuMat& buf); // x is tanh(x)

	void create_pack_map(const int offset_x, const int offset_y, const int width, const int height, const int s, GpuMat& mapx, GpuMat& mapy);
	void create_unpack_map(const int width, const int height, const int s, GpuMat& mapx, GpuMat& mapy);	

	struct GPU_Random
	{
		cv::RNG seed_;
		int width_;
		int height_;
		GpuMat temp_;
		GpuMat temp2_;

		GPU_Random(int seed, int row, int column);
		void randu(GpuMat& dst);
	};

	template<class T>
    T abs(T a)
    {
        return a>0? a:-a;
    }

	void sample_bernoulli(const GpuMat& x, GpuMat& dst, GPU_Random& seed);
	void sample_gaussian(const GpuMat& x, GpuMat& dst, GPU_Random& seed);

	void arrange_data(const GpuMat& data, int recursive_step, const GpuMat& dummy, GpuMat& hist);
	double ave_sqr_sum(const GpuMat& data, GpuMat& buf, double total);
}

#endif
