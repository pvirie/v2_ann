#ifndef V_GPU_AUC_H
#define V_GPU_AUC_H

#include <vector>
#include "trainer/v_gpu_tensor.h"

namespace cv_ann
{
	struct V_Parameter_AUC
	{
		V_Parameter_AUC(): learning_rate(0.1), error_threshold(1e-6), max_iteration(2000), momentum(0.9), lambda(0.0), batchPortion(1.0), with_bias(true) {}
		V_Parameter_AUC(double _learning_rate, double _error_threshold = 1e-4): learning_rate(_learning_rate), error_threshold(_error_threshold), max_iteration(2000), momentum(0.9), lambda(0.0), batchPortion(1.0), with_bias(true) {}

		double learning_rate;
		double error_threshold;
		int max_iteration;
		double momentum;
		double lambda;
		double batchPortion;
		bool with_bias;
	};

	V_GPU_Tensor auc_train(
		std::vector<V_GPU_Tensor>& data,
		std::vector<V_GPU_Tensor>& Ws,
		V_Parameter_AUC params = V_Parameter_AUC()
		);

	V_GPU_Tensor auc_train(
		std::vector<V_GPU_Tensor>& data,
		std::vector<V_GPU_Tensor>& Ws,   //residue
		std::vector<V_GPU_Tensor>& condition,
		std::vector<V_GPU_Tensor>& Us,
		V_Parameter_AUC params = V_Parameter_AUC()
		);
	
	//Bp will not be trained, so it must have the dimension of (W.cols x data.cols).
	void auc_relu_train(
		GpuMat data, GpuMat W, GpuMat B, GpuMat Bp,
		V_Parameter_AUC params = V_Parameter_AUC(0.001)
		);

	//h = W*data + U*condition + B; v = W^T*h + Bp
	//for adaptive recon: h = W*data + U*past + B; v = W^T*h + Bp where Bp = (U^T*F + T*past)
	//Bp will not be trained, so it must have the dimension of (W.cols x data.cols).
	void auc_relu_train(
		GpuMat data, GpuMat condition, GpuMat W, GpuMat B, GpuMat U, GpuMat Bp,
		V_Parameter_AUC params = V_Parameter_AUC(0.001)
		);

	//with minibatch
	//Bp will not be trained, so it must have the dimension of (W.cols x data.cols).
	void auc_relu_train(
		std::vector<GpuMat> data, GpuMat W, GpuMat B, std::vector<GpuMat> Bp,
		V_Parameter_AUC params = V_Parameter_AUC(0.001)
		);

	//with minibatch
	//Bp will not be trained, so it must have the dimension of (W.cols x data.cols).
	void auc_relu_train(
		std::vector<GpuMat> data, std::vector<GpuMat> condition, GpuMat W, GpuMat B, GpuMat U, std::vector<GpuMat> Bp,
		V_Parameter_AUC params = V_Parameter_AUC(0.001)
		);

	//the generative weight is not the transpose of the recognition weight.
	//all weights have the upward direction (ascentral x descentral)
	//Bp will not be trained, so it must have the dimension of (W.cols x data.cols).
	void auc_non_sharing_relu_train(
		GpuMat data, GpuMat Wr, GpuMat Wg, GpuMat B, GpuMat Bp,
		V_Parameter_AUC params = V_Parameter_AUC(0.001)
		);

	//the generative weight is not the transpose of the recognition weight.
	//all weights have the upward direction (ascentral x descentral)
	//Bp will not be trained, so it must have the dimension of (W.cols x data.cols).
	void auc_non_sharing_tanh_train(
		GpuMat data, GpuMat Wr, GpuMat Wg, GpuMat B, GpuMat Bp,
		V_Parameter_AUC params = V_Parameter_AUC(0.001)
		);

	//tie
	void supervise_relu_train(
		GpuMat data,GpuMat labels,GpuMat W,GpuMat B,
		V_Parameter_AUC params = V_Parameter_AUC(0.001)
		);

	//tie with minibatch
	void supervise_relu_train(
		std::vector<GpuMat> data,std::vector<GpuMat> labels,GpuMat W,GpuMat B,
		V_Parameter_AUC params = V_Parameter_AUC(0.001)
		);

	//with minibatch
	//Bp will not be trained, so it must have the dimension of (W.cols x data.cols).
	void auc_relu_train(
		std::vector<GpuMat> data, 
		std::vector<GpuMat> resd, 
		std::vector<GpuMat> C1,
		std::vector<GpuMat> C2,
		GpuMat W, 
		GpuMat R,
		GpuMat U1,
		GpuMat U2,
		std::vector<GpuMat> Bp_data,
		std::vector<GpuMat> Bp_resd,
		V_Parameter_AUC params = V_Parameter_AUC(0.001)
		);


	//legacy
	//Bp will not be trained, so it must have the dimension of (W.cols x data.cols).
	cv::Mat auenc_train(
		cv::Mat data,cv::Mat W,cv::Mat B,cv::Mat Bp,
		std::function<cv::Mat(cv::Mat)> A,std::function<cv::Mat(cv::Mat)> Ap,std::function<cv::Mat(cv::Mat)> D,
		int _max_iteration = 20000, double _error_steady = 1e-8, 
		double _learning_rate = 0.1, double _momentum_weight = 0.9, 
		double _lambda = 0.0, bool _with_bias = true);
	
	//legacy
	cv::Mat conditional_auenc_train(
        cv::Mat data,cv::Mat condition,cv::Mat W,cv::Mat B, cv::Mat U,cv::Mat Bp,
		std::function<cv::Mat(cv::Mat)> A,std::function<cv::Mat(cv::Mat)> Ap,std::function<cv::Mat(cv::Mat)> D,
		int _max_iteration = 20000, double _error_steady = 1e-8, 
		double _learning_rate = 0.1, double _momentum_weight = 0.9, 
		double _lambda = 0.0, bool _with_bias = true);

}

#endif
