#ifndef V_GPU_TENSOR_H
#define V_GPU_TENSOR_H

#include <opencv2/opencv.hpp>
#include <vector>
#include <functional>
#include "network/v_tensor.h"

#ifdef OPENCV_3_0
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudawarping.hpp>
using namespace cv::cuda;
#define cv_gpu cv::cuda
#else
#include <opencv2/gpu/gpu.hpp>
using namespace cv::gpu;
#define cv_gpu cv::gpu
#endif

namespace cv_ann
{

	class V_GPU_Tensor
	{
	protected:
		std::vector<GpuMat> data;

	public:

		V_GPU_Tensor();
		V_GPU_Tensor(int depth);
		V_GPU_Tensor(GpuMat img);
		~V_GPU_Tensor();

		void download(V_Tensor& local);

		int depth() const;
		void push_back(GpuMat slice);
		GpuMat& operator[](int index);
		const GpuMat& operator[](int index) const;

		void convolve(const V_GPU_Tensor& kernel, GpuMat& res, GpuMat& buff, V_GPU_Tensor& buff2) const;
	};

	std::vector<V_GPU_Tensor> copy_template(const std::vector<V_GPU_Tensor>& x); //can return by value efficiently due to 'return value optimization' for C++0x
	std::vector<V_GPU_Tensor> copy_flipped_template(const std::vector<V_GPU_Tensor>& x);
	
	void add(const V_GPU_Tensor& a,const V_GPU_Tensor& b, V_GPU_Tensor& res);
	void subtract(const V_GPU_Tensor& a,const V_GPU_Tensor& b, V_GPU_Tensor& res);
	void multiply(const V_GPU_Tensor& a, const V_GPU_Tensor& b, V_GPU_Tensor& res);
	void multiply(const V_GPU_Tensor& a, const double b, V_GPU_Tensor& res);
	void sigmoid(const V_GPU_Tensor& x, V_GPU_Tensor& res, GpuMat& buff);
	void grad_sigmoid(const V_GPU_Tensor& x, V_GPU_Tensor& res, GpuMat& buff);
	void relu(const V_GPU_Tensor& x, V_GPU_Tensor& res, GpuMat& buff);
	void grad_relu(const V_GPU_Tensor& x, V_GPU_Tensor& res, GpuMat& buff);
	void threshold(const V_GPU_Tensor& x, V_GPU_Tensor& res, double t);
		
	void flip(const std::vector<V_GPU_Tensor>& W, std::vector<V_GPU_Tensor>& Wt);
	void convolve(const std::vector<V_GPU_Tensor>& W, const V_GPU_Tensor& x, V_GPU_Tensor& res, V_GPU_Tensor& buff, std::vector<V_GPU_Tensor>& buff2);
	void deconvolve(const V_GPU_Tensor& y, const V_GPU_Tensor& x, std::vector<V_GPU_Tensor>& res, std::vector<V_GPU_Tensor>& buff);

	void add(const std::vector<V_GPU_Tensor>& a,const std::vector<V_GPU_Tensor>& b, std::vector<V_GPU_Tensor>& res);
	void weighted_add(std::vector<V_GPU_Tensor>& self, double w_self,const std::vector<V_GPU_Tensor>& x, double w_x);

	double get_avesqr_error(const V_GPU_Tensor& diff, V_GPU_Tensor& buff);
	void init_value(std::vector<V_GPU_Tensor>& x, const std::vector<V_GPU_Tensor>& temp, double value);
	void set_value(std::vector<V_GPU_Tensor>& x, double value);

	//map_xy arranges as follows: [img1_mapx,img1_mapy,img2_mapx,img2_mapy]
	void pack_grid(const V_GPU_Tensor& src, V_GPU_Tensor& dst, const int s, const V_GPU_Tensor& map_xy);
	void unpack_grid(const V_GPU_Tensor& src, V_GPU_Tensor& dst, const int s, const V_GPU_Tensor& map_xy, V_GPU_Tensor& buff_y);

	void create_pack_map(const int data_size, const int width, const int height, const int s, V_GPU_Tensor& map_xy);
	void create_unpack_map(const int data_size, const int width, const int height, const int s, V_GPU_Tensor& map_xy);

	//reduce size by half along each dimension
	void pyr_down(const V_GPU_Tensor& src, V_GPU_Tensor& dst);

}

#endif
