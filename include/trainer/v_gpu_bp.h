#ifndef V_GPU_BP_H
#define V_GPU_BP_H

#include <opencv2/opencv.hpp>
#include <vector>
#include <functional>

#ifdef OPENCV_3_0
#include <opencv2/cudaarithm.hpp>
using namespace cv::cuda;
#define cv_gpu cv::cuda
#else
#include <opencv2/gpu/gpu.hpp>
using namespace cv::gpu;
#define cv_gpu cv::gpu
#endif

namespace cv_ann
{
	struct V_Parameter_BP
	{
		V_Parameter_BP() : learning_rate(0.1), error_threshold(1e-6), max_iteration(2000), momentum(0.9), lambda(0.0), batchPortion(1.0) {}
		V_Parameter_BP(double _learning_rate) : learning_rate(_learning_rate), error_threshold(1e-6), max_iteration(2000), momentum(0.9), lambda(0.0), batchPortion(1.0) {}

		double learning_rate;
		double error_threshold;
		int max_iteration;
		double momentum;
		double lambda;
		double batchPortion;
	};

	//with minibatch
	//multilayer backpropagation
	void backpropagation(
		GpuMat data, GpuMat label, std::vector<GpuMat>& Ws, std::vector<GpuMat>& Bs, V_Parameter_BP params = V_Parameter_BP(0.001)
		);

	//with minibatch
	//multilayer backpropagation
	void backpropagation_tanh(
		GpuMat data, GpuMat label, std::vector<GpuMat>& Ws, std::vector<GpuMat>& Bs, V_Parameter_BP params = V_Parameter_BP(0.001)
		);

	//with minibatch
	//temporal backpropagation: W data to next data, A past to next data
	void backpropagation(
		std::vector<GpuMat> data, std::vector<GpuMat> condition, std::vector<GpuMat> Ws, std::vector<GpuMat> As,
		std::vector<int> num_recursions, int final_depth, V_Parameter_BP params = V_Parameter_BP(0.001)
		);


}

#endif
