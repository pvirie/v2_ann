#ifndef V_TENSOR_H
#define V_TENSOR_H

#include <opencv2/opencv.hpp>
#include <vector>
#include <functional>

namespace cv_ann
{
	class V_Tensor
	{
	protected:
		std::vector<cv::Mat> data;

	public:

		V_Tensor();
		V_Tensor(cv::Mat img);
		V_Tensor(int depth);
		~V_Tensor();

		int depth() const;
		void push_back(cv::Mat slice);
		cv::Mat& operator[](int index);
		const cv::Mat& operator[](int index) const;

		cv::Mat operator*(const V_Tensor& x) const;
		V_Tensor operator+(const V_Tensor& x) const;
		V_Tensor operator*(const double x) const;

	};

	V_Tensor operator* (const std::vector<V_Tensor>& W, const V_Tensor& x);
	
	void sigmoidm(const cv::Mat& x, cv::Mat& res);
	V_Tensor sigmoidm(const V_Tensor& x);

	void relum(const cv::Mat& x, cv::Mat& res);
	V_Tensor relum(const V_Tensor& x);
	
	V_Tensor threshold(const V_Tensor& x, double t);

	V_Tensor pack_grid(const V_Tensor& x, const int s, V_Tensor& map);
	V_Tensor unpack_grid(const V_Tensor& x, const int s, V_Tensor& map);
	
	//reduce size by half along each dimension
	void pyr_down(const V_Tensor& src, V_Tensor& dst);

	bool save(std::string file_name, std::vector<V_Tensor>& W);
	bool load(std::string file_name, std::vector<V_Tensor>& W);
}

#endif
