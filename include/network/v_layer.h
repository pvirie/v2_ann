#ifndef V_LAYER_H
#define V_LAYER_H

#include "network/v_util.h"
#include <vector>
#include <functional>

namespace cv_ann
{

	/**
	* @brief The V_Layer class
	* This class is an interface for layer classes.
	*/
	class V_Layer
	{
	protected:

	public:
		V_Layer();
		~V_Layer();

		virtual V_Tensor activate(V_Tensor v) = 0;
		virtual V_Tensor project(V_Tensor v) = 0;

	};

}

#endif
