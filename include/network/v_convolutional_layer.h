#ifndef V_CONVOLUTIONAL_LAYER_H
#define V_CONVOLUTIONAL_LAYER_H

#include <vector>
#include <functional>
#include "network/v_layer.h"
#include "network/v_util.h"

namespace cv_ann
{

	class V_Convolutional_Layer : public V_Layer
	{
	protected:
		std::vector<V_Tensor> W;
		std::vector<V_Tensor> Wt;

	public:
		V_Convolutional_Layer();
		~V_Convolutional_Layer();

		V_Tensor activate(V_Tensor v);
		V_Tensor project(V_Tensor h);
		
		void set_weight(std::vector<V_Tensor> W);
	};

	class V_Conditional_Convolutional_Layer : public V_Convolutional_Layer
	{
	protected:
		std::vector<V_Tensor> U;
		std::vector<V_Tensor> Ut;

	public:
		V_Conditional_Convolutional_Layer();
		~V_Conditional_Convolutional_Layer();

		V_Tensor activate_condition(V_Tensor v);
		V_Tensor project(V_Tensor h, V_Tensor c);
		
		void set_weight(std::vector<V_Tensor> W, std::vector<V_Tensor> U);
	};
	
}

#endif
