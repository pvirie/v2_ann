#ifndef V_UTIL_H
#define V_UTIL_H

#include <opencv2/opencv.hpp>
#include "network/v_tensor.h"

namespace cv_ann
{
	double sigmoid(double x);	
	cv::Mat sigmoid_activate(cv::Mat x);
	cv::Mat sigmoid_grad(cv::Mat x);
	cv::Mat sigmoid_sample(cv::Mat x);

	cv::Mat relu_activate(cv::Mat x);
	cv::Mat relu_grad(cv::Mat x);
	cv::Mat relu_sample(cv::Mat x);

	cv::Mat identity(cv::Mat x);

	cv::Mat logm(cv::Mat x);
	cv::Mat expm(cv::Mat x);

	cv::Mat to_column_vector(cv::Mat m);
	cv::Mat to_column_matrix(cv::Mat m,int columns);

	cv::Mat sample_bernoulli(cv::Mat m);
    cv::Mat sample_gaussian(cv::Mat m, cv::Mat sd = cv::Mat());

	cv::Mat log_sum_exp_over_cols(cv::Mat x);
	cv::Mat threshold(cv::Mat in, double value);

    void print_mat(cv::Mat m, bool with_data = false);
	void save_mat(std::string filename, cv::Mat m);

	cv::Mat tie(
		cv::Mat data,cv::Mat labels,cv::Mat W,cv::Mat B,
		std::function<cv::Mat(cv::Mat)> A,
		int _max_iteration = 20000, double _error_steady = 1e-8, 
		double _learning_rate = 0.1, double _momentum_weight = 0.9, 
		double _lambda = 0.0, bool _with_bias = true);
}

#endif
