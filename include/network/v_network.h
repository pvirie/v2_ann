#ifndef V_NETWORK_H
#define V_NETWORK_H

#include "network/v_layer.h"
#include <vector>
#include <functional>
#include <memory>

namespace cv_ann
{
	class V_Network
	{
	protected:
		std::vector<std::shared_ptr<V_Layer>> layers_;

	public:

		V_Network();
		~V_Network();

		/**
		* @brief add_layer
		* @param layer
		*/
		void add_layer(std::shared_ptr<V_Layer> layer);
		std::shared_ptr<V_Layer> operator[](int index);
		int size();

		void train(std::vector<cv::Mat> data);

		bool save(std::string file_name = "./network.ann");
		bool load(std::string file_name = "./network.ann");
	};

}

#endif
